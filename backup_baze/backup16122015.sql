--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: kreiraj_razgovor(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION kreiraj_razgovor(ime character varying, idkreatora integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
  DECLARE novi_id INTEGER;
	BEGIN
		INSERT INTO razgovor (imerazgovora) VALUES ($1);
		SELECT currval(pg_get_serial_sequence('razgovor','idrazgovora')) INTO novi_id;
		
		INSERT INTO pripadnost_razgovoru (idrazgovora,idsudionika,status) values (novi_id,$2,true);
		
		
	
		RETURN novi_id;
	END
	$_$;


ALTER FUNCTION public.kreiraj_razgovor(ime character varying, idkreatora integer) OWNER TO postgres;

--
-- Name: notify_dodan_u_razgovor(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION notify_dodan_u_razgovor() RETURNS trigger
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
	PERFORM pg_catalog.pg_notify('korisnik_' || NEW.idsudionika::text || '_dodan_u_razgovor', NEW.idrazgovora::text);
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.notify_dodan_u_razgovor() OWNER TO postgres;

--
-- Name: notify_nova_poruka(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION notify_nova_poruka() RETURNS trigger
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
	PERFORM pg_catalog.pg_notify('poslana_poruka_u_razgovoru_' || NEW.idrazgovora::text, NEW.vrijemeslanja::text); 
	--vrijemeslanja je payload tako da svaka notifikacija bude razlicita, da ih postgresql ne spoji ako su iste, tako pise u dokumentaciji
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.notify_nova_poruka() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: avatar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE avatar (
    idavatar integer NOT NULL,
    lokacijaavatara character varying(200) NOT NULL,
    imeavatara character varying(50) NOT NULL
);


ALTER TABLE avatar OWNER TO postgres;

--
-- Name: avatar_idavatara_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE avatar_idavatara_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE avatar_idavatara_seq OWNER TO postgres;

--
-- Name: avatar_idavatara_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE avatar_idavatara_seq OWNED BY avatar.idavatar;


--
-- Name: bojafonta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bojafonta (
    idboje integer NOT NULL,
    imeboje character varying(20),
    rgb_r integer NOT NULL,
    rgb_g integer NOT NULL,
    rgb_b integer NOT NULL,
    CONSTRAINT bojafonta_rgb_b_check CHECK (((rgb_b >= 0) AND (rgb_b <= 255))),
    CONSTRAINT bojafonta_rgb_g_check CHECK (((rgb_g >= 0) AND (rgb_g <= 255))),
    CONSTRAINT bojafonta_rgb_r_check CHECK (((rgb_r >= 0) AND (rgb_r <= 255)))
);


ALTER TABLE bojafonta OWNER TO postgres;

--
-- Name: bojafonta_idboje_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bojafonta_idboje_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bojafonta_idboje_seq OWNER TO postgres;

--
-- Name: bojafonta_idboje_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bojafonta_idboje_seq OWNED BY bojafonta.idboje;


--
-- Name: bojasucelja; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bojasucelja (
    idboje integer NOT NULL,
    imeboje character varying(20),
    rgb_r integer NOT NULL,
    rgb_g integer NOT NULL,
    rgb_b integer NOT NULL,
    CONSTRAINT bojasucelja_rgb_b_check CHECK (((rgb_b >= 0) AND (rgb_b <= 255))),
    CONSTRAINT bojasucelja_rgb_g_check CHECK (((rgb_g >= 0) AND (rgb_g <= 255))),
    CONSTRAINT bojasucelja_rgb_r_check CHECK (((rgb_r >= 0) AND (rgb_r <= 255)))
);


ALTER TABLE bojasucelja OWNER TO postgres;

--
-- Name: bojasucelja_idboje_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bojasucelja_idboje_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bojasucelja_idboje_seq OWNER TO postgres;

--
-- Name: bojasucelja_idboje_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bojasucelja_idboje_seq OWNED BY bojasucelja.idboje;


--
-- Name: font; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE font (
    idfonta integer NOT NULL,
    imefonta character varying(20),
    velicinafonta integer DEFAULT 20 NOT NULL,
    stilfonta character(6) DEFAULT 'plain'::bpchar NOT NULL,
    CONSTRAINT font_stilfonta_check CHECK ((((lower((stilfonta)::text) = 'plain'::text) OR (lower((stilfonta)::text) = 'italic'::text)) OR (lower((stilfonta)::text) = 'bold'::text))),
    CONSTRAINT font_velicinafonta_check CHECK (((velicinafonta >= 10) AND (velicinafonta <= 30)))
);


ALTER TABLE font OWNER TO postgres;

--
-- Name: CONSTRAINT font_stilfonta_check ON font; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT font_stilfonta_check ON font IS 'Ako dodajemo novi font, provjerava jesmo li napisali stil fonta ''plain'', ''italic'', ili ''bold'', inace je greska';


--
-- Name: CONSTRAINT font_velicinafonta_check ON font; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT font_velicinafonta_check ON font IS 'ako dodajemo novi font provjerava nalazi li se velicina fonta u zadanom intervalu';


--
-- Name: font_idfonta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE font_idfonta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE font_idfonta_seq OWNER TO postgres;

--
-- Name: font_idfonta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE font_idfonta_seq OWNED BY font.idfonta;


--
-- Name: kontakti; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kontakti (
    idvlasnika integer NOT NULL,
    idkontakta integer NOT NULL,
    status boolean,
    nadimak character varying(40)
);


ALTER TABLE kontakti OWNER TO postgres;

--
-- Name: korisnik; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE korisnik (
    idkorisnika integer NOT NULL,
    korisnickoime character varying(20) NOT NULL,
    lozinka character varying(40) NOT NULL,
    aktivan boolean DEFAULT true NOT NULL,
    email character varying(50) NOT NULL,
    idbojafonta integer DEFAULT 1 NOT NULL,
    idfont integer DEFAULT 1 NOT NULL,
    idavatar integer DEFAULT 1 NOT NULL,
    idbojasucelja integer DEFAULT 1 NOT NULL
);


ALTER TABLE korisnik OWNER TO postgres;

--
-- Name: korisnik_idkorisnika_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE korisnik_idkorisnika_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE korisnik_idkorisnika_seq OWNER TO postgres;

--
-- Name: korisnik_idkorisnika_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE korisnik_idkorisnika_seq OWNED BY korisnik.idkorisnika;


--
-- Name: poruka; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE poruka (
    idporuke integer NOT NULL,
    idrazgovora integer NOT NULL,
    idposiljatelja integer NOT NULL,
    tekst text NOT NULL,
    vrijemeslanja timestamp without time zone DEFAULT transaction_timestamp() NOT NULL
);


ALTER TABLE poruka OWNER TO postgres;

--
-- Name: poruka_idporuke_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE poruka_idporuke_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE poruka_idporuke_seq OWNER TO postgres;

--
-- Name: poruka_idporuke_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE poruka_idporuke_seq OWNED BY poruka.idporuke;


--
-- Name: pripadnost_razgovoru; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pripadnost_razgovoru (
    idrazgovora integer NOT NULL,
    idsudionika integer NOT NULL,
    status boolean DEFAULT true NOT NULL
);


ALTER TABLE pripadnost_razgovoru OWNER TO postgres;

--
-- Name: razgovor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE razgovor (
    idrazgovora integer NOT NULL,
    imerazgovora character varying(30),
    vrijemepocetka timestamp without time zone DEFAULT transaction_timestamp() NOT NULL
);


ALTER TABLE razgovor OWNER TO postgres;

--
-- Name: razgovor_idrazgovora_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE razgovor_idrazgovora_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE razgovor_idrazgovora_seq OWNER TO postgres;

--
-- Name: razgovor_idrazgovora_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE razgovor_idrazgovora_seq OWNED BY razgovor.idrazgovora;


--
-- Name: idavatar; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avatar ALTER COLUMN idavatar SET DEFAULT nextval('avatar_idavatara_seq'::regclass);


--
-- Name: idboje; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bojafonta ALTER COLUMN idboje SET DEFAULT nextval('bojafonta_idboje_seq'::regclass);


--
-- Name: idboje; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bojasucelja ALTER COLUMN idboje SET DEFAULT nextval('bojasucelja_idboje_seq'::regclass);


--
-- Name: idfonta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY font ALTER COLUMN idfonta SET DEFAULT nextval('font_idfonta_seq'::regclass);


--
-- Name: idkorisnika; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik ALTER COLUMN idkorisnika SET DEFAULT nextval('korisnik_idkorisnika_seq'::regclass);


--
-- Name: idporuke; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poruka ALTER COLUMN idporuke SET DEFAULT nextval('poruka_idporuke_seq'::regclass);


--
-- Name: idrazgovora; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY razgovor ALTER COLUMN idrazgovora SET DEFAULT nextval('razgovor_idrazgovora_seq'::regclass);


--
-- Data for Name: avatar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY avatar (idavatar, lokacijaavatara, imeavatara) FROM stdin;
1	http://i.imgur.com/PlbgSFk.jpg	banana
2	http://images.clipartpanda.com/watermelon-clip-art-watermelon_slice.svg	lubenica
3	https://stephanieallison.files.wordpress.com/2010/08/single-small-pear5b15d.jpg	crtez kruske
\.


--
-- Name: avatar_idavatara_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('avatar_idavatara_seq', 3, true);


--
-- Data for Name: bojafonta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bojafonta (idboje, imeboje, rgb_r, rgb_g, rgb_b) FROM stdin;
1	crna	0	0	0
2	bijela	255	255	255
3	siva	51	51	51
\.


--
-- Name: bojafonta_idboje_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bojafonta_idboje_seq', 3, true);


--
-- Data for Name: bojasucelja; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bojasucelja (idboje, imeboje, rgb_r, rgb_g, rgb_b) FROM stdin;
1	plava	0	0	255
2	Nezrela banana	204	255	51
3	Baby pink	255	199	236
4	Lavender	229	199	255
5	Purple wedding	65	7	34
\.


--
-- Name: bojasucelja_idboje_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bojasucelja_idboje_seq', 5, true);


--
-- Data for Name: font; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY font (idfonta, imefonta, velicinafonta, stilfonta) FROM stdin;
2	Arial	15	bold  
1	Times New Roman	20	plain 
3	DejaVu Sans	20	italic
4	Old London	22	plain 
5	Comic Sans MS	19	plain 
6	Serif	14	bold  
7	Verdana	14	bold  
8	Georgia	15	bold  
\.


--
-- Name: font_idfonta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('font_idfonta_seq', 9, true);


--
-- Data for Name: kontakti; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kontakti (idvlasnika, idkontakta, status, nadimak) FROM stdin;
2	3	t	netko
13	12	t	telefon
12	13	t	tablet
14	12	t	marin1
14	13	t	marin2
\.


--
-- Data for Name: korisnik; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY korisnik (idkorisnika, korisnickoime, lozinka, aktivan, email, idbojafonta, idfont, idavatar, idbojasucelja) FROM stdin;
2	zlatko.hrastic	5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8	t	zlatko.hrastic@fer.hr	1	1	1	1
3	vekszorp	b446080ccff9f99de750fac9db98c7cde151fd8e	t	vedran.priscan@fer.hr	2	8	1	5
5	zoran.karamarko	d9dd7135972df03ac7eae9c8c2c418abbe1745e6	t	zoran.karamarko@gmail.com	1	1	1	1
7	mujo	9633ef86687fa9565c7641b22344927e785c5250	t	mujo@mujo.hr	1	1	1	1
8	ottosinger	AF933EAD2B9BDDBE90F7BB47C6D637C4E654F58E	t	otto.singer@fer.hr	1	1	1	1
9	vucko	8c6a06670992025c8c8f6c8bd3a7db418bce3039	t	vucko@vucko.hr	1	1	1	1
12	marin	70c881d4a26984ddce795f6f71817c9cf4480e79	t	marin@email.com	1	1	1	1
13	marin2	70c881d4a26984ddce795f6f71817c9cf4480e79	t	marin2@email.com	1	1	1	1
14	marin3	70c881d4a26984ddce795f6f71817c9cf4480e79	t	marin3@email.com	1	1	1	1
10	pero	98FC7B34760FACE5E268BFF318180E05861A970F	t	pero@pero.pero	1	2	1	1
11	Agneza	b687ea9353a11e817a77bfaea0a16194ab5f8307	t	agneza.sandic@fer.hr	2	1	3	3
4	mkalebota	648c06b8459ba15d459f13e369847704815207d	t	marija.kalebota@fer.hr	2	7	3	5
15	prozskev	d688ae4face9f51ed4844f51c609fc9762900535	t	lazni_email@tvz.hr	1	7	1	2
\.


--
-- Name: korisnik_idkorisnika_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('korisnik_idkorisnika_seq', 15, true);


--
-- Data for Name: poruka; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY poruka (idporuke, idrazgovora, idposiljatelja, tekst, vrijemeslanja) FROM stdin;
1	1	2	tekst poruke	2015-11-14 10:21:41.720219
2	5	2	bla	2015-12-11 18:06:17.407231
4	5	2	molim te radi	2015-12-11 18:38:00.550686
5	5	2	katarian ili Silvija?	2015-12-11 18:48:51.444645
6	5	2	katarian ili Silvija?	2015-12-11 18:49:12.620277
7	5	2	katarian ili Silvija?bfdsbdsf	2015-12-11 18:49:21.203742
8	5	2	vani je	2015-12-11 18:55:11.740456
9	5	2	sretni smo	2015-12-11 19:01:01.326828
11	10	12	prva por	2015-12-13 00:38:34.246505
20	13	3	moja prva poruka	2015-12-14 18:41:39.654201
21	13	3	hello	2015-12-14 19:53:06.144721
22	5	2	još jedna	2015-12-14 20:08:23.191344
23	1	2	još jedna	2015-12-14 20:08:37.851791
24	5	2	keeee?	2015-12-14 20:09:56.234168
25	1	2	ova sad mora bit zadnja	2015-12-14 20:16:28.547982
26	4	2	drugi razgovor	2015-12-14 20:32:04.053537
27	13	3	hi\n	2015-12-14 20:58:47.474545
28	13	3	je li nevidljiva poruka zapravo poruka?	2015-12-14 21:01:02.628875
29	5	2	nova poruka	2015-12-14 21:02:31.194472
30	13	3	visoka\nviisoka\nporukaa\naaaa\na	2015-12-14 21:16:52.806161
31	13	3	testić	2015-12-14 21:17:15.351711
32	13	3	test	2015-12-14 21:21:10.477132
33	13	3	test2\n	2015-12-14 21:39:46.426541
34	13	3	tttt	2015-12-14 21:44:34.435696
35	13	3	tetetete	2015-12-14 21:44:47.327799
36	13	3	t	2015-12-14 21:45:01.636587
37	13	3	dodana poruka	2015-12-14 21:46:38.482198
38	4	7	mujina poruka	2015-12-14 21:53:11.309809
39	4	7	mujina poruka	2015-12-14 21:53:17.841381
40	13	3	dddddddddddddddddddddddddddddddddddduuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuugaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaccccccka	2015-12-14 21:54:03.385722
41	1	2	ja bi na kavu	2015-12-14 21:57:34.731452
43	13	2	ma di je dodana poruka?	2015-12-14 21:59:36.363364
44	13	3	dawdwadwakdwaikdwaddddddddddddddddddddddddddddddddddddddddddddddddddddddwd aw wad a ad aw	2015-12-14 22:10:50.748009
45	13	3	dwaijdwad awd dawdwa f fwawa ef fse fsesf sef sef sefse fse fse fsgs ga wd da b vsfe  foaf aoawfaa	2015-12-14 22:15:01.042933
46	13	3	Napisi poruku:Predjednik SDP-a Zoran Milanović formalno je u ponedjeljak odgovorio na prijedloge Mosta vezane za konstituiranje Hrvatskog sabora i formiranje buduće hrvatske Vlade. Kao što je poznato, Most zahtijeva tripartitnu Vladu na čelu s nestranačkim premijerom, a da SDP takav prijedlog odbija bilo je poznato već u subotu, no u ponedjeljak je Milanović osobno pisao Mostu kako bi ih obavijestio o odbijenici.	2015-12-14 22:22:50.38323
47	13	2	vekszorp kakvi su to politički razgovori?	2015-12-14 22:23:51.133047
48	5	2	evo još jedna	2015-12-14 22:45:47.216661
49	5	2	ma još	2015-12-14 22:46:00.225804
50	5	2	ma još	2015-12-14 22:47:39.211087
51	13	3	dugackaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa rijec rijec rijec daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa rijec aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa ddddd	2015-12-14 22:49:27.425682
52	5	2	jel ima greška?	2015-12-14 22:50:20.125546
53	13	3	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa ddddddddddddddddd ffef 	2015-12-14 22:50:31.515119
54	13	3	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa ffffff	2015-12-14 22:50:43.371483
55	13	3	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaafffffff	2015-12-14 22:51:05.252149
56	13	3	aaaaaaaaaaaaaaaaaaaaaaaaaaaaa\naaaaaaaaaaaaaaaaaaaaa fffffffff	2015-12-14 22:51:17.820607
57	13	3	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa fffffff	2015-12-14 22:51:41.092741
58	13	3	odlicno, radi!	2015-12-14 22:51:52.077018
59	13	3	testiranje prelamanja dugackih rijeci uspjesno	2015-12-14 22:52:17.437761
60	5	2	pokreće greške	2015-12-14 22:54:31.306289
61	5	2	ajmoooo	2015-12-14 22:58:11.485816
62	13	2	kak prelamaš duge riječi?	2015-12-14 23:14:39.710456
63	4	7	u lalalalallalala	2015-12-14 23:15:35.127692
64	4	2	radiiii	2015-12-14 23:16:47.375202
65	13	3	ima li te zlatko??	2015-12-14 23:22:52.764782
66	13	3	Napisi poruku:	2015-12-14 23:24:59.10489
67	13	2	kakvu?	2015-12-14 23:26:35.148964
68	13	3	jos jedan bug :D	2015-12-14 23:26:48.762741
69	13	2	hehe ja imam pun kufer stvari, jbm line breaking ne radi	2015-12-14 23:27:21.459259
70	13	3	jesi probao reverse engineerat ono sto sam ti poslao	2015-12-14 23:28:01.316743
71	13	2	ma ima kako napravit u cssu al ne radi	2015-12-14 23:29:26.930003
72	13	3	aha da to je skroz drugo podrucje	2015-12-14 23:30:36.408806
73	13	2	jap	2015-12-14 23:30:52.866215
74	13	2	begam sad vekszorp	2015-12-14 23:32:08.052923
75	13	3	ajde lijep pozdrav	2015-12-14 23:32:17.139875
76	13	2	ćao	2015-12-14 23:33:18.262954
77	5	2	hmmm	2015-12-14 23:33:35.450146
78	2	2	kakav sastanak?	2015-12-14 23:33:56.524109
79	13	3	test	2015-12-15 00:38:16.679645
80	13	3	ayyy	2015-12-15 01:19:31.252895
81	13	3	ajmo se kladit da radi	2015-12-15 01:35:33.54489
82	13	3	mozda	2015-12-15 01:36:56.825037
83	13	3	radi ali se baca neki IllegalStateException, wut	2015-12-15 01:37:49.288725
84	13	3	ddd	2015-12-15 01:40:12.819299
85	13	3	d\n	2015-12-15 01:40:28.579536
86	13	3	poruka1	2015-12-15 01:43:09.601129
87	13	3	poruka2	2015-12-15 01:43:21.689013
88	13	3	j	2015-12-15 01:45:02.123015
89	13	3	jk	2015-12-15 01:45:11.829534
90	13	3	k	2015-12-15 01:45:52.445654
91	13	3	dwadaw\n	2015-12-15 01:52:05.334385
92	13	3	ggsgssg	2015-12-15 01:52:25.239069
93	13	3	dadwdaadw	2015-12-15 01:54:52.5555
94	13	3	tititititit	2015-12-15 01:55:03.130933
95	13	3	dddd	2015-12-15 01:56:20.884909
96	13	3	nova poruka	2015-12-15 01:57:27.813049
97	13	3	g	2015-12-15 01:57:40.007262
98	13	3	dwadawdwa	2015-12-15 01:58:48.171639
99	13	3	j	2015-12-15 01:58:57.094758
100	13	3	kiki	2015-12-15 01:59:02.281812
101	13	3	jijij	2015-12-15 01:59:04.925109
102	13	3	sto je ovo	2015-12-15 01:59:14.633019
103	13	3	aaaa	2015-12-15 01:59:20.95377
104	13	3	dwadwaawdwad	2015-12-15 01:59:23.803005
105	13	3	g	2015-12-15 01:59:51.866873
106	13	3	de	2015-12-15 01:59:58.274786
107	13	3	g	2015-12-15 02:08:05.816735
108	13	3	jjjjjjjjj	2015-12-15 02:08:14.144353
109	13	3	n	2015-12-15 02:08:35.465761
110	13	3	d	2015-12-15 02:09:46.346965
111	13	3	poku	2015-12-15 02:09:55.795859
112	13	3	poruka	2015-12-15 02:12:13.611835
113	13	3	porukkk	2015-12-15 02:12:26.268767
114	13	3	p	2015-12-15 02:12:36.769618
115	13	3	d	2015-12-15 02:12:47.988639
116	13	3	d	2015-12-15 02:13:04.389972
117	13	3	tetete	2015-12-15 02:13:08.168794
118	13	3	dwadadaw	2015-12-15 02:13:10.090411
119	13	3	ne dolaze notifikacije....	2015-12-15 02:13:27.542832
120	13	3	rrrrr	2015-12-15 02:14:02.39782
121	13	3	d	2015-12-15 02:14:31.037507
122	13	3	f	2015-12-15 02:14:41.883973
123	13	3	f	2015-12-15 02:14:42.893587
124	13	3	f	2015-12-15 02:14:43.122144
125	13	3	f	2015-12-15 02:14:43.330133
126	13	3	fffff	2015-12-15 02:14:44.752016
127	13	3	d	2015-12-15 02:15:18.575408
128	13	3	ee	2015-12-15 02:15:24.035024
129	13	3	t	2015-12-15 02:15:27.900472
130	13	3	i	2015-12-15 02:15:37.465371
131	13	3	zasto salje po dvije	2015-12-15 02:15:46.200716
133	13	5	oj	2015-12-15 02:19:27.735604
134	13	5	ej	2015-12-15 02:24:02.482423
135	13	3	bok	2015-12-15 02:24:13.109905
136	13	3	bolje?	2015-12-15 02:25:28.383786
137	13	3	jee	2015-12-15 02:25:33.039831
138	13	3	dakle da	2015-12-15 02:26:14.35814
139	13	5	ej1	2015-12-15 02:26:36.209951
140	13	5	ej1	2015-12-15 02:27:05.131642
141	13	3	d	2015-12-15 02:27:17.863868
142	13	3	eeeee	2015-12-15 02:27:26.827709
143	13	3	daadwwadawd	2015-12-15 02:27:28.312866
144	13	3	daw	2015-12-15 02:27:29.661132
145	13	3	adw	2015-12-15 02:27:30.039903
146	13	3	ad	2015-12-15 02:27:30.410319
147	13	3	awd	2015-12-15 02:27:30.747123
148	13	3	ima koga	2015-12-15 02:28:19.945628
149	13	5	ej12	2015-12-15 02:28:40.700911
150	13	3	eee	2015-12-15 02:28:51.8342
151	13	5	ej12	2015-12-15 02:29:02.188885
152	13	3	da	2015-12-15 02:29:14.263419
153	13	3	hello	2015-12-15 13:41:40.703522
154	13	5	aj	2015-12-15 13:42:56.412176
155	13	5	aj	2015-12-15 13:44:16.071794
156	13	5	ej	2015-12-15 13:48:08.823023
157	13	5	ej	2015-12-15 13:49:16.073523
158	13	5	ej	2015-12-15 13:49:38.95625
159	13	5	ej	2015-12-15 13:50:04.984079
160	13	5	ej	2015-12-15 14:09:06.803596
161	13	5	ej2	2015-12-15 14:09:48.173698
162	13	5	ej2	2015-12-15 14:10:02.767901
163	13	5	ej2	2015-12-15 14:10:08.55475
164	13	5	ej2	2015-12-15 14:10:45.022249
165	13	5	ej2	2015-12-15 14:11:33.46919
166	13	5	ej2	2015-12-15 14:12:01.704427
167	13	5	ej2	2015-12-15 14:12:27.505394
168	13	5	ej2	2015-12-15 14:22:56.948688
169	13	5	ej2	2015-12-15 14:23:03.167726
170	13	5	ej2	2015-12-15 14:23:29.900898
171	13	5	ej2	2015-12-15 14:23:59.569316
172	13	5	ej2	2015-12-15 14:24:04.966825
173	13	5	ej2	2015-12-15 14:24:07.820543
174	13	5	ej2	2015-12-15 14:24:09.99599
175	13	5	ej3	2015-12-15 14:24:16.764071
176	13	5	aj	2015-12-15 14:29:34.251305
177	13	5	aj	2015-12-15 14:29:42.846506
178	13	5	aj	2015-12-15 14:29:52.773087
179	13	5	aj	2015-12-15 14:29:56.173688
180	13	5	aj	2015-12-15 14:30:05.652238
181	13	5	aj	2015-12-15 14:31:04.70356
182	13	5	aj	2015-12-15 14:31:16.56594
183	13	5	aj	2015-12-15 14:31:28.088459
184	13	5	aj	2015-12-15 14:31:34.463913
185	13	5	aj	2015-12-15 14:33:02.945019
186	13	5	aj	2015-12-15 14:33:10.914771
187	13	5	aj	2015-12-15 14:38:09.209322
188	13	5	aj	2015-12-15 14:38:09.373281
189	13	5	aj	2015-12-15 14:38:09.524296
190	13	5	aj	2015-12-15 14:38:09.675928
191	13	5	aj	2015-12-15 14:38:09.838723
192	13	5	aj	2015-12-15 14:38:17.978639
193	13	5	aj	2015-12-15 14:38:22.024523
194	13	5	aj	2015-12-15 14:38:25.575904
195	13	5	aj	2015-12-15 14:38:32.89692
196	13	5	aj	2015-12-15 14:38:38.571286
197	13	5	aj	2015-12-15 14:38:49.259831
198	13	5	aj	2015-12-15 14:38:49.405075
199	13	5	aj	2015-12-15 14:38:49.552344
200	13	5	aj	2015-12-15 14:38:49.688923
201	13	5	aj	2015-12-15 14:38:57.401608
202	13	5	aj	2015-12-15 14:39:00.701358
203	13	5	aj	2015-12-15 14:50:30.070807
204	13	5	aj	2015-12-15 14:50:33.520805
205	13	5	aj	2015-12-15 14:50:36.838537
206	13	5	aj	2015-12-15 14:50:39.638295
207	13	5	aj	2015-12-15 14:50:42.408125
208	13	5	aj	2015-12-15 14:50:42.583819
209	13	5	aj	2015-12-15 14:50:59.159458
210	13	3	d\n	2015-12-15 15:00:34.957865
211	13	3	dwadwda	2015-12-15 15:00:41.971869
212	13	2	veki čega si se ti napio?	2015-12-15 15:10:08.586513
213	13	2	:P	2015-12-15 15:14:58.065739
214	13	2	scroll radi?	2015-12-15 15:15:26.932918
215	2	2	?	2015-12-15 15:21:21.514485
216	13	2	radi :D	2015-12-15 15:22:17.422327
217	2	2	ajmooo	2015-12-15 15:29:28.78973
218	2	2	u ponedjeljak	2015-12-15 15:32:10.969134
219	13	3	hahahaha\n	2015-12-15 15:32:57.398867
220	13	2	kae :P	2015-12-15 15:33:57.259335
221	13	2	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	2015-12-15 15:34:21.696624
222	13	3	opet dretve ne rade aaaaaaaaaaaaaaaaaaa\n	2015-12-15 15:37:47.486087
223	13	3	test	2015-12-15 15:39:40.817075
224	13	3	oke ovo je bilo brzo	2015-12-15 15:39:53.107237
225	13	3	neki ljudi bi se vjerojatno zgrozili nacinom kako sam popravio probleme sa layoutom i dretvom	2015-12-15 15:40:28.394331
226	13	3	JComponent parent = this.getParent();	2015-12-15 15:40:59.327965
227	13	3	parent.remove(this);\nparent.add(new.....);	2015-12-15 15:41:29.812043
228	13	3	dwadwadw	2015-12-15 15:42:12.395384
229	13	3	dawdwawawda	2015-12-15 15:43:39.496733
230	13	2	wuhuuuu	2015-12-15 15:50:22.083297
231	13	3	dwadawdwa\ndwadw	2015-12-15 15:57:48.038563
232	13	2	moja poreuka	2015-12-15 15:58:30.204734
233	13	2	oće sad 3?	2015-12-15 15:58:55.3948
234	13	2	da vidimo	2015-12-15 16:05:12.702659
235	13	3	oce oceeeee\noceee	2015-12-15 16:10:51.3142
236	13	3	oce	2015-12-15 16:10:53.752399
237	13	3	ddddd\n\n\n	2015-12-15 16:10:57.803949
238	13	3	d	2015-12-15 16:10:58.477091
239	13	3	radiii	2015-12-15 16:11:01.565472
240	13	3	pppp\n\n\ndaw\n	2015-12-15 16:11:08.282712
241	13	3	tn tn tn t t\nt t t t t t	2015-12-15 16:15:53.978541
242	13	3	tnt tnt ttnt	2015-12-15 16:15:59.578773
243	4	2	njanjanaj	2015-12-15 16:16:05.114924
244	4	2	u lala	2015-12-15 16:16:15.422969
245	4	2	scroll	2015-12-15 16:23:31.898677
246	4	2	da vidimo	2015-12-15 16:27:10.660062
247	13	2	batmaaaaaan	2015-12-15 16:33:45.809781
248	13	3	saljem poruku u jazbinu	2015-12-15 16:34:42.890301
249	13	3	:DDDD	2015-12-15 16:34:59.48539
250	13	2	wuhuuuu	2015-12-15 16:35:45.246151
251	13	2	šaljem ja	2015-12-15 16:39:17.057929
252	13	2	opet ja	2015-12-15 16:39:38.90967
253	13	2		2015-12-15 16:43:22.321101
254	13	3	test\n	2015-12-15 16:45:33.033852
255	13	3	holy macarony	2015-12-15 16:45:43.109996
256	13	3	it works	2015-12-15 16:45:52.315142
257	13	2	baba manda stišće enter	2015-12-15 16:48:12.735927
258	13	2	a šta sada	2015-12-15 16:48:26.91982
259	4	2	radiiii	2015-12-15 16:49:55.555076
260	4	2	blablabal	2015-12-15 16:50:34.593193
261	4	2	blablabla	2015-12-15 16:50:53.040414
262	13	3	istinita prica	2015-12-15 16:51:07.400785
263	14	3	ajj	2015-12-15 16:54:37.705398
264	14	3	aaa	2015-12-15 16:54:42.055368
265	14	3	what is this	2015-12-15 16:54:54.462386
266	13	3	aj	2015-12-15 16:55:14.871643
267	13	3	jajajaja	2015-12-15 16:55:19.44214
268	14	3	d	2015-12-15 16:55:26.103951
269	14	3	d	2015-12-15 16:55:26.478865
270	14	3	d	2015-12-15 16:55:26.831178
271	14	3	d	2015-12-15 16:55:27.152042
272	14	3	is	2015-12-15 16:55:36.58275
273	14	3	si	2015-12-15 16:55:39.781948
274	14	3	ju	2015-12-15 16:55:43.064673
275	14	3	k	2015-12-15 16:56:04.023218
276	13	2	veki baš voliš ajajajajat	2015-12-15 17:24:51.341362
277	4	7	mujoooooooo hasooo o o o o o	2015-12-15 17:25:14.904052
278	13	2	?	2015-12-15 17:28:43.564481
279	13	2	moja poruka 	2015-12-15 17:29:06.946907
280	13	3	d	2015-12-15 17:58:26.094239
281	13	3	d	2015-12-15 18:17:12.513532
282	13	3	dg	2015-12-15 18:17:21.339851
283	13	3	tttt	2015-12-15 18:17:25.847381
284	13	3	a	2015-12-15 18:22:29.255204
285	13	3	napokon radi glupi scroll	2015-12-15 18:52:51.712308
286	13	3	nanananana	2015-12-15 18:52:54.319532
287	13	3	htio me unistit	2015-12-15 18:53:19.405517
288	13	3	ali ne ne ne	2015-12-15 18:53:21.723603
289	14	15	ayyyyyyyyyyyyyy	2015-12-15 18:58:13.253028
290	14	15	ayayayaya	2015-12-15 18:58:32.59015
291	14	15	1	2015-12-15 18:58:35.805488
292	14	15	2	2015-12-15 18:58:36.211047
293	14	15	3	2015-12-15 18:58:36.507132
294	14	15	4	2015-12-15 18:58:37.006611
295	14	15	super, nista ne nestaje	2015-12-15 18:58:46.648888
296	14	15	1	2015-12-15 18:58:50.921888
297	14	15	2	2015-12-15 18:58:51.168519
298	14	15	3	2015-12-15 18:58:51.423984
299	14	15	4	2015-12-15 18:58:51.648536
300	14	15	5	2015-12-15 18:58:51.889894
301	14	15	6	2015-12-15 18:58:52.117942
302	14	15	7	2015-12-15 18:58:52.37951
303	14	15	8	2015-12-15 18:58:52.737065
304	14	15	9	2015-12-15 18:58:52.996924
305	14	15	(y)	2015-12-15 18:58:57.144723
306	14	5	sad cu ti crashat razgovor	2015-12-15 19:04:51.950882
307	14	5	sad cu ti crashat razgovor	2015-12-15 19:06:04.844626
308	14	5	sad cu ti crashat razgovor	2015-12-15 19:11:46.004727
309	13	3	huhiuhiuh	2015-12-15 22:09:31.989647
310	13	3	8joijoij	2015-12-15 22:09:37.118886
311	13	3	iloijoi	2015-12-15 22:09:43.357203
\.


--
-- Name: poruka_idporuke_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('poruka_idporuke_seq', 311, true);


--
-- Data for Name: pripadnost_razgovoru; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pripadnost_razgovoru (idrazgovora, idsudionika, status) FROM stdin;
1	2	t
2	2	t
4	2	t
5	2	t
6	12	t
6	13	t
8	12	t
9	12	t
9	13	t
10	12	t
10	13	t
13	3	t
4	7	t
13	2	t
13	5	t
14	3	t
15	3	t
14	15	t
15	11	t
15	4	t
\.


--
-- Data for Name: razgovor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY razgovor (idrazgovora, imerazgovora, vrijemepocetka) FROM stdin;
1	Odlazak na palacinke	2015-11-14 10:19:23.085565
2	Dogovor  za sastanak	2015-11-14 10:34:10.696254
3	webici	2015-12-11 16:18:09.948779
4	Katarina ili Silvija	2015-12-11 16:46:07.258162
6	conv_tab_tel_-229	2015-12-12 20:38:11.729519
7	test1	2015-12-12 23:58:00.207527
8	test2	2015-12-13 00:29:10.60183
9	test3	2015-12-13 00:34:10.610135
10	test4	2015-12-13 00:38:33.446438
13	jazbina	2015-12-14 18:38:38.47582
5	Silvija ili Katarina	2015-12-11 16:47:01.574238
14	razgovor sa malo poruka	2015-12-15 16:54:04.767669
15	desktopasi	2015-12-15 18:54:24.476357
\.


--
-- Name: razgovor_idrazgovora_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('razgovor_idrazgovora_seq', 15, true);


--
-- Name: avatar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY avatar
    ADD CONSTRAINT avatar_pkey PRIMARY KEY (idavatar);


--
-- Name: bojafonta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bojafonta
    ADD CONSTRAINT bojafonta_pkey PRIMARY KEY (idboje);


--
-- Name: bojasucelja_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bojasucelja
    ADD CONSTRAINT bojasucelja_pkey PRIMARY KEY (idboje);


--
-- Name: font_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY font
    ADD CONSTRAINT font_pkey PRIMARY KEY (idfonta);


--
-- Name: kontakti_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kontakti
    ADD CONSTRAINT kontakti_pkey PRIMARY KEY (idvlasnika, idkontakta);


--
-- Name: korisnik_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_email_key UNIQUE (email);


--
-- Name: korisnik_korisnickoime_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_korisnickoime_key UNIQUE (korisnickoime);


--
-- Name: korisnik_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_pkey PRIMARY KEY (idkorisnika);


--
-- Name: poruka_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY poruka
    ADD CONSTRAINT poruka_pkey PRIMARY KEY (idporuke);


--
-- Name: pripadnost_razgovoru_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pripadnost_razgovoru
    ADD CONSTRAINT pripadnost_razgovoru_pkey PRIMARY KEY (idrazgovora, idsudionika);


--
-- Name: razgovor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY razgovor
    ADD CONSTRAINT razgovor_pkey PRIMARY KEY (idrazgovora);


--
-- Name: dodan_u_razgovor; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER dodan_u_razgovor AFTER INSERT ON pripadnost_razgovoru FOR EACH ROW EXECUTE PROCEDURE notify_dodan_u_razgovor();


--
-- Name: nova_poruka; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER nova_poruka AFTER INSERT ON poruka FOR EACH ROW EXECUTE PROCEDURE notify_nova_poruka();


--
-- Name: kontakti_idkontakta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kontakti
    ADD CONSTRAINT kontakti_idkontakta_fkey FOREIGN KEY (idkontakta) REFERENCES korisnik(idkorisnika);


--
-- Name: kontakti_idvlasnika_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kontakti
    ADD CONSTRAINT kontakti_idvlasnika_fkey FOREIGN KEY (idvlasnika) REFERENCES korisnik(idkorisnika);


--
-- Name: korisnik_idavatar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idavatar_fkey FOREIGN KEY (idavatar) REFERENCES avatar(idavatar);


--
-- Name: korisnik_idbojafonta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idbojafonta_fkey FOREIGN KEY (idbojafonta) REFERENCES bojafonta(idboje);


--
-- Name: korisnik_idbojasucelja_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idbojasucelja_fkey FOREIGN KEY (idbojasucelja) REFERENCES bojasucelja(idboje);


--
-- Name: korisnik_idfont_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idfont_fkey FOREIGN KEY (idfont) REFERENCES font(idfonta);


--
-- Name: poruka_idposiljatelja_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poruka
    ADD CONSTRAINT poruka_idposiljatelja_fkey FOREIGN KEY (idposiljatelja) REFERENCES korisnik(idkorisnika);


--
-- Name: poruka_idrazgovora_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poruka
    ADD CONSTRAINT poruka_idrazgovora_fkey FOREIGN KEY (idrazgovora) REFERENCES razgovor(idrazgovora);


--
-- Name: pripadnost_razgovoru_idrazgovora_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pripadnost_razgovoru
    ADD CONSTRAINT pripadnost_razgovoru_idrazgovora_fkey FOREIGN KEY (idrazgovora) REFERENCES razgovor(idrazgovora);


--
-- Name: pripadnost_razgovoru_idsudionika_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pripadnost_razgovoru
    ADD CONSTRAINT pripadnost_razgovoru_idsudionika_fkey FOREIGN KEY (idsudionika) REFERENCES korisnik(idkorisnika);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: avatar; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE avatar FROM PUBLIC;
REVOKE ALL ON TABLE avatar FROM postgres;
GRANT ALL ON TABLE avatar TO postgres;
GRANT SELECT,INSERT ON TABLE avatar TO defaultuser;


--
-- Name: avatar_idavatara_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE avatar_idavatara_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE avatar_idavatara_seq FROM postgres;
GRANT ALL ON SEQUENCE avatar_idavatara_seq TO postgres;
GRANT USAGE ON SEQUENCE avatar_idavatara_seq TO defaultuser;


--
-- Name: bojafonta; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE bojafonta FROM PUBLIC;
REVOKE ALL ON TABLE bojafonta FROM postgres;
GRANT ALL ON TABLE bojafonta TO postgres;
GRANT SELECT,INSERT ON TABLE bojafonta TO defaultuser;


--
-- Name: bojafonta_idboje_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE bojafonta_idboje_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bojafonta_idboje_seq FROM postgres;
GRANT ALL ON SEQUENCE bojafonta_idboje_seq TO postgres;
GRANT USAGE ON SEQUENCE bojafonta_idboje_seq TO defaultuser;


--
-- Name: bojasucelja; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE bojasucelja FROM PUBLIC;
REVOKE ALL ON TABLE bojasucelja FROM postgres;
GRANT ALL ON TABLE bojasucelja TO postgres;
GRANT SELECT,INSERT ON TABLE bojasucelja TO defaultuser;


--
-- Name: bojasucelja_idboje_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE bojasucelja_idboje_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bojasucelja_idboje_seq FROM postgres;
GRANT ALL ON SEQUENCE bojasucelja_idboje_seq TO postgres;
GRANT USAGE ON SEQUENCE bojasucelja_idboje_seq TO defaultuser;


--
-- Name: font; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE font FROM PUBLIC;
REVOKE ALL ON TABLE font FROM postgres;
GRANT ALL ON TABLE font TO postgres;
GRANT SELECT,INSERT ON TABLE font TO defaultuser;


--
-- Name: font_idfonta_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE font_idfonta_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE font_idfonta_seq FROM postgres;
GRANT ALL ON SEQUENCE font_idfonta_seq TO postgres;
GRANT USAGE ON SEQUENCE font_idfonta_seq TO defaultuser;


--
-- Name: kontakti; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE kontakti FROM PUBLIC;
REVOKE ALL ON TABLE kontakti FROM postgres;
GRANT ALL ON TABLE kontakti TO postgres;
GRANT SELECT,INSERT ON TABLE kontakti TO defaultuser;


--
-- Name: korisnik; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE korisnik FROM PUBLIC;
REVOKE ALL ON TABLE korisnik FROM postgres;
GRANT ALL ON TABLE korisnik TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE korisnik TO defaultuser;


--
-- Name: korisnik_idkorisnika_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE korisnik_idkorisnika_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE korisnik_idkorisnika_seq FROM postgres;
GRANT ALL ON SEQUENCE korisnik_idkorisnika_seq TO postgres;
GRANT USAGE ON SEQUENCE korisnik_idkorisnika_seq TO defaultuser;


--
-- Name: poruka; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE poruka FROM PUBLIC;
REVOKE ALL ON TABLE poruka FROM postgres;
GRANT ALL ON TABLE poruka TO postgres;
GRANT SELECT,INSERT ON TABLE poruka TO defaultuser;


--
-- Name: poruka_idporuke_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE poruka_idporuke_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE poruka_idporuke_seq FROM postgres;
GRANT ALL ON SEQUENCE poruka_idporuke_seq TO postgres;
GRANT USAGE ON SEQUENCE poruka_idporuke_seq TO defaultuser;


--
-- Name: pripadnost_razgovoru; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE pripadnost_razgovoru FROM PUBLIC;
REVOKE ALL ON TABLE pripadnost_razgovoru FROM postgres;
GRANT ALL ON TABLE pripadnost_razgovoru TO postgres;
GRANT SELECT,INSERT ON TABLE pripadnost_razgovoru TO defaultuser;


--
-- Name: razgovor; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE razgovor FROM PUBLIC;
REVOKE ALL ON TABLE razgovor FROM postgres;
GRANT ALL ON TABLE razgovor TO postgres;
GRANT SELECT,INSERT ON TABLE razgovor TO defaultuser;


--
-- Name: razgovor_idrazgovora_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE razgovor_idrazgovora_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE razgovor_idrazgovora_seq FROM postgres;
GRANT ALL ON SEQUENCE razgovor_idrazgovora_seq TO postgres;
GRANT USAGE ON SEQUENCE razgovor_idrazgovora_seq TO defaultuser;


--
-- PostgreSQL database dump complete
--

