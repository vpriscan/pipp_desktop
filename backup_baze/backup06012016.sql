--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: dodajkontakt(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dodajkontakt(_idvlasnika integer, _idkontakta integer, nadimak character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
  DECLARE postoji INTEGER;
	BEGIN
		INSERT INTO razgovor (imerazgovora) VALUES ($1);
		SELECT count(*) from kontakti where kontakti.idvlasnika=$1 and kontakti.idkontakta=$2 INTO postoji;

		IF postoji>0 
		THEN
			UPDATE kontakti SET status=true,nadimak=$3 where idvlasnika=$1 and idkontakta=$2;
		ELSE
			INSERT INTO kontakti (idvlasnika, idkontakta, nadimak) VALUES ($1, $2, $3);
		end if;
		RETURN postoji;
	END
	$_$;


ALTER FUNCTION public.dodajkontakt(_idvlasnika integer, _idkontakta integer, nadimak character varying) OWNER TO postgres;

--
-- Name: kreiraj_razgovor(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION kreiraj_razgovor(ime character varying, idkreatora integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
  DECLARE novi_id INTEGER;
	BEGIN
		INSERT INTO razgovor (imerazgovora) VALUES ($1);
		SELECT currval(pg_get_serial_sequence('razgovor','idrazgovora')) INTO novi_id;
		
		INSERT INTO pripadnost_razgovoru (idrazgovora,idsudionika,status) values (novi_id,$2,true);
		
		
	
		RETURN novi_id;
	END
	$_$;


ALTER FUNCTION public.kreiraj_razgovor(ime character varying, idkreatora integer) OWNER TO postgres;

--
-- Name: notify_dodan_u_razgovor(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION notify_dodan_u_razgovor() RETURNS trigger
    LANGUAGE plpgsql IMMUTABLE
    AS $$
DECLARE 
	id integer;
BEGIN
	FOR id in SELECT idsudionika FROM pripadnost_razgovoru WHERE idrazgovora=NEW.idrazgovora AND idsudionika<>NEW.idsudionika AND status=TRUE
	LOOP 
		PERFORM pg_catalog.pg_notify('obavijesti_' || id::text || '_o_promjenama_u_razgovoru' , NEW.idrazgovora::text);
		-- obavijesti ostale korisnike da je netko dodan u razgovor (ista notifikacija ce se slat i kad netko drugi izadje iz razgovora i kod promjene naziva)
	END LOOP;
	PERFORM pg_catalog.pg_notify('obavijesti_' || NEW.idsudionika::text || '_o_novoj_poruci_ili_razgovoru', NEW.idrazgovora::text);
	-- obavijesti dodanog korisnika u razgovor
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.notify_dodan_u_razgovor() OWNER TO postgres;

--
-- Name: notify_izasao_iz_razgovora(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION notify_izasao_iz_razgovora() RETURNS trigger
    LANGUAGE plpgsql IMMUTABLE
    AS $$
DECLARE 
	id integer;
BEGIN
	FOR id in SELECT idsudionika FROM pripadnost_razgovoru WHERE idrazgovora=NEW.idrazgovora AND idsudionika<>NEW.idsudionika AND status=TRUE
	LOOP 
		PERFORM pg_catalog.pg_notify('obavijesti_' || id::text || '_o_promjenama_u_razgovoru' , NEW.idrazgovora::text);
		-- obavijesti ostale korisnike da je netko izasao iz razgovora (ista notifikacija ce se slat i kad netko drugi udje u razgovor i kod promjene naziva)
	END LOOP;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.notify_izasao_iz_razgovora() OWNER TO postgres;

--
-- Name: notify_nova_poruka(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION notify_nova_poruka() RETURNS trigger
    LANGUAGE plpgsql IMMUTABLE
    AS $$
DECLARE 
	id integer;
BEGIN
	FOR id in SELECT idsudionika FROM pripadnost_razgovoru WHERE idrazgovora=NEW.idrazgovora AND status=TRUE
	LOOP 
		PERFORM pg_catalog.pg_notify('obavijesti_' || id::text || '_o_novoj_poruci_ili_razgovoru', NEW.idrazgovora::text);
	END LOOP;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.notify_nova_poruka() OWNER TO postgres;

--
-- Name: notify_promjena_naziva_razgovora(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION notify_promjena_naziva_razgovora() RETURNS trigger
    LANGUAGE plpgsql IMMUTABLE
    AS $$
DECLARE 
	id integer;
BEGIN
	FOR id in SELECT idsudionika FROM pripadnost_razgovoru WHERE idrazgovora=NEW.idrazgovora AND status=TRUE
	LOOP 
		PERFORM pg_catalog.pg_notify('obavijesti_' || id::text || '_o_promjenama_u_razgovoru' , NEW.idrazgovora::text);
		-- obavijesti sve korisnike da je promijenjeno ime razgovora
	END LOOP;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.notify_promjena_naziva_razgovora() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: avatar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE avatar (
    idavatar integer NOT NULL,
    lokacijaavatara character varying(200) NOT NULL,
    imeavatara character varying(50) NOT NULL
);


ALTER TABLE avatar OWNER TO postgres;

--
-- Name: avatar_idavatara_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE avatar_idavatara_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE avatar_idavatara_seq OWNER TO postgres;

--
-- Name: avatar_idavatara_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE avatar_idavatara_seq OWNED BY avatar.idavatar;


--
-- Name: bojafonta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bojafonta (
    idboje integer NOT NULL,
    imeboje character varying(20),
    rgb_r integer NOT NULL,
    rgb_g integer NOT NULL,
    rgb_b integer NOT NULL,
    CONSTRAINT bojafonta_rgb_b_check CHECK (((rgb_b >= 0) AND (rgb_b <= 255))),
    CONSTRAINT bojafonta_rgb_g_check CHECK (((rgb_g >= 0) AND (rgb_g <= 255))),
    CONSTRAINT bojafonta_rgb_r_check CHECK (((rgb_r >= 0) AND (rgb_r <= 255)))
);


ALTER TABLE bojafonta OWNER TO postgres;

--
-- Name: bojafonta_idboje_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bojafonta_idboje_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bojafonta_idboje_seq OWNER TO postgres;

--
-- Name: bojafonta_idboje_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bojafonta_idboje_seq OWNED BY bojafonta.idboje;


--
-- Name: bojasucelja; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bojasucelja (
    idboje integer NOT NULL,
    imeboje character varying(20),
    rgb_r integer NOT NULL,
    rgb_g integer NOT NULL,
    rgb_b integer NOT NULL,
    CONSTRAINT bojasucelja_rgb_b_check CHECK (((rgb_b >= 0) AND (rgb_b <= 255))),
    CONSTRAINT bojasucelja_rgb_g_check CHECK (((rgb_g >= 0) AND (rgb_g <= 255))),
    CONSTRAINT bojasucelja_rgb_r_check CHECK (((rgb_r >= 0) AND (rgb_r <= 255)))
);


ALTER TABLE bojasucelja OWNER TO postgres;

--
-- Name: bojasucelja_idboje_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bojasucelja_idboje_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bojasucelja_idboje_seq OWNER TO postgres;

--
-- Name: bojasucelja_idboje_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bojasucelja_idboje_seq OWNED BY bojasucelja.idboje;


--
-- Name: font; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE font (
    idfonta integer NOT NULL,
    imefonta character varying(20),
    velicinafonta integer DEFAULT 20 NOT NULL,
    stilfonta character(6) DEFAULT 'plain'::bpchar NOT NULL,
    CONSTRAINT font_stilfonta_check CHECK ((((lower((stilfonta)::text) = 'plain'::text) OR (lower((stilfonta)::text) = 'italic'::text)) OR (lower((stilfonta)::text) = 'bold'::text))),
    CONSTRAINT font_velicinafonta_check CHECK (((velicinafonta >= 10) AND (velicinafonta <= 30)))
);


ALTER TABLE font OWNER TO postgres;

--
-- Name: CONSTRAINT font_stilfonta_check ON font; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT font_stilfonta_check ON font IS 'Ako dodajemo novi font, provjerava jesmo li napisali stil fonta ''plain'', ''italic'', ili ''bold'', inace je greska';


--
-- Name: CONSTRAINT font_velicinafonta_check ON font; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT font_velicinafonta_check ON font IS 'ako dodajemo novi font provjerava nalazi li se velicina fonta u zadanom intervalu';


--
-- Name: font_idfonta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE font_idfonta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE font_idfonta_seq OWNER TO postgres;

--
-- Name: font_idfonta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE font_idfonta_seq OWNED BY font.idfonta;


--
-- Name: kontakti; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kontakti (
    idvlasnika integer NOT NULL,
    idkontakta integer NOT NULL,
    status boolean DEFAULT true NOT NULL,
    nadimak character varying(40)
);


ALTER TABLE kontakti OWNER TO postgres;

--
-- Name: korisnik; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE korisnik (
    idkorisnika integer NOT NULL,
    korisnickoime character varying(20) NOT NULL,
    lozinka character varying(40) NOT NULL,
    aktivan boolean DEFAULT true NOT NULL,
    email character varying(50) NOT NULL,
    idbojafonta integer DEFAULT 1 NOT NULL,
    idfont integer DEFAULT 1 NOT NULL,
    idavatar integer DEFAULT 1 NOT NULL,
    idbojasucelja integer DEFAULT 1 NOT NULL
);


ALTER TABLE korisnik OWNER TO postgres;

--
-- Name: korisnik_idkorisnika_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE korisnik_idkorisnika_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE korisnik_idkorisnika_seq OWNER TO postgres;

--
-- Name: korisnik_idkorisnika_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE korisnik_idkorisnika_seq OWNED BY korisnik.idkorisnika;


--
-- Name: poruka; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE poruka (
    idporuke integer NOT NULL,
    idrazgovora integer NOT NULL,
    idposiljatelja integer NOT NULL,
    tekst text NOT NULL,
    vrijemeslanja timestamp without time zone DEFAULT timezone('CET'::text, now()) NOT NULL
);


ALTER TABLE poruka OWNER TO postgres;

--
-- Name: poruka_idporuke_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE poruka_idporuke_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE poruka_idporuke_seq OWNER TO postgres;

--
-- Name: poruka_idporuke_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE poruka_idporuke_seq OWNED BY poruka.idporuke;


--
-- Name: pripadnost_razgovoru; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pripadnost_razgovoru (
    idrazgovora integer NOT NULL,
    idsudionika integer NOT NULL,
    status boolean DEFAULT true NOT NULL
);


ALTER TABLE pripadnost_razgovoru OWNER TO postgres;

--
-- Name: razgovor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE razgovor (
    idrazgovora integer NOT NULL,
    imerazgovora character varying(30),
    vrijemepocetka timestamp without time zone DEFAULT timezone('CET'::text, now()) NOT NULL
);


ALTER TABLE razgovor OWNER TO postgres;

--
-- Name: razgovor_idrazgovora_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE razgovor_idrazgovora_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE razgovor_idrazgovora_seq OWNER TO postgres;

--
-- Name: razgovor_idrazgovora_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE razgovor_idrazgovora_seq OWNED BY razgovor.idrazgovora;


--
-- Name: idavatar; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avatar ALTER COLUMN idavatar SET DEFAULT nextval('avatar_idavatara_seq'::regclass);


--
-- Name: idboje; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bojafonta ALTER COLUMN idboje SET DEFAULT nextval('bojafonta_idboje_seq'::regclass);


--
-- Name: idboje; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bojasucelja ALTER COLUMN idboje SET DEFAULT nextval('bojasucelja_idboje_seq'::regclass);


--
-- Name: idfonta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY font ALTER COLUMN idfonta SET DEFAULT nextval('font_idfonta_seq'::regclass);


--
-- Name: idkorisnika; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik ALTER COLUMN idkorisnika SET DEFAULT nextval('korisnik_idkorisnika_seq'::regclass);


--
-- Name: idporuke; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poruka ALTER COLUMN idporuke SET DEFAULT nextval('poruka_idporuke_seq'::regclass);


--
-- Name: idrazgovora; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY razgovor ALTER COLUMN idrazgovora SET DEFAULT nextval('razgovor_idrazgovora_seq'::regclass);


--
-- Data for Name: avatar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY avatar (idavatar, lokacijaavatara, imeavatara) FROM stdin;
1	http://i.imgur.com/PlbgSFk.jpg	banana
2	http://images.clipartpanda.com/watermelon-clip-art-watermelon_slice.svg	lubenica
3	https://stephanieallison.files.wordpress.com/2010/08/single-small-pear5b15d.jpg	crtez kruske
4	http://i.imgur.com/5krrodq.png	borovnica
5	http://i.imgur.com/ZjEgXF2.jpg	malina
6	http://i.imgur.com/eau0ZBF.jpg	naranca
7	http://i.imgur.com/HTy0dXg.jpg	tresnja
8	http://i.imgur.com/VXyLl7y.png	lubenica2
\.


--
-- Name: avatar_idavatara_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('avatar_idavatara_seq', 8, true);


--
-- Data for Name: bojafonta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bojafonta (idboje, imeboje, rgb_r, rgb_g, rgb_b) FROM stdin;
1	crna	0	0	0
2	bijela	255	255	255
3	siva	51	51	51
\.


--
-- Name: bojafonta_idboje_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bojafonta_idboje_seq', 3, true);


--
-- Data for Name: bojasucelja; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bojasucelja (idboje, imeboje, rgb_r, rgb_g, rgb_b) FROM stdin;
1	plava	0	0	255
2	Nezrela banana	204	255	51
3	Baby pink	255	199	236
4	Lavender	229	199	255
5	Purple wedding	65	7	34
\.


--
-- Name: bojasucelja_idboje_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bojasucelja_idboje_seq', 5, true);


--
-- Data for Name: font; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY font (idfonta, imefonta, velicinafonta, stilfonta) FROM stdin;
2	Arial	15	bold  
1	Times New Roman	20	plain 
3	DejaVu Sans	20	italic
5	Comic Sans MS	19	plain 
6	Serif	14	bold  
7	Verdana	14	bold  
8	Georgia	15	bold  
4	Old London	18	plain 
\.


--
-- Name: font_idfonta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('font_idfonta_seq', 9, true);


--
-- Data for Name: kontakti; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kontakti (idvlasnika, idkontakta, status, nadimak) FROM stdin;
13	12	t	telefon
12	13	t	tablet
14	12	t	marin1
14	13	t	marin2
17	12	t	Marin
18	17	t	Miko123
17	18	t	sarma
12	14	t	marin3
3	3	f	\N
3	18	t	sm
3	12	t	mar
3	5	f	kizo
3	11	f	a
3	16	f	vladek
2	3	t	Veki
3	2	t	zlatkec
3	17	t	micika
3	19	t	katica
2	7	t	Mujica
3	8	t	oto
3	15	t	ja
4	3	t	Veki
3	4	t	marica
2	12	t	marin
2	13	f	marin2
2	4	t	mare
\.


--
-- Data for Name: korisnik; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY korisnik (idkorisnika, korisnickoime, lozinka, aktivan, email, idbojafonta, idfont, idavatar, idbojasucelja) FROM stdin;
17	miko	de76d35871e1e90759e6acb6234e58f3481fab3	t	damjan7miko@gmail.com	1	1	1	1
18	sarma	29477380a8a3a7732d26af0abe0262db904d0e7b	t	sarma@fer.com	1	1	1	1
5	zoran.karamarko	d9dd7135972df03ac7eae9c8c2c418abbe1745e6	t	zoran.karamarko@gmail.com	1	1	1	1
7	mujo	9633ef86687fa9565c7641b22344927e785c5250	t	mujo@mujo.hr	1	1	1	1
8	ottosinger	AF933EAD2B9BDDBE90F7BB47C6D637C4E654F58E	t	otto.singer@fer.hr	1	1	1	1
9	vucko	8c6a06670992025c8c8f6c8bd3a7db418bce3039	t	vucko@vucko.hr	1	1	1	1
4	mkalebota	648c06b8459ba15d459f13e369847704815207d	t	marija.kalebota@fer.hr	2	7	5	5
11	Agneza	b687ea9353a11e817a77bfaea0a16194ab5f8307	t	agneza.sandic@fer.hr	2	4	3	3
12	marin	70c881d4a26984ddce795f6f71817c9cf4480e79	t	marin@email.com	1	1	1	1
13	marin2	70c881d4a26984ddce795f6f71817c9cf4480e79	t	marin2@email.com	1	1	1	1
14	marin3	70c881d4a26984ddce795f6f71817c9cf4480e79	t	marin3@email.com	1	1	1	1
10	pero	98FC7B34760FACE5E268BFF318180E05861A970F	t	pero@pero.pero	1	2	1	1
19	katarina	A655AD068BA9EB1FDB57BCC5A898D18775163178	t	katarina.plantic@fer.hr	3	5	5	4
15	prozskev	d688ae4face9f51ed4844f51c609fc9762900535	t	lazni_email@tvz.hr	2	7	1	2
16	vlazorp	ca3933b8e809c7a6cc820e03c277d13941322b0e	t	wuiegf@gmail.com	3	2	3	3
2	zlatko.hrastic	5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8	t	zlatko.hrastic@fer.hr	1	6	7	2
3	vekszorp	b446080ccff9f99de750fac9db98c7cde151fd8e	t	vedran.priscan@fer.hr	1	7	1	4
\.


--
-- Name: korisnik_idkorisnika_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('korisnik_idkorisnika_seq', 19, true);


--
-- Data for Name: poruka; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY poruka (idporuke, idrazgovora, idposiljatelja, tekst, vrijemeslanja) FROM stdin;
1	1	2	tekst poruke	2015-11-14 10:21:41.720219
2	5	2	bla	2015-12-11 18:06:17.407231
4	5	2	molim te radi	2015-12-11 18:38:00.550686
5	5	2	katarian ili Silvija?	2015-12-11 18:48:51.444645
6	5	2	katarian ili Silvija?	2015-12-11 18:49:12.620277
7	5	2	katarian ili Silvija?bfdsbdsf	2015-12-11 18:49:21.203742
8	5	2	vani je	2015-12-11 18:55:11.740456
9	5	2	sretni smo	2015-12-11 19:01:01.326828
11	10	12	prva por	2015-12-13 00:38:34.246505
20	13	3	moja prva poruka	2015-12-14 18:41:39.654201
21	13	3	hello	2015-12-14 19:53:06.144721
22	5	2	još jedna	2015-12-14 20:08:23.191344
23	1	2	još jedna	2015-12-14 20:08:37.851791
24	5	2	keeee?	2015-12-14 20:09:56.234168
25	1	2	ova sad mora bit zadnja	2015-12-14 20:16:28.547982
26	4	2	drugi razgovor	2015-12-14 20:32:04.053537
27	13	3	hi\n	2015-12-14 20:58:47.474545
28	13	3	je li nevidljiva poruka zapravo poruka?	2015-12-14 21:01:02.628875
29	5	2	nova poruka	2015-12-14 21:02:31.194472
30	13	3	visoka\nviisoka\nporukaa\naaaa\na	2015-12-14 21:16:52.806161
31	13	3	testić	2015-12-14 21:17:15.351711
32	13	3	test	2015-12-14 21:21:10.477132
33	13	3	test2\n	2015-12-14 21:39:46.426541
34	13	3	tttt	2015-12-14 21:44:34.435696
35	13	3	tetetete	2015-12-14 21:44:47.327799
36	13	3	t	2015-12-14 21:45:01.636587
37	13	3	dodana poruka	2015-12-14 21:46:38.482198
38	4	7	mujina poruka	2015-12-14 21:53:11.309809
39	4	7	mujina poruka	2015-12-14 21:53:17.841381
40	13	3	dddddddddddddddddddddddddddddddddddduuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuugaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaccccccka	2015-12-14 21:54:03.385722
41	1	2	ja bi na kavu	2015-12-14 21:57:34.731452
43	13	2	ma di je dodana poruka?	2015-12-14 21:59:36.363364
44	13	3	dawdwadwakdwaikdwaddddddddddddddddddddddddddddddddddddddddddddddddddddddwd aw wad a ad aw	2015-12-14 22:10:50.748009
45	13	3	dwaijdwad awd dawdwa f fwawa ef fse fsesf sef sef sefse fse fse fsgs ga wd da b vsfe  foaf aoawfaa	2015-12-14 22:15:01.042933
46	13	3	Napisi poruku:Predjednik SDP-a Zoran Milanović formalno je u ponedjeljak odgovorio na prijedloge Mosta vezane za konstituiranje Hrvatskog sabora i formiranje buduće hrvatske Vlade. Kao što je poznato, Most zahtijeva tripartitnu Vladu na čelu s nestranačkim premijerom, a da SDP takav prijedlog odbija bilo je poznato već u subotu, no u ponedjeljak je Milanović osobno pisao Mostu kako bi ih obavijestio o odbijenici.	2015-12-14 22:22:50.38323
47	13	2	vekszorp kakvi su to politički razgovori?	2015-12-14 22:23:51.133047
48	5	2	evo još jedna	2015-12-14 22:45:47.216661
49	5	2	ma još	2015-12-14 22:46:00.225804
50	5	2	ma još	2015-12-14 22:47:39.211087
51	13	3	dugackaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa rijec rijec rijec daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa rijec aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa ddddd	2015-12-14 22:49:27.425682
52	5	2	jel ima greška?	2015-12-14 22:50:20.125546
53	13	3	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa ddddddddddddddddd ffef 	2015-12-14 22:50:31.515119
54	13	3	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa ffffff	2015-12-14 22:50:43.371483
55	13	3	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaafffffff	2015-12-14 22:51:05.252149
56	13	3	aaaaaaaaaaaaaaaaaaaaaaaaaaaaa\naaaaaaaaaaaaaaaaaaaaa fffffffff	2015-12-14 22:51:17.820607
57	13	3	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa fffffff	2015-12-14 22:51:41.092741
58	13	3	odlicno, radi!	2015-12-14 22:51:52.077018
59	13	3	testiranje prelamanja dugackih rijeci uspjesno	2015-12-14 22:52:17.437761
60	5	2	pokreće greške	2015-12-14 22:54:31.306289
61	5	2	ajmoooo	2015-12-14 22:58:11.485816
62	13	2	kak prelamaš duge riječi?	2015-12-14 23:14:39.710456
63	4	7	u lalalalallalala	2015-12-14 23:15:35.127692
64	4	2	radiiii	2015-12-14 23:16:47.375202
65	13	3	ima li te zlatko??	2015-12-14 23:22:52.764782
66	13	3	Napisi poruku:	2015-12-14 23:24:59.10489
67	13	2	kakvu?	2015-12-14 23:26:35.148964
68	13	3	jos jedan bug :D	2015-12-14 23:26:48.762741
69	13	2	hehe ja imam pun kufer stvari, jbm line breaking ne radi	2015-12-14 23:27:21.459259
70	13	3	jesi probao reverse engineerat ono sto sam ti poslao	2015-12-14 23:28:01.316743
71	13	2	ma ima kako napravit u cssu al ne radi	2015-12-14 23:29:26.930003
72	13	3	aha da to je skroz drugo podrucje	2015-12-14 23:30:36.408806
73	13	2	jap	2015-12-14 23:30:52.866215
74	13	2	begam sad vekszorp	2015-12-14 23:32:08.052923
75	13	3	ajde lijep pozdrav	2015-12-14 23:32:17.139875
76	13	2	ćao	2015-12-14 23:33:18.262954
77	5	2	hmmm	2015-12-14 23:33:35.450146
78	2	2	kakav sastanak?	2015-12-14 23:33:56.524109
79	13	3	test	2015-12-15 00:38:16.679645
80	13	3	ayyy	2015-12-15 01:19:31.252895
81	13	3	ajmo se kladit da radi	2015-12-15 01:35:33.54489
82	13	3	mozda	2015-12-15 01:36:56.825037
83	13	3	radi ali se baca neki IllegalStateException, wut	2015-12-15 01:37:49.288725
84	13	3	ddd	2015-12-15 01:40:12.819299
85	13	3	d\n	2015-12-15 01:40:28.579536
86	13	3	poruka1	2015-12-15 01:43:09.601129
87	13	3	poruka2	2015-12-15 01:43:21.689013
88	13	3	j	2015-12-15 01:45:02.123015
89	13	3	jk	2015-12-15 01:45:11.829534
90	13	3	k	2015-12-15 01:45:52.445654
91	13	3	dwadaw\n	2015-12-15 01:52:05.334385
92	13	3	ggsgssg	2015-12-15 01:52:25.239069
93	13	3	dadwdaadw	2015-12-15 01:54:52.5555
94	13	3	tititititit	2015-12-15 01:55:03.130933
95	13	3	dddd	2015-12-15 01:56:20.884909
96	13	3	nova poruka	2015-12-15 01:57:27.813049
97	13	3	g	2015-12-15 01:57:40.007262
98	13	3	dwadawdwa	2015-12-15 01:58:48.171639
99	13	3	j	2015-12-15 01:58:57.094758
100	13	3	kiki	2015-12-15 01:59:02.281812
101	13	3	jijij	2015-12-15 01:59:04.925109
102	13	3	sto je ovo	2015-12-15 01:59:14.633019
103	13	3	aaaa	2015-12-15 01:59:20.95377
104	13	3	dwadwaawdwad	2015-12-15 01:59:23.803005
105	13	3	g	2015-12-15 01:59:51.866873
106	13	3	de	2015-12-15 01:59:58.274786
107	13	3	g	2015-12-15 02:08:05.816735
108	13	3	jjjjjjjjj	2015-12-15 02:08:14.144353
109	13	3	n	2015-12-15 02:08:35.465761
110	13	3	d	2015-12-15 02:09:46.346965
111	13	3	poku	2015-12-15 02:09:55.795859
112	13	3	poruka	2015-12-15 02:12:13.611835
113	13	3	porukkk	2015-12-15 02:12:26.268767
114	13	3	p	2015-12-15 02:12:36.769618
115	13	3	d	2015-12-15 02:12:47.988639
116	13	3	d	2015-12-15 02:13:04.389972
117	13	3	tetete	2015-12-15 02:13:08.168794
118	13	3	dwadadaw	2015-12-15 02:13:10.090411
119	13	3	ne dolaze notifikacije....	2015-12-15 02:13:27.542832
120	13	3	rrrrr	2015-12-15 02:14:02.39782
121	13	3	d	2015-12-15 02:14:31.037507
122	13	3	f	2015-12-15 02:14:41.883973
123	13	3	f	2015-12-15 02:14:42.893587
124	13	3	f	2015-12-15 02:14:43.122144
125	13	3	f	2015-12-15 02:14:43.330133
126	13	3	fffff	2015-12-15 02:14:44.752016
127	13	3	d	2015-12-15 02:15:18.575408
128	13	3	ee	2015-12-15 02:15:24.035024
129	13	3	t	2015-12-15 02:15:27.900472
130	13	3	i	2015-12-15 02:15:37.465371
131	13	3	zasto salje po dvije	2015-12-15 02:15:46.200716
133	13	5	oj	2015-12-15 02:19:27.735604
134	13	5	ej	2015-12-15 02:24:02.482423
135	13	3	bok	2015-12-15 02:24:13.109905
136	13	3	bolje?	2015-12-15 02:25:28.383786
137	13	3	jee	2015-12-15 02:25:33.039831
138	13	3	dakle da	2015-12-15 02:26:14.35814
139	13	5	ej1	2015-12-15 02:26:36.209951
140	13	5	ej1	2015-12-15 02:27:05.131642
141	13	3	d	2015-12-15 02:27:17.863868
142	13	3	eeeee	2015-12-15 02:27:26.827709
143	13	3	daadwwadawd	2015-12-15 02:27:28.312866
144	13	3	daw	2015-12-15 02:27:29.661132
145	13	3	adw	2015-12-15 02:27:30.039903
146	13	3	ad	2015-12-15 02:27:30.410319
147	13	3	awd	2015-12-15 02:27:30.747123
148	13	3	ima koga	2015-12-15 02:28:19.945628
149	13	5	ej12	2015-12-15 02:28:40.700911
150	13	3	eee	2015-12-15 02:28:51.8342
151	13	5	ej12	2015-12-15 02:29:02.188885
152	13	3	da	2015-12-15 02:29:14.263419
153	13	3	hello	2015-12-15 13:41:40.703522
154	13	5	aj	2015-12-15 13:42:56.412176
155	13	5	aj	2015-12-15 13:44:16.071794
156	13	5	ej	2015-12-15 13:48:08.823023
157	13	5	ej	2015-12-15 13:49:16.073523
158	13	5	ej	2015-12-15 13:49:38.95625
159	13	5	ej	2015-12-15 13:50:04.984079
160	13	5	ej	2015-12-15 14:09:06.803596
161	13	5	ej2	2015-12-15 14:09:48.173698
162	13	5	ej2	2015-12-15 14:10:02.767901
163	13	5	ej2	2015-12-15 14:10:08.55475
164	13	5	ej2	2015-12-15 14:10:45.022249
165	13	5	ej2	2015-12-15 14:11:33.46919
166	13	5	ej2	2015-12-15 14:12:01.704427
167	13	5	ej2	2015-12-15 14:12:27.505394
168	13	5	ej2	2015-12-15 14:22:56.948688
169	13	5	ej2	2015-12-15 14:23:03.167726
170	13	5	ej2	2015-12-15 14:23:29.900898
171	13	5	ej2	2015-12-15 14:23:59.569316
172	13	5	ej2	2015-12-15 14:24:04.966825
173	13	5	ej2	2015-12-15 14:24:07.820543
174	13	5	ej2	2015-12-15 14:24:09.99599
175	13	5	ej3	2015-12-15 14:24:16.764071
176	13	5	aj	2015-12-15 14:29:34.251305
177	13	5	aj	2015-12-15 14:29:42.846506
178	13	5	aj	2015-12-15 14:29:52.773087
179	13	5	aj	2015-12-15 14:29:56.173688
180	13	5	aj	2015-12-15 14:30:05.652238
181	13	5	aj	2015-12-15 14:31:04.70356
182	13	5	aj	2015-12-15 14:31:16.56594
183	13	5	aj	2015-12-15 14:31:28.088459
184	13	5	aj	2015-12-15 14:31:34.463913
185	13	5	aj	2015-12-15 14:33:02.945019
186	13	5	aj	2015-12-15 14:33:10.914771
187	13	5	aj	2015-12-15 14:38:09.209322
188	13	5	aj	2015-12-15 14:38:09.373281
189	13	5	aj	2015-12-15 14:38:09.524296
190	13	5	aj	2015-12-15 14:38:09.675928
191	13	5	aj	2015-12-15 14:38:09.838723
192	13	5	aj	2015-12-15 14:38:17.978639
193	13	5	aj	2015-12-15 14:38:22.024523
194	13	5	aj	2015-12-15 14:38:25.575904
195	13	5	aj	2015-12-15 14:38:32.89692
196	13	5	aj	2015-12-15 14:38:38.571286
197	13	5	aj	2015-12-15 14:38:49.259831
198	13	5	aj	2015-12-15 14:38:49.405075
199	13	5	aj	2015-12-15 14:38:49.552344
200	13	5	aj	2015-12-15 14:38:49.688923
201	13	5	aj	2015-12-15 14:38:57.401608
202	13	5	aj	2015-12-15 14:39:00.701358
203	13	5	aj	2015-12-15 14:50:30.070807
204	13	5	aj	2015-12-15 14:50:33.520805
205	13	5	aj	2015-12-15 14:50:36.838537
206	13	5	aj	2015-12-15 14:50:39.638295
207	13	5	aj	2015-12-15 14:50:42.408125
208	13	5	aj	2015-12-15 14:50:42.583819
209	13	5	aj	2015-12-15 14:50:59.159458
210	13	3	d\n	2015-12-15 15:00:34.957865
211	13	3	dwadwda	2015-12-15 15:00:41.971869
212	13	2	veki čega si se ti napio?	2015-12-15 15:10:08.586513
213	13	2	:P	2015-12-15 15:14:58.065739
214	13	2	scroll radi?	2015-12-15 15:15:26.932918
215	2	2	?	2015-12-15 15:21:21.514485
216	13	2	radi :D	2015-12-15 15:22:17.422327
217	2	2	ajmooo	2015-12-15 15:29:28.78973
218	2	2	u ponedjeljak	2015-12-15 15:32:10.969134
219	13	3	hahahaha\n	2015-12-15 15:32:57.398867
220	13	2	kae :P	2015-12-15 15:33:57.259335
221	13	2	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	2015-12-15 15:34:21.696624
222	13	3	opet dretve ne rade aaaaaaaaaaaaaaaaaaa\n	2015-12-15 15:37:47.486087
223	13	3	test	2015-12-15 15:39:40.817075
224	13	3	oke ovo je bilo brzo	2015-12-15 15:39:53.107237
225	13	3	neki ljudi bi se vjerojatno zgrozili nacinom kako sam popravio probleme sa layoutom i dretvom	2015-12-15 15:40:28.394331
226	13	3	JComponent parent = this.getParent();	2015-12-15 15:40:59.327965
227	13	3	parent.remove(this);\nparent.add(new.....);	2015-12-15 15:41:29.812043
228	13	3	dwadwadw	2015-12-15 15:42:12.395384
229	13	3	dawdwawawda	2015-12-15 15:43:39.496733
230	13	2	wuhuuuu	2015-12-15 15:50:22.083297
231	13	3	dwadawdwa\ndwadw	2015-12-15 15:57:48.038563
232	13	2	moja poreuka	2015-12-15 15:58:30.204734
233	13	2	oće sad 3?	2015-12-15 15:58:55.3948
234	13	2	da vidimo	2015-12-15 16:05:12.702659
235	13	3	oce oceeeee\noceee	2015-12-15 16:10:51.3142
236	13	3	oce	2015-12-15 16:10:53.752399
237	13	3	ddddd\n\n\n	2015-12-15 16:10:57.803949
238	13	3	d	2015-12-15 16:10:58.477091
239	13	3	radiii	2015-12-15 16:11:01.565472
240	13	3	pppp\n\n\ndaw\n	2015-12-15 16:11:08.282712
241	13	3	tn tn tn t t\nt t t t t t	2015-12-15 16:15:53.978541
242	13	3	tnt tnt ttnt	2015-12-15 16:15:59.578773
243	4	2	njanjanaj	2015-12-15 16:16:05.114924
244	4	2	u lala	2015-12-15 16:16:15.422969
245	4	2	scroll	2015-12-15 16:23:31.898677
246	4	2	da vidimo	2015-12-15 16:27:10.660062
247	13	2	batmaaaaaan	2015-12-15 16:33:45.809781
248	13	3	saljem poruku u jazbinu	2015-12-15 16:34:42.890301
249	13	3	:DDDD	2015-12-15 16:34:59.48539
250	13	2	wuhuuuu	2015-12-15 16:35:45.246151
251	13	2	šaljem ja	2015-12-15 16:39:17.057929
252	13	2	opet ja	2015-12-15 16:39:38.90967
253	13	2		2015-12-15 16:43:22.321101
254	13	3	test\n	2015-12-15 16:45:33.033852
255	13	3	holy macarony	2015-12-15 16:45:43.109996
256	13	3	it works	2015-12-15 16:45:52.315142
257	13	2	baba manda stišće enter	2015-12-15 16:48:12.735927
258	13	2	a šta sada	2015-12-15 16:48:26.91982
259	4	2	radiiii	2015-12-15 16:49:55.555076
260	4	2	blablabal	2015-12-15 16:50:34.593193
261	4	2	blablabla	2015-12-15 16:50:53.040414
262	13	3	istinita prica	2015-12-15 16:51:07.400785
263	14	3	ajj	2015-12-15 16:54:37.705398
264	14	3	aaa	2015-12-15 16:54:42.055368
265	14	3	what is this	2015-12-15 16:54:54.462386
266	13	3	aj	2015-12-15 16:55:14.871643
267	13	3	jajajaja	2015-12-15 16:55:19.44214
268	14	3	d	2015-12-15 16:55:26.103951
269	14	3	d	2015-12-15 16:55:26.478865
270	14	3	d	2015-12-15 16:55:26.831178
271	14	3	d	2015-12-15 16:55:27.152042
272	14	3	is	2015-12-15 16:55:36.58275
273	14	3	si	2015-12-15 16:55:39.781948
274	14	3	ju	2015-12-15 16:55:43.064673
275	14	3	k	2015-12-15 16:56:04.023218
276	13	2	veki baš voliš ajajajajat	2015-12-15 17:24:51.341362
277	4	7	mujoooooooo hasooo o o o o o	2015-12-15 17:25:14.904052
278	13	2	?	2015-12-15 17:28:43.564481
279	13	2	moja poruka 	2015-12-15 17:29:06.946907
280	13	3	d	2015-12-15 17:58:26.094239
281	13	3	d	2015-12-15 18:17:12.513532
282	13	3	dg	2015-12-15 18:17:21.339851
283	13	3	tttt	2015-12-15 18:17:25.847381
284	13	3	a	2015-12-15 18:22:29.255204
285	13	3	napokon radi glupi scroll	2015-12-15 18:52:51.712308
286	13	3	nanananana	2015-12-15 18:52:54.319532
287	13	3	htio me unistit	2015-12-15 18:53:19.405517
288	13	3	ali ne ne ne	2015-12-15 18:53:21.723603
289	14	15	ayyyyyyyyyyyyyy	2015-12-15 18:58:13.253028
290	14	15	ayayayaya	2015-12-15 18:58:32.59015
291	14	15	1	2015-12-15 18:58:35.805488
292	14	15	2	2015-12-15 18:58:36.211047
293	14	15	3	2015-12-15 18:58:36.507132
294	14	15	4	2015-12-15 18:58:37.006611
295	14	15	super, nista ne nestaje	2015-12-15 18:58:46.648888
296	14	15	1	2015-12-15 18:58:50.921888
297	14	15	2	2015-12-15 18:58:51.168519
298	14	15	3	2015-12-15 18:58:51.423984
299	14	15	4	2015-12-15 18:58:51.648536
300	14	15	5	2015-12-15 18:58:51.889894
301	14	15	6	2015-12-15 18:58:52.117942
302	14	15	7	2015-12-15 18:58:52.37951
303	14	15	8	2015-12-15 18:58:52.737065
304	14	15	9	2015-12-15 18:58:52.996924
305	14	15	(y)	2015-12-15 18:58:57.144723
306	14	5	sad cu ti crashat razgovor	2015-12-15 19:04:51.950882
307	14	5	sad cu ti crashat razgovor	2015-12-15 19:06:04.844626
308	14	5	sad cu ti crashat razgovor	2015-12-15 19:11:46.004727
309	13	3	huhiuhiuh	2015-12-15 22:09:31.989647
310	13	3	8joijoij	2015-12-15 22:09:37.118886
311	13	3	iloijoi	2015-12-15 22:09:43.357203
312	14	3	jajajaja	2015-12-17 11:26:38.26727
313	14	3	dwadwadaadw	2015-12-17 11:27:09.190401
314	13	3	hu\n	2015-12-18 15:10:17.932635
315	15	16	cao skvadro\n	2015-12-18 15:39:53.33357
316	15	3	ola senorita	2015-12-18 15:40:07.459401
317	15	16	cao misevi stigla je godzila	2015-12-18 15:40:12.911253
318	15	16	jeste primjetili ovo vlazorp	2015-12-18 15:40:46.174496
319	15	16	nisam vekszorp, vlazorp sam , kuzite	2015-12-18 15:41:01.826741
320	15	16	ljudi nasao sam vekszorpu bug, hehe	2015-12-18 16:32:32.16313
321	13	3	wazup bbbbbbbb	2015-12-19 15:38:30.396269
322	13	3	test	2015-12-19 19:06:13.174988
323	13	3	yyy	2015-12-19 19:45:12.043324
324	14	3	yyyy	2015-12-19 19:45:39.93698
325	13	3	aaaaa	2015-12-19 20:07:46.410333
326	13	3	ajajajaja	2015-12-19 20:25:24.488087
327	15	3	ajmo vlado programirajjjj	2015-12-19 20:26:04.544449
328	13	3	hello	2015-12-19 20:43:20.422393
329	16	18	pozdrav	2015-12-21 23:37:18.727025
330	17	17	Bok	2015-12-22 20:08:30.629662
331	13	3	dobar dan	2015-12-23 00:47:08.644127
332	13	3	fff	2015-12-23 00:47:41.732598
333	18	12	lol	2015-12-25 00:13:51.439654
334	18	12	novi test	2015-12-25 01:40:03.806123
335	18	12	hmm sto dalje	2015-12-25 01:41:23.775618
336	13	3	fe	2015-12-26 12:36:35.181517
337	13	3	ffaw f waf waf waf waf waf wakfawkfw akf wakf wakf wakf wakf wakfwak fwak fwakf awkfwakf wakf wakf wakfaw kfwa kfw akfwa kfwa kfa fawkfw akfw akfwakfkaw fkwa fkwa fwak fwakfwa	2015-12-26 12:36:45.699377
338	13	3	d	2015-12-26 12:51:08.378772
339	13	3	h	2015-12-26 12:54:56.617436
340	13	3	i	2015-12-26 12:55:17.138854
341	13	3	i	2015-12-26 12:58:49.200768
342	13	3	j	2015-12-26 13:08:35.291388
343	13	3	d	2015-12-26 13:19:27.340972
344	13	3	ajajajaja	2015-12-26 13:19:30.562481
345	13	3	s	2015-12-26 13:34:58.205269
346	13	3	d	2015-12-26 13:35:18.803352
347	13	2	bmsdi	2015-12-26 20:04:25.023021
348	13	2	nj fdasv	2015-12-26 20:04:29.41836
349	4	2	gokfdsb	2015-12-26 20:04:43.027697
350	13	3	test	2015-12-26 21:47:37.519586
351	14	3	test	2015-12-26 21:47:43.68109
352	15	3	test	2015-12-26 21:47:57.908367
353	8	3	ima l' koga	2015-12-26 21:51:23.772006
354	13	3	a	2015-12-26 21:53:07.561456
355	8	3	a	2015-12-26 21:59:07.283754
356	8	3	t	2015-12-26 22:04:06.112873
357	13	3	t	2015-12-26 22:04:12.891183
358	14	3	tttt	2015-12-26 22:04:21.572
359	10	3	druga por	2015-12-26 22:06:03.696936
360	18	3	nista	2015-12-26 22:14:27.704057
361	18	3	test	2015-12-26 22:14:30.490351
362	18	3	listener za nove razgovore radi ako se radi o insertu u tablicu pripadnost_razgovoru	2015-12-26 22:15:00.513113
363	18	3	ali ako se radi o updateu u tablici pripadnost_razgovoru gdje se status stavlja na TRUE (korisnik je jednom bio clan razgovora, otisao je, pa se sad vratio) ne radi	2015-12-26 22:15:41.219988
364	18	3	treba dodat novi trigger samo za to	2015-12-26 22:15:47.852127
365	18	3	TODO	2015-12-26 22:15:49.189726
366	18	3	gotovo	2015-12-27 01:05:47.358461
367	13	3	a	2015-12-27 01:07:42.883559
368	13	3	t	2015-12-27 01:14:42.739781
369	8	3	d	2015-12-27 01:14:53.308424
370	8	3	d	2015-12-27 01:14:56.180328
371	14	3	d	2015-12-27 01:14:58.83463
372	13	3	test	2015-12-27 01:15:30.784472
373	14	3	d	2015-12-27 01:15:50.726292
374	13	3	t	2015-12-27 01:18:13.747796
375	13	3	ajmoooo	2015-12-27 01:18:34.641471
376	8	3	d	2015-12-27 01:18:57.23223
377	14	3	wat	2015-12-27 01:31:18.512955
378	8	3	tttt	2015-12-27 01:31:24.216472
379	15	3	t	2015-12-27 01:43:46.212221
380	10	3	t	2015-12-27 01:49:52.162572
381	15	3	t	2015-12-27 01:49:58.616225
382	13	3	d	2015-12-27 01:52:44.273077
383	14	3	dwdwdw	2015-12-27 01:54:12.602949
384	14	3	d	2015-12-27 01:54:18.839689
385	10	3	d	2015-12-27 01:54:26.569626
386	10	3	d	2015-12-27 01:54:50.771601
387	8	3	d	2015-12-27 01:55:41.701773
388	8	3	d	2015-12-27 01:56:09.817644
389	13	3	a	2015-12-27 02:09:07.753271
390	10	3	d	2015-12-27 13:46:16.900921
391	15	3	d	2015-12-27 13:46:29.86549
392	10	3	dwaijdwaidjwawa	2015-12-27 14:11:49.711694
393	19	12	prva por	2015-12-27 14:20:24.440392
394	19	13	fg	2015-12-27 13:20:41.885706
395	19	12	jh	2015-12-27 14:20:46.485848
396	19	13	d	2015-12-27 13:20:51.662992
397	19	13	f	2015-12-27 13:21:00.520219
398	19	12	h	2015-12-27 14:21:04.44398
399	19	13	df	2015-12-27 13:23:04.464856
400	19	13	r	2015-12-27 13:23:10.05811
401	19	12	vg	2015-12-27 14:23:17.889027
402	19	12	v	2015-12-27 14:23:23.332931
403	19	13	gg	2015-12-27 13:31:35.918307
404	19	13	fg	2015-12-27 13:31:40.728115
405	19	13	ftt	2015-12-27 13:42:28.193412
406	13	2	veki testira na veliko	2015-12-27 16:00:10.217793
407	13	2	da vdiimo	2015-12-27 16:56:20.392187
408	4	2	beeeeeelj	2015-12-27 16:57:31.585063
409	4	2	ajmoooo	2015-12-27 17:00:28.002637
415	10	3	t	2015-12-27 17:21:29.94125
416	10	3	d	2015-12-27 17:21:37.488766
417	10	3	d	2015-12-27 17:21:37.712102
418	10	3	d	2015-12-27 17:21:37.925171
419	10	3	d	2015-12-27 17:21:38.124079
420	10	3	d	2015-12-27 17:21:38.319299
421	10	3	zđ	2015-12-27 17:21:50.303725
422	10	3	zđ	2015-12-27 17:21:50.466588
423	10	3	z	2015-12-27 17:21:50.610092
424	10	3	z	2015-12-27 17:21:50.768789
425	10	3	z	2015-12-27 17:21:50.929383
426	10	3	z	2015-12-27 17:21:51.087461
427	10	3	z	2015-12-27 17:21:51.248292
428	10	3	z	2015-12-27 17:21:51.404107
429	10	3	z	2015-12-27 17:21:51.563803
430	10	3	z	2015-12-27 17:21:51.718319
431	10	3	a	2015-12-27 17:22:30.888245
432	10	3	b	2015-12-27 17:22:31.121503
433	10	3	c	2015-12-27 17:22:31.357077
434	10	3	d	2015-12-27 17:22:31.573143
435	10	3	e	2015-12-27 17:22:31.786332
436	10	3	f	2015-12-27 17:22:31.980913
437	10	3	g	2015-12-27 17:22:32.239613
438	10	3	a	2015-12-27 17:22:39.811369
439	10	3	b	2015-12-27 17:22:40.049381
440	10	3	c	2015-12-27 17:22:40.28018
441	10	3	d	2015-12-27 17:22:40.516976
442	10	3	e	2015-12-27 17:22:40.75612
443	10	3	f	2015-12-27 17:22:40.970526
444	10	3	g	2015-12-27 17:22:41.194618
445	10	3	h	2015-12-27 17:22:41.407317
446	10	3	j	2015-12-27 17:22:41.639281
447	10	3	i	2015-12-27 17:22:41.920105
448	10	3	k	2015-12-27 17:22:42.121008
449	10	3	a	2015-12-27 17:22:56.670271
450	10	3	b	2015-12-27 17:22:56.934047
451	10	3	c	2015-12-27 17:22:57.157157
452	10	3	d	2015-12-27 17:22:57.395673
453	10	3	e	2015-12-27 17:22:57.633159
454	10	3	f	2015-12-27 17:22:57.879688
455	10	3	g	2015-12-27 17:22:58.112985
456	10	3	h	2015-12-27 17:22:58.339775
457	10	3	j	2015-12-27 17:22:58.552247
458	10	3	i	2015-12-27 17:22:58.788061
459	10	3	k	2015-12-27 17:22:59.047506
460	10	3	l	2015-12-27 17:23:08.059813
461	14	3	t	2015-12-27 17:29:15.317439
462	14	3	d	2015-12-27 17:29:18.228065
463	13	2	SYNC TEST	2015-12-27 17:35:21.560208
464	13	2	a	2015-12-27 17:35:35.17419
465	13	2	c	2015-12-27 17:35:38.604906
466	13	2	a	2015-12-27 17:35:46.260008
467	13	2	a	2015-12-27 17:35:47.396181
468	13	2	a	2015-12-27 17:36:00.877008
469	13	2	s	2015-12-27 17:36:01.763566
470	13	2	z	2015-12-27 17:36:54.471337
471	4	2	gooooo	2015-12-27 17:46:53.941982
472	4	2	goooooooo	2015-12-27 17:47:25.127251
473	4	2	go zlatko	2015-12-27 17:52:21.690478
474	2	2	ha	2015-12-27 17:52:53.966915
475	4	2	bleh	2015-12-27 17:53:50.428728
476	2	2	ovo mora gore	2015-12-27 17:54:58.982603
477	1	2	ajmo svi	2015-12-27 17:55:18.77069
478	13	2	vekiiii	2015-12-27 17:55:52.508758
479	4	7	da vidmjo ovo	2015-12-27 17:56:31.389763
480	1	2	pala pala pala činke	2015-12-27 18:05:35.814577
481	4	7	hahahaha	2015-12-27 18:08:41.726766
482	4	7	hm	2015-12-27 18:11:19.693011
483	1	7	a bljuv	2015-12-27 18:11:30.409096
484	10	3	test	2015-12-27 18:12:22.227255
485	10	3	t	2015-12-27 18:13:33.212197
486	8	3	t	2015-12-27 18:14:10.865224
487	15	3	t	2015-12-27 18:14:15.9094
488	4	2	jel radi?	2015-12-27 18:17:48.390621
489	4	7	radi	2015-12-27 18:17:55.720476
490	4	2	gledaj desno razgovore	2015-12-27 18:18:08.478075
491	1	2	jel se pokanulo?	2015-12-27 18:18:17.791786
492	1	2	pomaknulo*	2015-12-27 18:18:29.950189
493	4	7	kak mislis desno? ovaj tab razgovori su lijevo	2015-12-27 18:18:32.498142
494	4	2	e lijevo	2015-12-27 18:18:37.483265
495	4	2	jesu se micali razgovori?	2015-12-27 18:18:45.936616
496	4	7	hahaha ok	2015-12-27 18:18:48.031098
497	4	7	jesu	2015-12-27 18:18:50.292355
498	4	2	znači sve radi	2015-12-27 18:19:14.330024
499	4	2	halooooo	2015-12-27 18:19:44.029412
500	4	2	jel radi? :')	2015-12-27 18:19:49.317853
501	4	7	osjecam se jako tempted da posaljem neki link sa applause gif-om	2015-12-27 18:19:52.968378
502	4	7	radi radi, stizu poruke haha	2015-12-27 18:20:10.660611
503	4	2	http://vignette1.wikia.nocookie.net/fairytailfanon/images/5/5c/Applause-gif-tumblr-43_zps4e464b19.gif/revision/latest?cb=20150513061029	2015-12-27 18:20:38.531288
504	4	7	e jedino kaj mogu scrollat cijeli screen	2015-12-27 18:20:42.435018
505	4	2	ha?	2015-12-27 18:20:51.27525
506	4	2	to bi se trebalo dešavat samo ako je mali ekran	2015-12-27 18:21:07.97159
507	4	7	pogle na fejs, bacio sam ti screenshot	2015-12-27 18:21:50.551955
508	4	2	ma vidio	2015-12-27 18:23:45.355467
509	1	2	ima još posla ljudi al jebeno ide	2015-12-27 18:25:35.790409
510	1	2	samo sam 5 sati radio dana	2015-12-27 18:25:44.407891
511	1	2	a fuck ovo ne pišem na faceu :')	2015-12-27 18:25:53.539685
512	1	7	radi jos uvijek sve	2015-12-27 18:34:17.195919
513	10	3	t	2015-12-27 18:48:35.925701
514	10	3	d	2015-12-27 18:48:44.658989
515	8	3	t	2015-12-27 20:11:09.472354
516	8	3	a	2015-12-27 21:03:07.821618
517	15	3	a	2015-12-27 21:03:12.107419
518	19	3	a	2015-12-27 21:03:44.256151
519	13	3	daaaa	2015-12-27 21:18:22.357679
520	8	3	j74	2015-12-27 21:18:48.80584
521	15	4	hello :)	2015-12-28 13:04:11.912395
522	15	4	promjenila vatar	2015-12-28 13:35:48.008073
523	1	2	palačinkeeeee	2015-12-28 14:30:00.912301
524	15	3	hello there	2015-12-28 15:20:52.629974
525	18	3	a	2015-12-28 15:22:09.254534
526	10	3	a	2015-12-28 15:22:15.539055
527	15	3	test	2015-12-28 14:44:02.617372
528	15	3	promijenjeno serversko vrijeme, pazite ^	2015-12-28 14:44:27.347245
529	25	3	yo pipl	2015-12-28 14:45:12.693447
530	14	3	vrijeme	2015-12-28 14:45:30.139551
531	14	3	(y)	2015-12-28 14:45:34.382601
532	25	7	al treći nije marin nego mujo :')	2015-12-28 14:46:08.284817
533	20	3	tko je tu	2015-12-28 14:48:07.476981
534	20	2	ja	2015-12-28 14:49:12.69256
535	26	2	jel ti se sviđa anziv?	2015-12-28 14:49:22.456732
536	26	3	da bas je fensi	2015-12-28 14:49:31.278561
537	26	2	skroz	2015-12-28 14:49:50.227309
538	20	2	ahahah malo su mi raspali timestampovi	2015-12-28 14:50:28.598505
539	20	3	hahahah	2015-12-28 14:51:10.693302
540	20	3	meni je sve dobro jer orderam by id :3	2015-12-28 14:51:19.189024
541	26	2	sad bi trebalora dit	2015-12-28 14:51:46.466197
542	20	2	ma i ja isto poruke	2015-12-28 14:52:14.324996
543	20	2	al popis zadnjih razgovora	2015-12-28 14:52:23.298954
544	26	2	yolo	2015-12-28 14:52:31.177938
545	26	2	testing	2015-12-28 14:53:12.072239
546	26	2	zakaj vraćaaaaaa	2015-12-28 14:54:29.224204
547	26	3	yolo	2015-12-28 14:54:46.468178
548	1	2	koji kurac	2015-12-28 14:55:00.38506
549	20	2	za mene i vekija konstanto vraća na vrh	2015-12-28 14:55:20.818604
550	20	2	ajme da doživljava ga kao novi razgovor	2015-12-28 14:55:34.802954
551	20	2	i puca na vrh	2015-12-28 14:55:39.13978
552	20	2	jel tebi označava sastrane ako si dobio novu poruku?	2015-12-28 14:56:30.303362
553	20	3	da, pofarba gumb u zuto i puca na vrh	2015-12-28 14:58:23.13869
554	25	3	:')	2015-12-28 15:06:52.612723
555	10	3	test	2015-12-28 15:09:53.644615
556	10	3	test4	2015-12-28 15:09:55.401489
557	20	3	t	2015-12-28 15:10:06.535092
558	20	3	t	2015-12-28 15:10:06.739158
559	20	3	t	2015-12-28 15:10:06.915581
560	20	3	t	2015-12-28 15:10:07.090715
561	20	3	t	2015-12-28 15:10:07.267161
562	20	3	t	2015-12-28 15:10:07.43864
563	20	3	tt	2015-12-28 15:10:07.701919
564	13	3	daaaa	2015-12-28 15:25:26.239524
565	26	3	jel sve dobro sada	2015-12-28 15:25:47.222664
566	26	3	t	2015-12-28 15:42:03.810615
567	26	3	test	2015-12-28 15:42:57.613453
568	13	3	t	2015-12-28 15:44:36.592125
569	13	3	t	2015-12-28 15:44:36.784352
570	13	3	t	2015-12-28 15:44:36.959819
571	13	3	t	2015-12-28 15:44:37.135353
572	13	3	t	2015-12-28 15:44:38.063595
573	19	12	zagreb vrem zona	2015-12-28 18:10:48.098139
574	19	12	london 	2015-12-28 17:11:20.459427
575	20	3	ova poruka je poslana dok je na klijentskoj aplikaciji bila time zona iz pakistana (22 h)	2015-12-28 22:17:39.289911
576	20	3	test	2015-12-28 22:22:11.7097
577	20	3	t	2015-12-28 22:27:22.183647
578	15	3	poruka preko pgadmina	2015-12-28 18:29:21.239494
579	15	3	wat	2015-12-28 22:32:14.502175
580	10	3	slanje iz pakistana	2015-12-28 18:39:19.524186
581	10	3	test	2015-12-28 18:40:59.030967
582	13	3	t	2015-12-28 18:41:59.824166
583	14	3	t	2015-12-28 18:43:31.731465
584	10	3	t	2015-12-28 18:44:16.000428
585	10	3	a	2015-12-28 18:56:50.854851
586	10	3	t	2015-12-28 18:59:13.334507
587	10	3	test	2015-12-28 19:01:25.568007
588	10	3	tttt	2015-12-28 19:01:28.787972
589	13	3	t	2015-12-28 19:01:43.254149
590	15	3	t	2015-12-28 19:01:55.230958
591	26	3	t	2015-12-28 19:02:05.092172
592	10	3	t	2015-12-28 19:02:14.233297
593	10	3	t	2015-12-28 19:02:18.868074
594	10	3	t	2015-12-28 19:02:22.72821
595	10	3	t	2015-12-28 19:03:16.424205
596	10	3	t	2015-12-28 19:03:18.467364
597	13	3	ay	2015-12-28 19:04:10.425179
598	13	3	aa	2015-12-28 19:04:13.048303
599	13	3	a	2015-12-28 19:04:15.451854
600	13	3	a	2015-12-28 19:04:16.742503
601	10	3	t	2015-12-28 19:07:07.373523
602	10	3	t	2015-12-28 19:07:49.000329
603	14	3	t	2015-12-28 19:08:00.936537
604	10	3	t	2015-12-28 19:09:08.971423
605	13	3	t	2015-12-28 19:10:46.197698
606	13	3	j	2015-12-28 19:18:11.896138
607	13	3	a	2015-12-28 19:18:12.541659
608	13	3	z	2015-12-28 19:18:12.983727
609	13	3	b	2015-12-28 19:18:13.306262
610	13	3	bi	2015-12-28 19:18:14.373941
611	13	3	n	2015-12-28 19:18:15.208825
612	13	3	a	2015-12-28 19:18:15.67508
613	10	3	dgd	2015-12-28 19:27:38.514992
614	10	3	gd	2015-12-28 19:27:38.763457
615	10	3	f	2015-12-28 19:27:44.212376
616	10	3	fff	2015-12-28 19:27:47.276419
617	10	3	f	2015-12-28 19:27:47.573544
618	10	3	d	2015-12-28 19:30:58.415812
619	10	3	ajme meni	2015-12-28 19:33:45.781195
620	14	3	t	2015-12-28 19:35:40.821014
621	10	3	d	2015-12-28 19:36:26.013888
622	10	3	d	2015-12-28 19:38:18.183714
623	13	3	a	2015-12-28 19:39:31.413959
624	10	3	d	2015-12-28 19:39:40.134266
625	10	3	d	2015-12-28 19:39:40.373792
626	10	3	d	2015-12-28 19:39:40.591251
627	10	3	df	2015-12-28 19:39:42.380551
628	10	3	t	2015-12-28 19:41:18.822329
629	10	3	dm	2015-12-28 19:42:22.83828
630	10	3	tttt	2015-12-28 19:47:01.838841
631	13	3	h	2015-12-28 19:47:27.342762
632	20	2	silvija piše porukuuuuu	2015-12-28 20:09:34.735694
633	20	2	woooohooooooooo	2015-12-28 20:09:43.845479
634	13	3	t	2015-12-29 13:11:28.903075
635	10	3	at	2015-12-29 13:11:35.089908
636	10	3	tttt	2016-01-01 17:28:12.720376
637	10	3	j	2016-01-01 17:47:13.854195
638	10	3	j	2016-01-01 17:54:50.969323
639	10	3	j	2016-01-01 17:54:57.801226
640	13	3	j	2016-01-01 18:14:12.413686
641	13	3	j	2016-01-01 18:14:51.647035
642	20	2	zašto silvija ak je za mene i vekija?	2016-01-02 11:28:52.882564
643	20	2	wuuuu	2016-01-03 13:40:48.401125
644	20	2	sljedeca	2016-01-03 13:42:06.568414
645	20	2	ke?	2016-01-03 13:42:50.056427
646	13	2	zbljuv	2016-01-03 13:43:05.050707
647	20	2	a sad?	2016-01-03 13:43:44.831241
648	20	2	ovo je za mene i vekija	2016-01-03 13:46:33.477156
649	13	2	refresh	2016-01-03 13:58:48.638804
650	13	2	manji refresh	2016-01-03 13:59:21.195859
651	20	2	refresh 1	2016-01-03 13:59:31.530874
652	13	3	gegege	2016-01-03 14:14:25.924965
653	13	3	dwadwdaw	2016-01-03 14:16:12.435892
654	15	4	marija	2016-01-03 14:21:18.28993
655	14	3	nova poruka	2016-01-03 14:29:59.175884
656	14	3	ddd	2016-01-03 14:48:10.112097
657	15	3	poruka	2016-01-03 14:48:50.521133
658	20	2	blabalbla	2016-01-03 15:05:02.522922
659	13	2	ahaha kao da se živine glasaju	2016-01-03 15:10:25.898914
660	14	3	srca mlada vesele se	2016-01-03 16:57:31.038562
661	14	4	test	2016-01-03 16:57:43.118902
662	14	3	OTVORILI SU PORTOVE	2016-01-03 16:57:55.116218
663	14	3	PORT*	2016-01-03 16:58:04.022358
664	10	3	dawdawdaw	2016-01-03 16:58:15.032203
665	15	4	radi popis sudionikaaa	2016-01-03 18:41:01.553851
666	15	4	wihuuuu	2016-01-03 18:41:03.655962
667	15	4	sve	2016-01-03 18:41:07.299964
668	15	4	aaaa	2016-01-03 18:41:08.108173
669	20	3	zlatko slatko	2016-01-04 11:45:35.669212
670	17	17	alooo, javi se	2016-01-04 18:18:58.938816
671	17	18	kuha li se sarma?	2016-01-04 18:19:41.060854
672	17	17	jebeno	2016-01-04 18:20:15.277728
673	15	11	Sretna nova godinaaaaa\n	2016-01-05 12:36:51.465064
674	17	18	bok, kak si?	2016-01-05 16:54:57.832513
675	17	17	dobro	2016-01-05 16:55:26.563798
676	17	17	zkj nece	2016-01-05 16:55:43.244102
677	13	3	j	2016-01-05 20:23:11.093185
678	15	3	aaaa	2016-01-06 10:54:33.409771
679	15	3	hvala	2016-01-06 10:54:36.830014
680	20	2	reci veki neki	2016-01-06 11:57:15.796443
681	15	3	tko me treba neka pise ovdje, ne mogu na fejs	2016-01-06 12:47:32.929735
682	26	3	t	2016-01-06 12:53:53.52603
683	15	3	ima koga	2016-01-06 12:54:08.068184
\.


--
-- Name: poruka_idporuke_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('poruka_idporuke_seq', 683, true);


--
-- Data for Name: pripadnost_razgovoru; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pripadnost_razgovoru (idrazgovora, idsudionika, status) FROM stdin;
18	12	t
1	2	t
2	2	t
4	2	t
5	2	t
6	12	t
6	13	t
18	13	t
8	12	t
18	14	t
9	12	t
9	13	t
10	13	t
5	3	f
10	3	f
37	17	t
4	7	t
13	2	t
13	5	t
14	3	t
14	15	t
15	11	t
15	4	t
15	16	t
16	18	t
17	17	t
17	18	t
13	3	t
19	12	t
15	3	t
19	13	t
1	7	t
8	3	f
19	3	f
10	12	t
18	3	f
20	2	t
20	3	t
25	2	t
25	7	t
26	2	t
26	3	t
25	3	t
14	4	f
\.


--
-- Data for Name: razgovor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY razgovor (idrazgovora, imerazgovora, vrijemepocetka) FROM stdin;
1	Odlazak na palacinke	2015-11-14 10:19:23.085565
2	Dogovor  za sastanak	2015-11-14 10:34:10.696254
3	webici	2015-12-11 16:18:09.948779
4	Katarina ili Silvija	2015-12-11 16:46:07.258162
6	conv_tab_tel_-229	2015-12-12 20:38:11.729519
7	test1	2015-12-12 23:58:00.207527
8	test2	2015-12-13 00:29:10.60183
9	test3	2015-12-13 00:34:10.610135
10	test4	2015-12-13 00:38:33.446438
13	jazbina	2015-12-14 18:38:38.47582
5	Silvija ili Katarina	2015-12-11 16:47:01.574238
16	proba	2015-12-21 23:37:18.217243
17	sarma1	2015-12-22 20:08:29.817045
18	test5	2015-12-25 00:13:47.056956
19	test6	2015-12-27 14:20:23.460244
20	Za mene i vekija	2015-12-28 15:25:48.001637
28	2	2016-01-04 11:08:06.791877
29	2	2016-01-04 11:14:57.194593
30	2	2016-01-04 11:20:38.600794
31	2	2016-01-04 11:22:22.293684
32	2	2016-01-04 11:23:33.388822
33	2	2016-01-04 11:23:55.382834
34	2	2016-01-04 11:23:58.375867
35	2	2016-01-04 11:32:07.125586
36	2	2016-01-04 11:34:27.869462
25	v&z&mujo	2015-12-28 14:44:27.155529
15	Desktopasi	2015-12-15 18:54:24.476357
37	proba1	2016-01-05 16:53:28.202682
14	Moj razgovor	2015-12-15 16:54:04.767669
26	veki & zlatica	2015-12-28 14:47:57.807683
\.


--
-- Name: razgovor_idrazgovora_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('razgovor_idrazgovora_seq', 37, true);


--
-- Name: avatar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY avatar
    ADD CONSTRAINT avatar_pkey PRIMARY KEY (idavatar);


--
-- Name: bojafonta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bojafonta
    ADD CONSTRAINT bojafonta_pkey PRIMARY KEY (idboje);


--
-- Name: bojasucelja_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bojasucelja
    ADD CONSTRAINT bojasucelja_pkey PRIMARY KEY (idboje);


--
-- Name: font_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY font
    ADD CONSTRAINT font_pkey PRIMARY KEY (idfonta);


--
-- Name: kontakti_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kontakti
    ADD CONSTRAINT kontakti_pkey PRIMARY KEY (idvlasnika, idkontakta);


--
-- Name: korisnik_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_email_key UNIQUE (email);


--
-- Name: korisnik_korisnickoime_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_korisnickoime_key UNIQUE (korisnickoime);


--
-- Name: korisnik_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_pkey PRIMARY KEY (idkorisnika);


--
-- Name: poruka_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY poruka
    ADD CONSTRAINT poruka_pkey PRIMARY KEY (idporuke);


--
-- Name: pripadnost_razgovoru_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pripadnost_razgovoru
    ADD CONSTRAINT pripadnost_razgovoru_pkey PRIMARY KEY (idrazgovora, idsudionika);


--
-- Name: razgovor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY razgovor
    ADD CONSTRAINT razgovor_pkey PRIMARY KEY (idrazgovora);


--
-- Name: dodan_u_razgovor; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER dodan_u_razgovor AFTER INSERT ON pripadnost_razgovoru FOR EACH ROW EXECUTE PROCEDURE notify_dodan_u_razgovor();


--
-- Name: izasao_iz_razgovora; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER izasao_iz_razgovora AFTER UPDATE ON pripadnost_razgovoru FOR EACH ROW WHEN (((old.status = true) AND (new.status = false))) EXECUTE PROCEDURE notify_izasao_iz_razgovora();


--
-- Name: nova_poruka; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER nova_poruka AFTER INSERT ON poruka FOR EACH ROW EXECUTE PROCEDURE notify_nova_poruka();


--
-- Name: promjena_naziva_razgovora; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER promjena_naziva_razgovora AFTER UPDATE ON razgovor FOR EACH ROW WHEN (((old.imerazgovora)::text <> (new.imerazgovora)::text)) EXECUTE PROCEDURE notify_promjena_naziva_razgovora();


--
-- Name: vracen_u_razgovor; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER vracen_u_razgovor AFTER UPDATE ON pripadnost_razgovoru FOR EACH ROW WHEN (((old.status = false) AND (new.status = true))) EXECUTE PROCEDURE notify_dodan_u_razgovor();


--
-- Name: kontakti_idkontakta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kontakti
    ADD CONSTRAINT kontakti_idkontakta_fkey FOREIGN KEY (idkontakta) REFERENCES korisnik(idkorisnika);


--
-- Name: kontakti_idvlasnika_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kontakti
    ADD CONSTRAINT kontakti_idvlasnika_fkey FOREIGN KEY (idvlasnika) REFERENCES korisnik(idkorisnika);


--
-- Name: korisnik_idavatar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idavatar_fkey FOREIGN KEY (idavatar) REFERENCES avatar(idavatar);


--
-- Name: korisnik_idbojafonta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idbojafonta_fkey FOREIGN KEY (idbojafonta) REFERENCES bojafonta(idboje);


--
-- Name: korisnik_idbojasucelja_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idbojasucelja_fkey FOREIGN KEY (idbojasucelja) REFERENCES bojasucelja(idboje);


--
-- Name: korisnik_idfont_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idfont_fkey FOREIGN KEY (idfont) REFERENCES font(idfonta);


--
-- Name: poruka_idposiljatelja_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poruka
    ADD CONSTRAINT poruka_idposiljatelja_fkey FOREIGN KEY (idposiljatelja) REFERENCES korisnik(idkorisnika);


--
-- Name: poruka_idrazgovora_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poruka
    ADD CONSTRAINT poruka_idrazgovora_fkey FOREIGN KEY (idrazgovora) REFERENCES razgovor(idrazgovora);


--
-- Name: pripadnost_razgovoru_idrazgovora_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pripadnost_razgovoru
    ADD CONSTRAINT pripadnost_razgovoru_idrazgovora_fkey FOREIGN KEY (idrazgovora) REFERENCES razgovor(idrazgovora);


--
-- Name: pripadnost_razgovoru_idsudionika_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pripadnost_razgovoru
    ADD CONSTRAINT pripadnost_razgovoru_idsudionika_fkey FOREIGN KEY (idsudionika) REFERENCES korisnik(idkorisnika);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: avatar; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE avatar FROM PUBLIC;
REVOKE ALL ON TABLE avatar FROM postgres;
GRANT ALL ON TABLE avatar TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE avatar TO defaultuser;


--
-- Name: avatar_idavatara_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE avatar_idavatara_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE avatar_idavatara_seq FROM postgres;
GRANT ALL ON SEQUENCE avatar_idavatara_seq TO postgres;
GRANT USAGE ON SEQUENCE avatar_idavatara_seq TO defaultuser;


--
-- Name: bojafonta; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE bojafonta FROM PUBLIC;
REVOKE ALL ON TABLE bojafonta FROM postgres;
GRANT ALL ON TABLE bojafonta TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE bojafonta TO defaultuser;


--
-- Name: bojafonta_idboje_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE bojafonta_idboje_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bojafonta_idboje_seq FROM postgres;
GRANT ALL ON SEQUENCE bojafonta_idboje_seq TO postgres;
GRANT USAGE ON SEQUENCE bojafonta_idboje_seq TO defaultuser;


--
-- Name: bojasucelja; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE bojasucelja FROM PUBLIC;
REVOKE ALL ON TABLE bojasucelja FROM postgres;
GRANT ALL ON TABLE bojasucelja TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE bojasucelja TO defaultuser;


--
-- Name: bojasucelja_idboje_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE bojasucelja_idboje_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bojasucelja_idboje_seq FROM postgres;
GRANT ALL ON SEQUENCE bojasucelja_idboje_seq TO postgres;
GRANT USAGE ON SEQUENCE bojasucelja_idboje_seq TO defaultuser;


--
-- Name: font; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE font FROM PUBLIC;
REVOKE ALL ON TABLE font FROM postgres;
GRANT ALL ON TABLE font TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE font TO defaultuser;


--
-- Name: font_idfonta_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE font_idfonta_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE font_idfonta_seq FROM postgres;
GRANT ALL ON SEQUENCE font_idfonta_seq TO postgres;
GRANT USAGE ON SEQUENCE font_idfonta_seq TO defaultuser;


--
-- Name: kontakti; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE kontakti FROM PUBLIC;
REVOKE ALL ON TABLE kontakti FROM postgres;
GRANT ALL ON TABLE kontakti TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE kontakti TO defaultuser;


--
-- Name: korisnik; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE korisnik FROM PUBLIC;
REVOKE ALL ON TABLE korisnik FROM postgres;
GRANT ALL ON TABLE korisnik TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE korisnik TO defaultuser;


--
-- Name: korisnik_idkorisnika_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE korisnik_idkorisnika_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE korisnik_idkorisnika_seq FROM postgres;
GRANT ALL ON SEQUENCE korisnik_idkorisnika_seq TO postgres;
GRANT USAGE ON SEQUENCE korisnik_idkorisnika_seq TO defaultuser;


--
-- Name: poruka; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE poruka FROM PUBLIC;
REVOKE ALL ON TABLE poruka FROM postgres;
GRANT ALL ON TABLE poruka TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE poruka TO defaultuser;


--
-- Name: poruka_idporuke_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE poruka_idporuke_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE poruka_idporuke_seq FROM postgres;
GRANT ALL ON SEQUENCE poruka_idporuke_seq TO postgres;
GRANT USAGE ON SEQUENCE poruka_idporuke_seq TO defaultuser;


--
-- Name: pripadnost_razgovoru; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE pripadnost_razgovoru FROM PUBLIC;
REVOKE ALL ON TABLE pripadnost_razgovoru FROM postgres;
GRANT ALL ON TABLE pripadnost_razgovoru TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE pripadnost_razgovoru TO defaultuser;


--
-- Name: razgovor; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE razgovor FROM PUBLIC;
REVOKE ALL ON TABLE razgovor FROM postgres;
GRANT ALL ON TABLE razgovor TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE razgovor TO defaultuser;


--
-- Name: razgovor_idrazgovora_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE razgovor_idrazgovora_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE razgovor_idrazgovora_seq FROM postgres;
GRANT ALL ON SEQUENCE razgovor_idrazgovora_seq TO postgres;
GRANT USAGE ON SEQUENCE razgovor_idrazgovora_seq TO defaultuser;


--
-- PostgreSQL database dump complete
--

