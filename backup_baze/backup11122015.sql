--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: kreiraj_razgovor(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION kreiraj_razgovor(ime character varying, idkreatora integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
  DECLARE novi_id INTEGER;
	BEGIN
		INSERT INTO razgovor (imerazgovora) VALUES ($1);
		SELECT currval(pg_get_serial_sequence('razgovor','idrazgovora')) INTO novi_id;
		
		INSERT INTO pripadnost_razgovoru (idrazgovora,idsudionika,status) values (novi_id,$2,true);
		
		
	
		RETURN novi_id;
	END
	$_$;


ALTER FUNCTION public.kreiraj_razgovor(ime character varying, idkreatora integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: avatar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE avatar (
    idavatar integer NOT NULL,
    lokacijaavatara character varying(200) NOT NULL,
    imeavatara character varying(50) NOT NULL
);


ALTER TABLE avatar OWNER TO postgres;

--
-- Name: avatar_idavatara_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE avatar_idavatara_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE avatar_idavatara_seq OWNER TO postgres;

--
-- Name: avatar_idavatara_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE avatar_idavatara_seq OWNED BY avatar.idavatar;


--
-- Name: bojafonta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bojafonta (
    idboje integer NOT NULL,
    imeboje character varying(20),
    rgb_r integer NOT NULL,
    rgb_g integer NOT NULL,
    rgb_b integer NOT NULL,
    CONSTRAINT bojafonta_rgb_b_check CHECK (((rgb_b >= 0) AND (rgb_b <= 255))),
    CONSTRAINT bojafonta_rgb_g_check CHECK (((rgb_g >= 0) AND (rgb_g <= 255))),
    CONSTRAINT bojafonta_rgb_r_check CHECK (((rgb_r >= 0) AND (rgb_r <= 255)))
);


ALTER TABLE bojafonta OWNER TO postgres;

--
-- Name: bojafonta_idboje_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bojafonta_idboje_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bojafonta_idboje_seq OWNER TO postgres;

--
-- Name: bojafonta_idboje_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bojafonta_idboje_seq OWNED BY bojafonta.idboje;


--
-- Name: bojasucelja; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bojasucelja (
    idboje integer NOT NULL,
    imeboje character varying(20),
    rgb_r integer NOT NULL,
    rgb_g integer NOT NULL,
    rgb_b integer NOT NULL,
    CONSTRAINT bojasucelja_rgb_b_check CHECK (((rgb_b >= 0) AND (rgb_b <= 255))),
    CONSTRAINT bojasucelja_rgb_g_check CHECK (((rgb_g >= 0) AND (rgb_g <= 255))),
    CONSTRAINT bojasucelja_rgb_r_check CHECK (((rgb_r >= 0) AND (rgb_r <= 255)))
);


ALTER TABLE bojasucelja OWNER TO postgres;

--
-- Name: bojasucelja_idboje_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bojasucelja_idboje_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bojasucelja_idboje_seq OWNER TO postgres;

--
-- Name: bojasucelja_idboje_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bojasucelja_idboje_seq OWNED BY bojasucelja.idboje;


--
-- Name: font; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE font (
    idfonta integer NOT NULL,
    imefonta character varying(20),
    velicinafonta integer DEFAULT 20 NOT NULL,
    stilfonta character(6) DEFAULT 'plain'::bpchar NOT NULL,
    CONSTRAINT font_stilfonta_check CHECK ((((lower((stilfonta)::text) = 'plain'::text) OR (lower((stilfonta)::text) = 'italic'::text)) OR (lower((stilfonta)::text) = 'bold'::text))),
    CONSTRAINT font_velicinafonta_check CHECK (((velicinafonta >= 10) AND (velicinafonta <= 30)))
);


ALTER TABLE font OWNER TO postgres;

--
-- Name: CONSTRAINT font_stilfonta_check ON font; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT font_stilfonta_check ON font IS 'Ako dodajemo novi font, provjerava jesmo li napisali stil fonta ''plain'', ''italic'', ili ''bold'', inace je greska';


--
-- Name: CONSTRAINT font_velicinafonta_check ON font; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT font_velicinafonta_check ON font IS 'ako dodajemo novi font provjerava nalazi li se velicina fonta u zadanom intervalu';


--
-- Name: font_idfonta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE font_idfonta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE font_idfonta_seq OWNER TO postgres;

--
-- Name: font_idfonta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE font_idfonta_seq OWNED BY font.idfonta;


--
-- Name: kontakti; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kontakti (
    idvlasnika integer NOT NULL,
    idkontakta integer NOT NULL,
    status boolean,
    nadimak character varying(40)
);


ALTER TABLE kontakti OWNER TO postgres;

--
-- Name: korisnik; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE korisnik (
    idkorisnika integer NOT NULL,
    korisnickoime character varying(20),
    lozinka character varying(40),
    aktivan boolean DEFAULT true NOT NULL,
    email character varying(50),
    idbojafonta integer DEFAULT 1,
    idfont integer DEFAULT 1,
    idavatar integer DEFAULT 1 NOT NULL,
    idbojasucelja integer DEFAULT 1
);


ALTER TABLE korisnik OWNER TO postgres;

--
-- Name: korisnik_idkorisnika_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE korisnik_idkorisnika_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE korisnik_idkorisnika_seq OWNER TO postgres;

--
-- Name: korisnik_idkorisnika_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE korisnik_idkorisnika_seq OWNED BY korisnik.idkorisnika;


--
-- Name: poruka; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE poruka (
    idporuke integer NOT NULL,
    idrazgovora integer,
    idposiljatelja integer,
    tekst text,
    vrijemeslanja timestamp without time zone DEFAULT transaction_timestamp()
);


ALTER TABLE poruka OWNER TO postgres;

--
-- Name: poruka_idporuke_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE poruka_idporuke_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE poruka_idporuke_seq OWNER TO postgres;

--
-- Name: poruka_idporuke_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE poruka_idporuke_seq OWNED BY poruka.idporuke;


--
-- Name: pripadnost_razgovoru; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pripadnost_razgovoru (
    idrazgovora integer NOT NULL,
    idsudionika integer NOT NULL,
    status boolean
);


ALTER TABLE pripadnost_razgovoru OWNER TO postgres;

--
-- Name: razgovor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE razgovor (
    idrazgovora integer NOT NULL,
    imerazgovora character varying(20),
    vrijemepocetka timestamp without time zone DEFAULT transaction_timestamp()
);


ALTER TABLE razgovor OWNER TO postgres;

--
-- Name: razgovor_idrazgovora_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE razgovor_idrazgovora_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE razgovor_idrazgovora_seq OWNER TO postgres;

--
-- Name: razgovor_idrazgovora_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE razgovor_idrazgovora_seq OWNED BY razgovor.idrazgovora;


--
-- Name: idavatar; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avatar ALTER COLUMN idavatar SET DEFAULT nextval('avatar_idavatara_seq'::regclass);


--
-- Name: idboje; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bojafonta ALTER COLUMN idboje SET DEFAULT nextval('bojafonta_idboje_seq'::regclass);


--
-- Name: idboje; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bojasucelja ALTER COLUMN idboje SET DEFAULT nextval('bojasucelja_idboje_seq'::regclass);


--
-- Name: idfonta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY font ALTER COLUMN idfonta SET DEFAULT nextval('font_idfonta_seq'::regclass);


--
-- Name: idkorisnika; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik ALTER COLUMN idkorisnika SET DEFAULT nextval('korisnik_idkorisnika_seq'::regclass);


--
-- Name: idporuke; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poruka ALTER COLUMN idporuke SET DEFAULT nextval('poruka_idporuke_seq'::regclass);


--
-- Name: idrazgovora; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY razgovor ALTER COLUMN idrazgovora SET DEFAULT nextval('razgovor_idrazgovora_seq'::regclass);


--
-- Data for Name: avatar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY avatar (idavatar, lokacijaavatara, imeavatara) FROM stdin;
1	http://i.imgur.com/PlbgSFk.jpg	banana
2	http://images.clipartpanda.com/watermelon-clip-art-watermelon_slice.svg	lubenica
3	https://stephanieallison.files.wordpress.com/2010/08/single-small-pear5b15d.jpg	crtez kruske
\.


--
-- Name: avatar_idavatara_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('avatar_idavatara_seq', 3, true);


--
-- Data for Name: bojafonta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bojafonta (idboje, imeboje, rgb_r, rgb_g, rgb_b) FROM stdin;
1	crna	0	0	0
2	bijela	255	255	255
\.


--
-- Name: bojafonta_idboje_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bojafonta_idboje_seq', 2, true);


--
-- Data for Name: bojasucelja; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bojasucelja (idboje, imeboje, rgb_r, rgb_g, rgb_b) FROM stdin;
1	plava	0	0	255
2	Nezrela banana	204	255	51
3	Baby pink	255	199	236
4	Lavender	229	199	255
5	Purple wedding	65	7	34
\.


--
-- Name: bojasucelja_idboje_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bojasucelja_idboje_seq', 5, true);


--
-- Data for Name: font; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY font (idfonta, imefonta, velicinafonta, stilfonta) FROM stdin;
2	Arial	15	bold  
1	Times New Roman	20	plain 
3	DejaVu Sans	20	italic
4	Old London	22	plain 
5	Comic Sans MS	19	plain 
6	Serif	14	bold  
7	Verdana	14	bold  
8	Georgia	15	bold  
\.


--
-- Name: font_idfonta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('font_idfonta_seq', 9, true);


--
-- Data for Name: kontakti; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kontakti (idvlasnika, idkontakta, status, nadimak) FROM stdin;
2	3	t	netko
\.


--
-- Data for Name: korisnik; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY korisnik (idkorisnika, korisnickoime, lozinka, aktivan, email, idbojafonta, idfont, idavatar, idbojasucelja) FROM stdin;
2	zlatko.hrastic	5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8	t	zlatko.hrastic@fer.hr	1	1	1	1
5	zoran.karamarko	d9dd7135972df03ac7eae9c8c2c418abbe1745e6	t	zoran.karamarko@gmail.com	1	1	1	1
7	mujo	9633ef86687fa9565c7641b22344927e785c5250	t	mujo@mujo.hr	1	1	1	1
8	ottosinger	AF933EAD2B9BDDBE90F7BB47C6D637C4E654F58E	t	otto.singer@fer.hr	1	1	1	1
9	vucko	8c6a06670992025c8c8f6c8bd3a7db418bce3039	t	vucko@vucko.hr	1	1	1	1
3	vekszorp	b446080ccff9f99de750fac9db98c7cde151fd8e	t	vedran.priscan@fer.hr	1	6	1	1
10	pero	98FC7B34760FACE5E268BFF318180E05861A970F	t	pero@pero.pero	1	2	1	1
11	Agneza	b687ea9353a11e817a77bfaea0a16194ab5f8307	t	agneza.sandic@fer.hr	2	1	3	3
4	mkalebota	648c06b8459ba15d459f13e369847704815207d	t	marija.kalebota@fer.hr	1	1	1	1
\.


--
-- Name: korisnik_idkorisnika_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('korisnik_idkorisnika_seq', 11, true);


--
-- Data for Name: poruka; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY poruka (idporuke, idrazgovora, idposiljatelja, tekst, vrijemeslanja) FROM stdin;
1	1	2	tekst poruke	2015-11-14 10:21:41.720219
2	5	2	bla	2015-12-11 18:06:17.407231
4	5	2	molim te radi	2015-12-11 18:38:00.550686
5	5	2	katarian ili Silvija?	2015-12-11 18:48:51.444645
6	5	2	katarian ili Silvija?	2015-12-11 18:49:12.620277
7	5	2	katarian ili Silvija?bfdsbdsf	2015-12-11 18:49:21.203742
8	5	2	vani je	2015-12-11 18:55:11.740456
9	5	2	sretni smo	2015-12-11 19:01:01.326828
\.


--
-- Name: poruka_idporuke_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('poruka_idporuke_seq', 10, true);


--
-- Data for Name: pripadnost_razgovoru; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pripadnost_razgovoru (idrazgovora, idsudionika, status) FROM stdin;
1	2	t
2	2	t
4	2	t
5	2	t
\.


--
-- Data for Name: razgovor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY razgovor (idrazgovora, imerazgovora, vrijemepocetka) FROM stdin;
1	Odlazak na palacinke	2015-11-14 10:19:23.085565
2	Dogovor  za sastanak	2015-11-14 10:34:10.696254
3	webici	2015-12-11 16:18:09.948779
4	Katarina ili Silvija	2015-12-11 16:46:07.258162
5	Katarina ili Silvija	2015-12-11 16:47:01.574238
\.


--
-- Name: razgovor_idrazgovora_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('razgovor_idrazgovora_seq', 5, true);


--
-- Name: avatar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY avatar
    ADD CONSTRAINT avatar_pkey PRIMARY KEY (idavatar);


--
-- Name: bojafonta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bojafonta
    ADD CONSTRAINT bojafonta_pkey PRIMARY KEY (idboje);


--
-- Name: bojasucelja_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bojasucelja
    ADD CONSTRAINT bojasucelja_pkey PRIMARY KEY (idboje);


--
-- Name: font_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY font
    ADD CONSTRAINT font_pkey PRIMARY KEY (idfonta);


--
-- Name: kontakti_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kontakti
    ADD CONSTRAINT kontakti_pkey PRIMARY KEY (idvlasnika, idkontakta);


--
-- Name: korisnik_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_email_key UNIQUE (email);


--
-- Name: korisnik_korisnickoime_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_korisnickoime_key UNIQUE (korisnickoime);


--
-- Name: korisnik_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_pkey PRIMARY KEY (idkorisnika);


--
-- Name: poruka_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY poruka
    ADD CONSTRAINT poruka_pkey PRIMARY KEY (idporuke);


--
-- Name: pripadnost_razgovoru_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pripadnost_razgovoru
    ADD CONSTRAINT pripadnost_razgovoru_pkey PRIMARY KEY (idrazgovora, idsudionika);


--
-- Name: razgovor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY razgovor
    ADD CONSTRAINT razgovor_pkey PRIMARY KEY (idrazgovora);


--
-- Name: kontakti_idkontakta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kontakti
    ADD CONSTRAINT kontakti_idkontakta_fkey FOREIGN KEY (idkontakta) REFERENCES korisnik(idkorisnika);


--
-- Name: kontakti_idvlasnika_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kontakti
    ADD CONSTRAINT kontakti_idvlasnika_fkey FOREIGN KEY (idvlasnika) REFERENCES korisnik(idkorisnika);


--
-- Name: korisnik_idavatar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idavatar_fkey FOREIGN KEY (idavatar) REFERENCES avatar(idavatar);


--
-- Name: korisnik_idbojafonta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idbojafonta_fkey FOREIGN KEY (idbojafonta) REFERENCES bojafonta(idboje);


--
-- Name: korisnik_idbojasucelja_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idbojasucelja_fkey FOREIGN KEY (idbojasucelja) REFERENCES bojasucelja(idboje);


--
-- Name: korisnik_idfont_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY korisnik
    ADD CONSTRAINT korisnik_idfont_fkey FOREIGN KEY (idfont) REFERENCES font(idfonta);


--
-- Name: poruka_idposiljatelja_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poruka
    ADD CONSTRAINT poruka_idposiljatelja_fkey FOREIGN KEY (idposiljatelja) REFERENCES korisnik(idkorisnika);


--
-- Name: poruka_idrazgovora_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poruka
    ADD CONSTRAINT poruka_idrazgovora_fkey FOREIGN KEY (idrazgovora) REFERENCES razgovor(idrazgovora);


--
-- Name: pripadnost_razgovoru_idrazgovora_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pripadnost_razgovoru
    ADD CONSTRAINT pripadnost_razgovoru_idrazgovora_fkey FOREIGN KEY (idrazgovora) REFERENCES razgovor(idrazgovora);


--
-- Name: pripadnost_razgovoru_idsudionika_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pripadnost_razgovoru
    ADD CONSTRAINT pripadnost_razgovoru_idsudionika_fkey FOREIGN KEY (idsudionika) REFERENCES korisnik(idkorisnika);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: avatar; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE avatar FROM PUBLIC;
REVOKE ALL ON TABLE avatar FROM postgres;
GRANT ALL ON TABLE avatar TO postgres;
GRANT SELECT,INSERT ON TABLE avatar TO defaultuser;


--
-- Name: avatar_idavatara_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE avatar_idavatara_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE avatar_idavatara_seq FROM postgres;
GRANT ALL ON SEQUENCE avatar_idavatara_seq TO postgres;
GRANT USAGE ON SEQUENCE avatar_idavatara_seq TO defaultuser;


--
-- Name: bojafonta; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE bojafonta FROM PUBLIC;
REVOKE ALL ON TABLE bojafonta FROM postgres;
GRANT ALL ON TABLE bojafonta TO postgres;
GRANT SELECT,INSERT ON TABLE bojafonta TO defaultuser;


--
-- Name: bojafonta_idboje_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE bojafonta_idboje_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bojafonta_idboje_seq FROM postgres;
GRANT ALL ON SEQUENCE bojafonta_idboje_seq TO postgres;
GRANT USAGE ON SEQUENCE bojafonta_idboje_seq TO defaultuser;


--
-- Name: bojasucelja; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE bojasucelja FROM PUBLIC;
REVOKE ALL ON TABLE bojasucelja FROM postgres;
GRANT ALL ON TABLE bojasucelja TO postgres;
GRANT SELECT,INSERT ON TABLE bojasucelja TO defaultuser;


--
-- Name: bojasucelja_idboje_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE bojasucelja_idboje_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bojasucelja_idboje_seq FROM postgres;
GRANT ALL ON SEQUENCE bojasucelja_idboje_seq TO postgres;
GRANT USAGE ON SEQUENCE bojasucelja_idboje_seq TO defaultuser;


--
-- Name: font; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE font FROM PUBLIC;
REVOKE ALL ON TABLE font FROM postgres;
GRANT ALL ON TABLE font TO postgres;
GRANT SELECT,INSERT ON TABLE font TO defaultuser;


--
-- Name: font_idfonta_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE font_idfonta_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE font_idfonta_seq FROM postgres;
GRANT ALL ON SEQUENCE font_idfonta_seq TO postgres;
GRANT USAGE ON SEQUENCE font_idfonta_seq TO defaultuser;


--
-- Name: kontakti; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE kontakti FROM PUBLIC;
REVOKE ALL ON TABLE kontakti FROM postgres;
GRANT ALL ON TABLE kontakti TO postgres;
GRANT SELECT,INSERT ON TABLE kontakti TO defaultuser;


--
-- Name: korisnik; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE korisnik FROM PUBLIC;
REVOKE ALL ON TABLE korisnik FROM postgres;
GRANT ALL ON TABLE korisnik TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE korisnik TO defaultuser;


--
-- Name: korisnik_idkorisnika_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE korisnik_idkorisnika_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE korisnik_idkorisnika_seq FROM postgres;
GRANT ALL ON SEQUENCE korisnik_idkorisnika_seq TO postgres;
GRANT USAGE ON SEQUENCE korisnik_idkorisnika_seq TO defaultuser;


--
-- Name: poruka; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE poruka FROM PUBLIC;
REVOKE ALL ON TABLE poruka FROM postgres;
GRANT ALL ON TABLE poruka TO postgres;
GRANT SELECT,INSERT ON TABLE poruka TO defaultuser;


--
-- Name: poruka_idporuke_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE poruka_idporuke_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE poruka_idporuke_seq FROM postgres;
GRANT ALL ON SEQUENCE poruka_idporuke_seq TO postgres;
GRANT USAGE ON SEQUENCE poruka_idporuke_seq TO defaultuser;


--
-- Name: pripadnost_razgovoru; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE pripadnost_razgovoru FROM PUBLIC;
REVOKE ALL ON TABLE pripadnost_razgovoru FROM postgres;
GRANT ALL ON TABLE pripadnost_razgovoru TO postgres;
GRANT SELECT,INSERT ON TABLE pripadnost_razgovoru TO defaultuser;


--
-- Name: razgovor; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE razgovor FROM PUBLIC;
REVOKE ALL ON TABLE razgovor FROM postgres;
GRANT ALL ON TABLE razgovor TO postgres;
GRANT SELECT,INSERT ON TABLE razgovor TO defaultuser;


--
-- Name: razgovor_idrazgovora_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE razgovor_idrazgovora_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE razgovor_idrazgovora_seq FROM postgres;
GRANT ALL ON SEQUENCE razgovor_idrazgovora_seq TO postgres;
GRANT USAGE ON SEQUENCE razgovor_idrazgovora_seq TO defaultuser;


--
-- PostgreSQL database dump complete
--

