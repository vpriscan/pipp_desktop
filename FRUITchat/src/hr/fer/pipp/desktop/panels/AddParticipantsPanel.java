package hr.fer.pipp.desktop.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;

import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;

import hr.fer.pipp.desktop.actions.CreateNewConversationAction;
import hr.fer.pipp.desktop.actions.UserToConversationAdder;
import hr.fer.pipp.desktop.frames.AddParticipantsFrame;
import hr.fer.pipp.desktop.models.ComboItem;
import hr.fer.pipp.desktop.models.Contact;
import hr.fer.pipp.desktop.models.Conversation;
import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.panels.NewConversationPanel.ContactListPanel;
import hr.fer.pipp.desktop.util.LocalDataStorage;

public class AddParticipantsPanel extends WebPanel {
	private static final long serialVersionUID = 6263430387415429867L;
	
	//unutarnja pomocna klasa
	private class ContactCheckPanel extends WebPanel{
		private static final long serialVersionUID = -3747879441595504311L;
		private WebLabel contactAvatar;
		private WebCheckBox contactCheckBox;
		private Contact contact;
		
		public ContactCheckPanel(Contact contact, LocalDataStorage localDataStorage) {
			ContactCheckPanel.this.contact = contact;
			//springLayout2 je MALI SpringLayout u kojeg ce ic WebCheckBox i avatar, i onda cemo tih malih SpringLayouta naslagat u BoxLayout jednog ispod drugog i tako dobit listu checkboxova
			SpringLayout springLayout2 = new SpringLayout(); 
			ContactCheckPanel.this.setLayout(springLayout2);
			ContactCheckPanel.this.setOpaque(false);
			
			String helpString;
			if(contact.getNickname()!=null && !contact.getNickname().equals("")){
				helpString = contact.getNickname() + " (" + contact.getUsername() + ")"; 
			}
			else{
				helpString = contact.getUsername();
			}
			
			contactCheckBox = new WebCheckBox(helpString);
			
			springLayout2.putConstraint(SpringLayout.NORTH, contactCheckBox, 1, SpringLayout.NORTH, ContactCheckPanel.this);
			springLayout2.putConstraint(SpringLayout.WEST, contactCheckBox, 1, SpringLayout.WEST, ContactCheckPanel.this);
			springLayout2.putConstraint(SpringLayout.EAST, contactCheckBox, -50/*mjesta za avatare*/, SpringLayout.EAST, ContactCheckPanel.this);
			springLayout2.putConstraint(SpringLayout.SOUTH, contactCheckBox, -1, SpringLayout.SOUTH, ContactCheckPanel.this);
			
			ContactCheckPanel.this.add(contactCheckBox);
			
			contactAvatar = new WebLabel();
			ComboItem<ImageIcon> realAvatar = localDataStorage.getAvatar(contact.getAvatarID());
			Image newImg = realAvatar.getValue().getImage().getScaledInstance(30, 30, Image.SCALE_SMOOTH); //slike unutar WebLabela
			ImageIcon scaledAvatar = new ImageIcon(newImg);
			contactAvatar.setIcon(scaledAvatar);
			
			springLayout2.putConstraint(SpringLayout.NORTH, contactAvatar, 0, SpringLayout.NORTH, contactCheckBox);
			springLayout2.putConstraint(SpringLayout.WEST, contactAvatar, 5, SpringLayout.EAST, contactCheckBox);
			springLayout2.putConstraint(SpringLayout.EAST, contactAvatar, -5, SpringLayout.EAST, ContactCheckPanel.this);
			springLayout2.putConstraint(SpringLayout.SOUTH, contactAvatar, 0, SpringLayout.SOUTH, contactCheckBox);
			
			ContactCheckPanel.this.add(contactAvatar);
			
			ContactCheckPanel.this.setFontColors(localDataStorage.getFontColor(localDataStorage.getThisUser().getPreferredFontColorID()).getValue());
			ContactCheckPanel.this.setFonts(localDataStorage.getFont(localDataStorage.getThisUser().getPreferredFontID()).getValue());
			
		}
		private void setFonts(Font value) {
			contactCheckBox.setFont(value);
		}
		private void setFontColors(Color value) {
			contactCheckBox.setForeground(value);
		}
		public boolean isChecked(){
			return contactCheckBox.isSelected();
		}
		public int contactID(){
			return contact.getUserID();
		}
	}
	//druga unutarnja anonimna klasa
	public class ContactListPanel extends WebPanel{
		private static final long serialVersionUID = 5689420652217497326L;
		private List<ContactCheckPanel> contactCheckPanelList;
		
		public ContactListPanel(Collection<Contact> collection, LocalDataStorage localDataStorage) {
			
			ContactListPanel.this.setLayout(new BoxLayout(ContactListPanel.this, BoxLayout.Y_AXIS));
			ContactListPanel.this.setBackground(Color.PINK);
			ContactListPanel.this.setOpaque(false); //TODO kad zavrsimo stavit opaque false i maknut rozu
			
			contactCheckPanelList = new ArrayList<>();
			
			for(Contact contact: collection){ //za svakog kontakta napravi njegov contactCheckPanel
				ContactCheckPanel contactCheckPanel = new ContactCheckPanel(contact, localDataStorage);
				contactCheckPanel.setPreferredSize(new Dimension(ContactListPanel.this.getWidth(), 30)); //al width ignorira jer nemamo horizontalglue pa ga ionako rastegne
				
				contactCheckPanelList.add(contactCheckPanel); //dodavanje u listu ContactCheckPanel-a
				ContactListPanel.this.add(contactCheckPanel); //dodavanje na ContactListPanel
				
			}
			ContactListPanel.this.add(Box.createVerticalGlue()); //ovo je da se ne rastezu komponente kao
		}
		
		public List<Integer> getCheckedContactIDs(){ //lista id-jeva kontakata ciji checkbox je checkiran
			List<Integer> checkedContactIDsList = new ArrayList<>();
			
			for(ContactCheckPanel contactCheckPanel: contactCheckPanelList){
				if(contactCheckPanel.isChecked()){ //za svaki contactCheckPanel gdje je checkbox checkiran
					checkedContactIDsList.add(contactCheckPanel.contactID());
				}
			}
			return checkedContactIDsList;
		}
	}
		
	private ContactListPanel contactListPanel;
	private WebButton addToConversation;

	public AddParticipantsPanel(Connection connection, LocalDataStorage localDataStorage,
			Conversation conversation, AddParticipantsFrame frame) {
		
		SpringLayout springLayout = new SpringLayout();
		AddParticipantsPanel.this.setLayout(springLayout);
		
		ThisUser thisUser = localDataStorage.getThisUser(); //dobili thisusera
		
		contactListPanel = new ContactListPanel(thisUser.getContacts(), localDataStorage);
		WebScrollPane contactListScroll = new WebScrollPane(contactListPanel);
		contactListScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		contactListScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		contactListScroll.getViewport().setOpaque(false);
		contactListScroll.setOpaque(false);
		contactListScroll.getWebVerticalScrollBar().setUnitIncrement(16);
		
		AddParticipantsPanel.this.add(contactListScroll);
		
		addToConversation = new WebButton("Dodaj u razgovor");
		addToConversation.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				List<Integer> checkedContactIDsList = contactListPanel.getCheckedContactIDs();
				for(Integer checkedContactID: checkedContactIDsList){
					UserToConversationAdder.addUserToConversation(connection, conversation.getConversationID(), checkedContactID);
				}
				frame.dispose();
			}	
		});
		
		AddParticipantsPanel.this.add(addToConversation);
		//svejedno je li this.add() iznad ili ispod springLayout.putConstraint()
		springLayout.putConstraint(SpringLayout.NORTH , addToConversation, -30, SpringLayout.SOUTH, addToConversation);
		springLayout.putConstraint(SpringLayout.WEST, addToConversation, 40, SpringLayout.WEST, AddParticipantsPanel.this);
		springLayout.putConstraint(SpringLayout.SOUTH, addToConversation, -40, SpringLayout.SOUTH, AddParticipantsPanel.this);
		springLayout.putConstraint(SpringLayout.EAST, addToConversation, -40, SpringLayout.EAST, AddParticipantsPanel.this);
		
		
		springLayout.putConstraint(SpringLayout.NORTH , contactListScroll, 20, SpringLayout.NORTH, AddParticipantsPanel.this);
		springLayout.putConstraint(SpringLayout.WEST, contactListScroll, 40, SpringLayout.WEST, AddParticipantsPanel.this);
		springLayout.putConstraint(SpringLayout.SOUTH, contactListScroll, -20, SpringLayout.NORTH, addToConversation);
		springLayout.putConstraint(SpringLayout.EAST, contactListScroll, -40, SpringLayout.EAST, AddParticipantsPanel.this);
		
		
		setBackgroundColors(localDataStorage.getInterfaceColor(localDataStorage.getThisUser().getPreferredInterfaceColorID()).getValue());
		setFontColors(localDataStorage.getFontColor(localDataStorage.getThisUser().getPreferredFontColorID()).getValue());
		setFonts(localDataStorage.getFont(localDataStorage.getThisUser().getPreferredFontID()).getValue());
	}
	public void setFonts(Font font) {
		addToConversation.setFont(font);
	}
	public void setBackgroundColors(Color color) {
		this.setBackground(color);
	}
	public void setFontColors(Color color) {
	}

}
