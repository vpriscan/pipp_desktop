package hr.fer.pipp.desktop.panels;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import com.alee.laf.label.WebLabel;

import hr.fer.pipp.desktop.frames.SearchResultsFrame;
import hr.fer.pipp.desktop.models.ComboItem;
import hr.fer.pipp.desktop.models.MyLabel;
import hr.fer.pipp.desktop.models.MyTextField;
import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.util.LocalDataStorage;
import hr.fer.pipp.desktop.util.Refresher;

public class HeaderPanel extends AbstractMyPanel{

	private static final long serialVersionUID = -5309840085952538957L;
	private MyTextField search;  //dolje su lokalne varijable, da ih mozemo koristiti van
	private WebLabel username;
	private WebLabel logo;
	
	public HeaderPanel(Connection connection, LocalDataStorage localDataStorage) {
			SpringLayout springLayout = new SpringLayout();
			this.setLayout(springLayout);
			this.setOpaque(false);
			
			ThisUser thisUser = localDataStorage.getThisUser(); //dobili thisusera
			int avatarID = thisUser.getAvatarID(); //ivukli id avatara
			
			ComboItem<ImageIcon> realAvatar = localDataStorage.getAvatar(avatarID);
			Image newImg = realAvatar.getValue().getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH); //skaliranje
			ImageIcon scaledAvatar = new ImageIcon(newImg);
			
			MyLabel avatarLabel = new MyLabel(scaledAvatar, realAvatar.getID());  //zatim nac iz lok baze avatara s tim id, avatar vec u kockici
			this.add(avatarLabel);
			
			springLayout.putConstraint(SpringLayout.NORTH , avatarLabel, 0, SpringLayout.NORTH, this);
			springLayout.putConstraint(SpringLayout.WEST, avatarLabel, -52, SpringLayout.EAST, this);
			springLayout.putConstraint(SpringLayout.EAST, avatarLabel, -12, SpringLayout.EAST , this); 
			springLayout.putConstraint(SpringLayout.SOUTH, avatarLabel, -0, SpringLayout.SOUTH, this);
			
			Refresher.subscribeToAvatarChange(avatarLabel); //ovo je da se moze mijenjat
			
			logo = new WebLabel();
			logo.setIcon(localDataStorage.getLogo());
			Refresher.subscribeLogoLabel(logo);
			this.add(logo);
			springLayout.putConstraint(SpringLayout.NORTH , logo, 0, SpringLayout.NORTH, this);
			springLayout.putConstraint(SpringLayout.WEST, logo, 5, SpringLayout.WEST, this);
			/*springLayout.putConstraint(SpringLayout.EAST, logo, 230, SpringLayout.WEST, this); //ne treba jer se po slici u sebi resizea
			springLayout.putConstraint(SpringLayout.SOUTH, logo, -10, SpringLayout.SOUTH, this);
			*/
			
			
			username = new WebLabel(thisUser.getUsername());
			username.setFont(new Font(username.getFont().getName(), username.getFont().getStyle(), username.getFont().getSize()+5));
			this.add(username);
			springLayout.putConstraint(SpringLayout.NORTH , username, 10, SpringLayout.NORTH, this);
			springLayout.putConstraint(SpringLayout.EAST, username, -15, SpringLayout.WEST, avatarLabel);
			springLayout.putConstraint(SpringLayout.SOUTH, username, -10, SpringLayout.SOUTH, this);
			
			search = new MyTextField();
			String searchHint = "Traži korisnika";
			search.setHint(searchHint);
			this.add(search);
			springLayout.putConstraint(SpringLayout.NORTH , search, 5, SpringLayout.NORTH, this);
			springLayout.putConstraint(SpringLayout.WEST, search, 235, SpringLayout.WEST, this);
			springLayout.putConstraint(SpringLayout.SOUTH, search, -5, SpringLayout.SOUTH, this);
			springLayout.putConstraint(SpringLayout.EAST, search, 500, SpringLayout.WEST, this);
			
			search.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(search.getText().equals(searchHint)){
						search.setText("");
					}
					SearchResultsFrame searchResultsFrame = new SearchResultsFrame(search, connection, localDataStorage);
					searchResultsFrame.setVisible(true); //da se pojavi na ekranu
					
				}
			});
			
		
			
			this.subscribeComponentsForLooksChange();
	}

	@Override
	public void subscribeComponentsForLooksChange() {
		Refresher.subscribeToFontChange(search);
		Refresher.subscribeToFontChange(username);
		
		Refresher.subscribeToFontColorChange(username);
		Refresher.subscribeToFontColorChange(logo);
	}
}
