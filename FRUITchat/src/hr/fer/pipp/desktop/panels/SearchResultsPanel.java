package hr.fer.pipp.desktop.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import com.alee.laf.button.WebButton;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;

import hr.fer.pipp.desktop.actions.AddOrRemoveContactAction;
import hr.fer.pipp.desktop.actions.BasicUserDataGetter;
import hr.fer.pipp.desktop.models.ComboItem;
import hr.fer.pipp.desktop.models.Contact;
import hr.fer.pipp.desktop.models.User;
import hr.fer.pipp.desktop.util.LocalDataStorage;

public class SearchResultsPanel extends WebPanel{
	
	private static final long serialVersionUID = 4322938060697134916L;
	
	private User selectedUser = null;
	private String addContactText = "Dodaj u kontakte", removeContactText = "Izbrisi iz kontakata";
	private WebLabel username;
	private WebButton addOrRemoveContact; 
	
	public SearchResultsPanel(JTextField search, Connection connection, LocalDataStorage localDataStorage) {
		SpringLayout springLayout = new SpringLayout();
		this.setLayout(springLayout);
		
		List<User> userList = BasicUserDataGetter.getFilteredUsersData(search.getText(), connection);
		User[] userArray = userList.toArray(new User[userList.size()]);
		WebComboBox comboBox = new WebComboBox(userArray);
		
		comboBox.setEditable(false);
		
		this.add(comboBox);
		springLayout.putConstraint(SpringLayout.NORTH , comboBox, 30, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.EAST, comboBox, -30, SpringLayout.EAST, this);
		springLayout.putConstraint(SpringLayout.WEST, comboBox, 30, SpringLayout.WEST , this); 
		springLayout.putConstraint(SpringLayout.SOUTH, comboBox, 65, SpringLayout.NORTH, this);
		
		username = new WebLabel("Korisničko ime: -");
		this.add(username);
		springLayout.putConstraint(SpringLayout.NORTH , username, 50, SpringLayout.SOUTH, comboBox);
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, username, 0, SpringLayout.HORIZONTAL_CENTER, this);
		springLayout.putConstraint(SpringLayout.SOUTH, username, 85, SpringLayout.SOUTH, comboBox);

		WebLabel avatarLabel = new WebLabel();
		this.add(avatarLabel);
		
		springLayout.putConstraint(SpringLayout.NORTH , avatarLabel, 25, SpringLayout.SOUTH, username);
		springLayout.putConstraint(SpringLayout.WEST, avatarLabel, -50, SpringLayout.HORIZONTAL_CENTER, this);
		springLayout.putConstraint(SpringLayout.EAST, avatarLabel, 50, SpringLayout.HORIZONTAL_CENTER , this); 
		springLayout.putConstraint(SpringLayout.SOUTH, avatarLabel, 125, SpringLayout.SOUTH, username);
		
		AddOrRemoveContactAction addOrRemoveContactAction = new AddOrRemoveContactAction(connection, localDataStorage);
		addOrRemoveContact = new WebButton("-");
		addOrRemoveContact.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				addOrRemoveContactAction.actionPerformed(e);
				
				if(addOrRemoveContact.getText().equals(removeContactText)){
					addOrRemoveContact.setText(addContactText);
				}else if(addOrRemoveContact.getText().equals(addContactText)){
					addOrRemoveContact.setText(removeContactText);
				}
			}
		});
		this.add(addOrRemoveContact);
		
		springLayout.putConstraint(SpringLayout.NORTH , addOrRemoveContact, 25, SpringLayout.SOUTH, avatarLabel);
		springLayout.putConstraint(SpringLayout.WEST, addOrRemoveContact, 100, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, addOrRemoveContact, -100, SpringLayout.EAST , this); 
		springLayout.putConstraint(SpringLayout.SOUTH, addOrRemoveContact, 75, SpringLayout.SOUTH, avatarLabel);
		
		comboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedUser = (User) comboBox.getSelectedItem();
				Contact potentialContact = new Contact(selectedUser);
				addOrRemoveContactAction.setTargetedUser(potentialContact);
				
				username.setText("Korisničko ime: " +selectedUser.getUsername());
				
				ComboItem<ImageIcon> realAvatar = localDataStorage.getAvatar(selectedUser.getAvatarID());
				Image newImg = realAvatar.getValue().getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH); //skaliranje
				ImageIcon scaledAvatar = new ImageIcon(newImg);
				
				avatarLabel.setIcon(scaledAvatar);
				boolean isInContacts = localDataStorage.getThisUser().hasContact(selectedUser.getUserID());
				
				if(isInContacts){
					addOrRemoveContact.setText(removeContactText);
				}
				else {
					addOrRemoveContact.setText(addContactText);
				}
			}
		});
		if(comboBox.getItemCount()!=0){ /*obavezno nakon actionlistenera, da ga aktivira da se odmah pojave podatci prvog kandidata*/
			comboBox.setSelectedIndex(0);
		}
		
		setBackgroundColors(localDataStorage.getInterfaceColor(localDataStorage.getThisUser().getPreferredInterfaceColorID()).getValue());
		setFontColors(localDataStorage.getFontColor(localDataStorage.getThisUser().getPreferredFontColorID()).getValue());
		setAllFonts(localDataStorage.getFont(localDataStorage.getThisUser().getPreferredFontID()).getValue());
	}
	public void setAllFonts(Font font) {
		username.setFont(font);
		addOrRemoveContact.setFont(font);
	}
	public void setBackgroundColors(Color color) {
		this.setBackground(color);
		//Color buttonColor = new Color(color.getRed()/2, color.getGreen()/2, color.getBlue()/2);
		//addOrRemoveContact.setBackground(buttonColor);
	}
	public void setFontColors(Color color) {
		username.setForeground(color);
		//addOrRemoveContact.setForeground(color);
	}

}
