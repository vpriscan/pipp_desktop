package hr.fer.pipp.desktop.panels;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;

/**
 * nas apstraktni panel kojem smo dodali par metoda za izgled i za hintove
 */
public abstract class AbstractEntryPanel extends WebPanel{

	private static final long serialVersionUID = 1160784762873265635L;
	private String defaultPictureFilename = "banana.jpg"; //treba se nalaziti u bin folderu inace ce se negdje dogoditi nullpointerexception
	
	//imati vise metoda istog imena ali razlicitih argumenata se zove overloadanje
	//prva metoda se poziva bez slike kao parametra
	//pa ona zatim zove metodu putFruitPictureOnScreen sa defaultnom slikom
	
	public abstract void setFontColors(Color color);
	public abstract void setFonts(Font font);
	public abstract void setInterfaceColors(Color color);
	
	public void putFruitLogoOnPanel(SpringLayout springLayout, JPanel panel){
		putFruitLogoOnPanel(defaultPictureFilename, springLayout, panel);
	}
	
	public void putFruitLogoOnPanel(String pictureFilename, SpringLayout springLayout, JPanel panel){
		WebLabel fruit = new WebLabel(); //ovo je komponenta koja moze sadrzavati tekst ili sliku
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		ImageIcon imgIcon=new ImageIcon(classLoader.getResource(pictureFilename)); //vadimo sliku koja se nalazi u bin folderu
		fruit.setIcon(imgIcon);
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, fruit, 0, SpringLayout.HORIZONTAL_CENTER, this);
		springLayout.putConstraint(SpringLayout.SOUTH, fruit, 0, SpringLayout.SOUTH, this);
		panel.add(fruit);
	}
}
