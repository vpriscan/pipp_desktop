package hr.fer.pipp.desktop.panels;

import java.sql.Connection;

import javax.swing.*;

import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.scroll.WebScrollPane;

import java.awt.Dimension;
import java.awt.event.*;

import hr.fer.pipp.desktop.actions.SendMessageAction;
import hr.fer.pipp.desktop.models.MyTextArea;
import hr.fer.pipp.desktop.util.LocalDataStorage;
import hr.fer.pipp.desktop.util.Refresher;


public class InputPanel extends AbstractMyPanel implements KeyListener{
	
	private MyTextArea messageArea;
	private WebButton sendButton;
	private WebCheckBox enterCheckBox;
	
	
	private static final long serialVersionUID = -4996567386163792145L;

	public InputPanel(Connection connection, LocalDataStorage localDataStorage) {
		
		SpringLayout springLayout = new SpringLayout();
		this.setLayout(springLayout);
		this.setOpaque(false); //stavimo da je proziran pa ce preuzet boju nadkomponente (mainpanela)

		messageArea = new MyTextArea();
		messageArea.setLineWrap(true);
		messageArea.setWrapStyleWord(true);
		messageArea.addKeyListener(this); 
		String hint = "Napiši poruku:";
		messageArea.setHint(hint);
		
		WebScrollPane scroll = new WebScrollPane(messageArea);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.add(scroll);
		
		SendMessageAction sendMessageAction = new SendMessageAction(connection, localDataStorage, messageArea, hint);
		sendButton = new WebButton(sendMessageAction);
		sendButton.setText("Send");
		sendButton.setPreferredSize(new Dimension(80, 40));
		this.add(sendButton);
		
		enterCheckBox = new WebCheckBox ("Pritisni enter za slanje");
		enterCheckBox.setOpaque(false); //stavi da je proziran
		enterCheckBox.doClick();
		enterCheckBox.setOpaque(false); 
		/*
		Object newLineMapKey = messageArea.getInputMap().get(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0));
		messageArea.getActionMap().put("sendMessage", sendMessageAction);
		enterCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(enterCheckBox.isSelected()){
					messageArea.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "sendMessage");
					messageArea.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.SHIFT_DOWN_MASK), newLineMapKey);
					
				}
				else{
					messageArea.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), newLineMapKey);
					messageArea.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.SHIFT_DOWN_MASK), "doNothing");
				}
			}
		});*/
		this.add(enterCheckBox);
					
		springLayout.putConstraint(SpringLayout.NORTH , scroll, 5, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, scroll, 15, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, scroll, -15, SpringLayout.EAST , this); 
		springLayout.putConstraint(SpringLayout.SOUTH, scroll, 60, SpringLayout.NORTH, this);
		
		springLayout.putConstraint(SpringLayout.NORTH , sendButton, 5, SpringLayout.SOUTH, scroll);
		springLayout.putConstraint(SpringLayout.EAST, sendButton, -25, SpringLayout.EAST, this); 
		springLayout.putConstraint(SpringLayout.SOUTH, sendButton,-3 , SpringLayout.SOUTH, this);
				 
		springLayout.putConstraint(SpringLayout.NORTH , enterCheckBox, 5, SpringLayout.SOUTH, scroll);
		springLayout.putConstraint(SpringLayout.EAST, enterCheckBox, -20, SpringLayout.WEST, sendButton); 
		springLayout.putConstraint(SpringLayout.SOUTH, enterCheckBox,-3 , SpringLayout.SOUTH, this);
		
		this.subscribeComponentsForLooksChange();
	}
	@Override
    public void keyPressed(KeyEvent e){
		messageArea.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),"doNothing");
		if((e.getKeyCode() == KeyEvent.VK_ENTER) && enterCheckBox.isSelected()){
			if(!e.isShiftDown()){
				sendButton.doClick();
			}
			else{
				messageArea.append("\n");
			}
        }
		else if((e.getKeyCode() == KeyEvent.VK_ENTER) && !enterCheckBox.isSelected()){
			messageArea.append("\n");
		}
	}
	
    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    };

	
	@Override
	public void subscribeComponentsForLooksChange() {
		Refresher.subscribeToFontChange(messageArea);

		Refresher.subscribeToFontChange(sendButton);
		Refresher.subscribeToFontChange(enterCheckBox);
		//Refresher.subscribeToSecondaryColorChange(sendButton);
		//Refresher.subscribeToFontColorChange(sendButton);
		Refresher.subscribeToFontColorChange(enterCheckBox);
	}
}
