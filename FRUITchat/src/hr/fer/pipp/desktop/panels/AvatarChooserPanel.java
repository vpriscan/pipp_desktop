package hr.fer.pipp.desktop.panels;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JViewport;

import com.alee.laf.panel.WebPanel;

import hr.fer.pipp.desktop.actions.StyleAndAvatarStorer;
import hr.fer.pipp.desktop.models.ComboItem;
import hr.fer.pipp.desktop.models.MyLabel;
import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.models.User;
import hr.fer.pipp.desktop.util.LocalDataStorage;
import hr.fer.pipp.desktop.util.Refresher;
import hr.fer.pipp.desktop.util.WrapLayout;

/**
 * panel koji se stavi u JScrollPane u AvatarChooserFrame-u,
 * u njemu su prikazani svi avatari (povuceni iz baze u trenutku prijave) u obliku WebLabel-a
 * koji mogu reagirati na klik misa
 */
public class AvatarChooserPanel extends WebPanel{
	
	private static final long serialVersionUID = 4876996688853925273L;

	public AvatarChooserPanel(Connection connection, LocalDataStorage localDataStorage) {
		ThisUser thisUser = localDataStorage.getThisUser();
		WrapLayout layout = new WrapLayout();
		this.setLayout(layout);
		this.setSize(new Dimension(300, 1));
		JPanel thisPanel = this;
		
		int oldAvatarID = thisUser.getAvatarID();
		
		for(ComboItem<ImageIcon> avatar: localDataStorage.getAllAvatars()){
			Image newImg = avatar.getValue().getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
			ImageIcon scaledAvatar = new ImageIcon(newImg);
			MyLabel avatarLabel = new MyLabel(scaledAvatar, avatar.getID());
			avatarLabel.setPreferredSize(new Dimension(104, 104));
			avatarLabel.setBackground(Color.GREEN);
			if(avatar.getID()==thisUser.getAvatarID())
				avatarLabel.setOpaque(true);
			else 
				avatarLabel.setOpaque(false);
			avatarLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			avatarLabel.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e){
					boolean successful = StyleAndAvatarStorer.storeNewAvatar(connection, thisUser, avatar);
					if(successful) {
						thisUser.setAvatarID(avatar.getID());
						User temp = localDataStorage.getUser(thisUser.getUserID());
						temp.setAvatarID(avatar.getID());
						localDataStorage.insertUser(temp);
						
						JViewport parent = (JViewport) thisPanel.getParent();
						parent.remove(thisPanel); //ne znam mozda se ovo moglo i bolje
						parent.add(new AvatarChooserPanel(connection, localDataStorage));
						parent.repaint();
						
						Refresher.executeChangeAvatarIcons(oldAvatarID, avatar);
					}
				}
			});
			this.add(avatarLabel);
			setBackgroundColors(localDataStorage.getInterfaceColor(localDataStorage.getThisUser().getPreferredInterfaceColorID()).getValue());
		}
	}
	
	public void setBackgroundColors(Color color) {
		this.setBackground(color);
		/*ne subscribeati jer se dogodi kao memory leak jer zatvaramo prozor a stari panel ostaje u listi u refresheru zauvijek*/
		
	}
}
