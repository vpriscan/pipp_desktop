package hr.fer.pipp.desktop.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;

import hr.fer.pipp.desktop.actions.ForgotPasswordAction;
import hr.fer.pipp.desktop.actions.LoginAction;
import hr.fer.pipp.desktop.frames.LoginFrame;
import hr.fer.pipp.desktop.frames.RegistrationFrame;
import hr.fer.pipp.desktop.models.MyPasswordField;
import hr.fer.pipp.desktop.models.MyTextField;

/**
 * panel za prijavu
 */
public class LoginPanel extends AbstractEntryPanel{ //iz JPanela dolaze sve metode koje cemo mi koristit
	
	//bolje ovde nego u konstruktoru, jer ih tad ne bismo mogli zvat iz drugih klasa koje budemo radili
	private MyTextField usernameOrEmailField;
	private MyPasswordField passwordField;
	private WebButton logInButton;
	private WebButton forgotPasswordButton;
	private WebButton registrationButton;
	
	//private ForgotPasswordAction forgotPasswordAction;
	private LoginAction loginAction;
	
	private static final long serialVersionUID = 3724545321435411529L;
	
	public LoginPanel(Color defaultBackgroundColor, Font defaultFont, LoginFrame loginFrame) { // konstruktor panela
		loginAction = new LoginAction(this, loginFrame);
		//forgotPasswordAction = new ForgotPasswordAction();
		
		//zelimo dodavat komponente - a kako zelimo da to izgleda?
		//U Javi vec postoje gotovi layouti.
		//one Vedranove slicice u inboxu su bile Spring layout - jer granice komponenata postavljas u odnosu na nesto drugo
		SpringLayout springLayout = new SpringLayout();//stvorili objekt tipa SpringLayout
		//sada ga treba dodat na panel
		//layouti se dodaju sa SetLayout - to je metoda iz JPanela
		this.setLayout(springLayout); //this se odnosi na to da kad pozoves ovaj konstruktor (kojeg upravo pisemo) kad stvaras neki novi objekt tipa LoginPanel, taj this se odnosi na TAJ KONKRETAN OBJEKT
		//trebaju nam kucice za username i password, button za submit, i select dropdowni za boju i stil fonta
		
		//sad cemo sve te komponente "postavit po prozorcicu" 
		
		//prvo logo
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		ImageIcon logoIconWhite = new ImageIcon(classLoader.getResource("logoBlack.png"));
		Image logoImgWhite = logoIconWhite.getImage().getScaledInstance(250, 60, Image.SCALE_SMOOTH); //skaliranje
		ImageIcon scaledLogoIconWhite = new ImageIcon(logoImgWhite);
		WebLabel logo = new WebLabel(scaledLogoIconWhite);
		springLayout.putConstraint(SpringLayout.NORTH , logo, 30, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, logo, 0, SpringLayout.HORIZONTAL_CENTER, this);
		this.add(logo);
		
		usernameOrEmailField = new MyTextField(); // stvorili objekt
		usernameOrEmailField.setHint("korisničko ime");
		usernameOrEmailField.setPreferredSize(new Dimension(100, 40)); //malo povecali defaultnu dimenziju da ne bude pretanko
		//ta gori stotka nam zapravo nista trenutno ne znaci, jer ga dolje sidrimo sa west i east
		springLayout.putConstraint(SpringLayout.NORTH /*staticka privatna varijabla, jer pocinje velikim S; sidro za prvu komponentu*/, usernameOrEmailField, /*zeljena udaljenost*/ 30, SpringLayout.SOUTH, logo);
		springLayout.putConstraint(SpringLayout.WEST, usernameOrEmailField, 15, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, usernameOrEmailField, -15, SpringLayout.EAST, this);
		this.add(usernameOrEmailField); // tek se sad "VIDI"
		
		
		passwordField = new MyPasswordField(); //JPasswordField nasljeduje JTextField pa sve metode rade
		passwordField.setHint("lozinka");
		passwordField.setPreferredSize(new Dimension(100, 40)); 
		springLayout.putConstraint(SpringLayout.NORTH , passwordField, 20, SpringLayout.SOUTH, usernameOrEmailField);
		springLayout.putConstraint(SpringLayout.WEST, passwordField, 15, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, passwordField, -15, SpringLayout.EAST, this);
		this.add(passwordField);
		
		logInButton = new WebButton(loginAction); // konstruktor koji prima akciju koju smo mi definirali
		logInButton.setText("Prijava");
		logInButton.setPreferredSize(new Dimension(200, 30));
		springLayout.putConstraint(SpringLayout.NORTH, logInButton, 30, SpringLayout.SOUTH, passwordField);
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, logInButton, 0, SpringLayout.HORIZONTAL_CENTER, this);
		this.add(logInButton);
		
		/*forgotPasswordButton = new WebButton(forgotPasswordAction);
		forgotPasswordButton.setText("Zaboravili ste lozinku?");
		forgotPasswordButton.setPreferredSize(new Dimension(200, 30));
		springLayout.putConstraint(SpringLayout.NORTH, forgotPasswordButton, 20, SpringLayout.SOUTH, logInButton);
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, forgotPasswordButton, 0, SpringLayout.HORIZONTAL_CENTER, this);
		this.add(forgotPasswordButton);*/
		
		registrationButton = new WebButton("Registracija");//prije je ovdje bila akcija za registraciju ali preselili smo ju na novi gumb u novom prozoru
		registrationButton.setPreferredSize(new Dimension(200, 30));
		registrationButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				loginFrame.setVisible(false); //zasad cemo ga sakrit dok se korisnik registrira
				RegistrationFrame registrationFrame = new RegistrationFrame(loginFrame); //saljemo loginFrame kako bi ga nakon registracije registerAction mogao ponovno stavit kao vidljivog
				registrationFrame.setVisible(true); //prikazujemo registrationFrame na ekranu
			}
		});
		//springLayout.putConstraint(SpringLayout.NORTH, registrationButton, 20, SpringLayout.SOUTH, forgotPasswordButton);
		springLayout.putConstraint(SpringLayout.NORTH, registrationButton, 20, SpringLayout.SOUTH, logInButton);
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, registrationButton, 0, SpringLayout.HORIZONTAL_CENTER, this);
		this.add(registrationButton);
		
		//sad omogucujemo da se pomocu tipkovnice obavi neka akcija na komponenti:
		passwordField.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "loginkey");
		passwordField.getActionMap().put("loginkey", loginAction); //tj loginkey je kljuc za taj key event nad tom komponentom
		
		logInButton.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "loginkey");
		logInButton.getActionMap().put("loginkey", loginAction);
		
		//forgotPasswordButton.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "forgotkey");
		//forgotPasswordButton.getActionMap().put("forgotkey", forgotPasswordAction);
		
		setFonts(defaultFont);
		setInterfaceColors(defaultBackgroundColor);
		setFontColors(Color.WHITE);
		super.putFruitLogoOnPanel(springLayout, this);
	}
	@Override
	public void setFonts(Font font){//pozvati tek nakon konstruiranja svih komponenti na popisu
		usernameOrEmailField.setFont(font);
		passwordField.setFont(font);
		logInButton.setFont(font);
		//forgotPasswordButton.setFont(font);
		registrationButton.setFont(font);
		//komponente koje dodate na panel a mijenja im se font, dodati i ovdje
	}
	@Override
	public void setInterfaceColors(Color color){
		Color secondary = new Color(color.getRed()/2, color.getGreen()/2, color.getBlue()/2);
		this.setBackground(color);
		
		logInButton.setBottomBgColor(secondary);
		//forgotPasswordButton.setBottomBgColor(secondary);
		registrationButton.setBottomBgColor(secondary);
		
		logInButton.setTopBgColor(color);
		//forgotPasswordButton.setTopBgColor(color);
		registrationButton.setTopBgColor(color);
		
		logInButton.setBottomSelectedBgColor(logInButton.getTopBgColor());
		logInButton.setTopSelectedBgColor(logInButton.getTopBgColor());
		
		//forgotPasswordButton.setBottomSelectedBgColor(forgotPasswordButton.getTopBgColor());
		//forgotPasswordButton.setTopSelectedBgColor(forgotPasswordButton.getTopBgColor());
		
		registrationButton.setBottomSelectedBgColor(registrationButton.getTopBgColor());
		registrationButton.setTopSelectedBgColor(registrationButton.getTopBgColor());
		
		
		//komponente koje dodate na panel a da im treba promijeniti boju u defaultnu, stavite ovdje
	}
	@Override
	public void setFontColors(Color color) {
	}
	public LoginAction getLoginAction(){
		return loginAction;
	}
	public MyTextField getUsernameOrEmailField(){
		return usernameOrEmailField;
	}
	public MyPasswordField getPasswordField(){
		return passwordField;
	}
}