package hr.fer.pipp.desktop.panels;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.alee.laf.label.WebLabel;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.menu.WebPopupMenu;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;

import hr.fer.pipp.desktop.actions.ChatMessagesGetter;
import hr.fer.pipp.desktop.actions.ConversationsUpdater;
import hr.fer.pipp.desktop.actions.UserToConversationAdder;
import hr.fer.pipp.desktop.frames.AddParticipantsFrame;
import hr.fer.pipp.desktop.models.Contact;
import hr.fer.pipp.desktop.models.Conversation;
import hr.fer.pipp.desktop.models.Message;
import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.models.User;
import hr.fer.pipp.desktop.util.LocalDataStorage;
import hr.fer.pipp.desktop.util.Refresher;

/**
 * panel gdje se prikazuju poruke trenutno otvorenog razgovora
 */
public class ChatPanel extends AbstractMyPanel{
	private static final long serialVersionUID = -1703936795451208337L;
	
	private Connection connection;
	private ThisUser thisUser;
	private WebLabel conversationNameLabel;
	private WebLabel loadMoreMessages;
	private List<Message> messageList;
	private LocalDataStorage localDataStorage;
	private int numberOfMoreMessagesToLoad = 25;
	
	private GroupLayout.SequentialGroup hGroup, vGroup;
	private GroupLayout.ParallelGroup hGroup1, hGroup2;
	private GroupLayout layout;
	
	private ChatMessagesPanel chatMessagesPanel;
	private WebLabel conversationOptions;
	
	private int lastConversationID = -1;
	
	private class ChatMessagesPanel extends WebPanel{
		private static final long serialVersionUID = 7778730726634859130L;

		public ChatMessagesPanel(Conversation conversation) {
			ChatMessagesPanel.this.setOpaque(false);
			
			layout = new GroupLayout(this);
			ChatMessagesPanel.this.setLayout(layout);
			layout.setAutoCreateGaps(true);
			layout.setAutoCreateContainerGaps(true);
			
			hGroup = layout.createSequentialGroup();
			vGroup = layout.createSequentialGroup();
			hGroup1 = layout.createParallelGroup();
			hGroup2 = layout.createParallelGroup();
			
			messageList = new ArrayList<>();
			if(conversation!=null && conversation.getConversationID()!=lastConversationID){
				conversationNameLabel.setText(conversation.getConversationName());
				WebPopupMenu popupMenu = new WebPopupMenu();
				
				/**
				 * Stvaram preimenujItem ali ga tek addam u popupMenu u MouseListeneru (znaci tek kad se klikne desni klik na gumb)
				 * nije bilo potrebno svaki put iznova stvarat preimenujItem jer je isti za svaki gumb
				 */
				WebMenuItem preimenujItem = new WebMenuItem("Preimenuj razgovor");
				preimenujItem.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						String newConversationName = WebOptionPane.showInputDialog("Novo ime za razgovor:");
						ConversationsUpdater.updateConversationName(connection, conversation.getConversationID(), newConversationName);
					}
				});
				
				/**
				 * Stvaram izbrisiItem ali ga tek addam u popupMenu u MouseListeneru (znaci tek kad se klikne desni klik na gumb)
				 * nije bilo potrebno svaki put iznova stvarat izbrisiItem jer je isti za svaki gumb
				 */
				WebMenuItem izbrisiItem = new WebMenuItem("Izađi iz razgovora");
				izbrisiItem.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						int result = WebOptionPane.showConfirmDialog(null, "Jeste li sigurni da želite napustiti ovaj razgovor? Ako nema više članova, razgovor će se obrisati. Nećete moći poništiti ovu akciju.", "OPREZ", JOptionPane.YES_NO_OPTION);
						if (result == 0){
							Refresher.leaveConversation(conversation.getConversationID());
						}
						
					}
				});
				
				WebMenuItem dodajKorisnikaItem = new WebMenuItem("Dodaj sudionika");
				dodajKorisnikaItem.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						AddParticipantsFrame addParticipantsFrame = new AddParticipantsFrame(conversationOptions, connection, localDataStorage, conversation);
						addParticipantsFrame.setVisible(true);
					}
				});
				conversationOptions.addMouseListener(new MouseAdapter() {
					
					@Override
					public void mouseClicked(MouseEvent e) {
						if (SwingUtilities.isLeftMouseButton(e)){
							popupMenu.removeAll(); /*zato jer je sad deklaracija popupmenija vani, moramo removat da nam se stalno ne addaju novi itemi na stare pri svakom kliku*/
							
							WebMenuItem sudioniciItem = new WebMenuItem("Sudionici:");
							sudioniciItem.setEnabled(false);
							popupMenu.add(sudioniciItem);
							
							List<User> convMembers = conversation.getMembers();
							for(User member : convMembers){
								String helpString;
								if ( thisUser.hasContact(member.getUserID())){
									Contact contact = thisUser.getContact(member.getUserID());
									if(contact.getNickname()!=null && !contact.getNickname().equals("")){
										helpString = contact.getNickname() + " (" + contact.getUsername() + ")"; 
									}else{
										helpString = contact.getUsername();
									}
								}
								else{
									helpString = member.getUsername();
								}
								WebMenuItem sudionikItem = new WebMenuItem(helpString);
								sudionikItem.setEnabled(false);
								popupMenu.add(sudionikItem);
								
							}
							/*
							 * sad dodajemo one preimenujItem i izbrisiItem
							 * */
							popupMenu.addSeparator();
							popupMenu.add(dodajKorisnikaItem);
							popupMenu.addSeparator();
							popupMenu.add(preimenujItem);
							popupMenu.addSeparator();
							popupMenu.add(izbrisiItem);
							
							if(popupMenu.getWidth()==0){
								popupMenu.show(e.getComponent(), e.getX(), e.getY());
							}//ovo je kao neki bug, ne moze se nacratat jpopupmenu prema lijevo jer mu se ne zna sirina prije nego se nacrta
							popupMenu.show(e.getComponent(), e.getX() - popupMenu.getWidth(), e.getY());
						}
					}
				});
			}
			if(conversation!=null){
				lastConversationID = conversation.getConversationID();
				messageList = conversation.getMessages();
			}
			ListIterator<Message> iterator = messageList.listIterator(messageList.size());
			boolean myMessageIsHere = false;
			boolean othersMessageIsHere = false;
			while(iterator.hasPrevious()){ //idemo od nazad jer smo ih obrnuto dohvatili
				Message message = iterator.previous();
				if(message.getSenderID()!=thisUser.getUserID()){
					hGroup1.addComponent(message);
					othersMessageIsHere = true;
				}else{
					hGroup2.addComponent(message);
					myMessageIsHere = true;
				}
				vGroup.addGroup(layout.createParallelGroup().addComponent(message));
			}
			JPanel invisible = new JPanel();
			invisible.setOpaque(false);
			if(!othersMessageIsHere && myMessageIsHere){
				hGroup1.addComponent(invisible);
				vGroup.addGroup(layout.createParallelGroup().addComponent(invisible));
			}else if(othersMessageIsHere && !myMessageIsHere){
				hGroup2.addComponent(invisible);
				vGroup.addGroup(layout.createParallelGroup().addComponent(invisible));
			}
			hGroup.addGroup(hGroup1);
			hGroup.addGroup(hGroup2);
			layout.setHorizontalGroup(hGroup); //ovo je jedan poseban layout u kojem nema this.add(component) nego se u layout dodaju napravljene grupe
			layout.setVerticalGroup(vGroup);
			
			ChatMessagesPanel.this.revalidate(); //vazno, promjena velicine panela, inace se panel nece resizeat
			ChatMessagesPanel.this.repaint(); //vazno, promjena grafike panela, inace se nece promijenit sadrzaj panela
		}
	}

	public ChatPanel(Connection connection, LocalDataStorage localDataStorage, WebLabel conversationNameLabel, WebLabel conversationOptions) {
		
		this.connection = connection;
		this.thisUser = localDataStorage.getThisUser();
		this.localDataStorage = localDataStorage;
		this.conversationNameLabel = conversationNameLabel;
		this.conversationOptions = conversationOptions;
		Refresher.subscribeChatPanel(this);
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.setOpaque(false);
		
		loadMoreMessages = new WebLabel();
		loadMoreMessages.setMinimumSize(new Dimension(Integer.MAX_VALUE, 10));
		loadMoreMessages.setMaximumSize(new Dimension(Integer.MAX_VALUE, 20));
		loadMoreMessages.setHorizontalAlignment(SwingConstants.CENTER);
		loadMoreMessages.addMouseListener(new MouseAdapter(){
			
			public void mouseClicked(MouseEvent e){
				Conversation conv = thisUser.getConversation(localDataStorage.getCurrentConversationID());
				if(conv!=null){
					localDataStorage.setMessageLimit(localDataStorage.getMessageLimit()+numberOfMoreMessagesToLoad);
					conv.addMessages(ChatMessagesGetter.getMessages(connection, conv, localDataStorage, localDataStorage.getMessageLimit()));
					updatePanelContent();
				}
			}
		});
		this.add(loadMoreMessages);
		
		this.add(Box.createVerticalGlue()); //da se panel stisne
		
		chatMessagesPanel = new ChatMessagesPanel(thisUser.getConversation(localDataStorage.getCurrentConversationID()));
		this.add(chatMessagesPanel, 1);
		
		this.subscribeComponentsForLooksChange();
	}
	public void updatePanel(){
		JViewport parent = (JViewport) this.getParent();
		
		updatePanelContent();

		parent.remove(this);
		parent.add(this);
		parent.revalidate();
		parent.repaint();
		
		JScrollPane grandparent = (JScrollPane)parent.getParent();
		JScrollBar vertical = grandparent.getVerticalScrollBar();
		vertical.setValue(vertical.getMaximum());
	}
	private void updatePanelContent(){
		if(chatMessagesPanel!=null){
			this.remove(chatMessagesPanel);
			Conversation conversation = thisUser.getConversation(localDataStorage.getCurrentConversationID());
			chatMessagesPanel = new ChatMessagesPanel(conversation);
			this.add(chatMessagesPanel, 1);
			loadMoreMessages.setText("učitaj još poruka");
			loadMoreMessages.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		}
		this.revalidate();
		this.repaint();
	}
	
	@Override
	public void subscribeComponentsForLooksChange() {
		Refresher.subscribeToFontChange(loadMoreMessages);
		Refresher.subscribeToFontColorChange(loadMoreMessages);
		Refresher.subscribeToFontChange(conversationNameLabel);
		Refresher.subscribeToFontColorChange(conversationNameLabel);
	}
}
