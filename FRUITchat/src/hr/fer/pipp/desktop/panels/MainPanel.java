package hr.fer.pipp.desktop.panels;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.sql.Connection;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;

import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;

import hr.fer.pipp.desktop.actions.ContactsGetter;
import hr.fer.pipp.desktop.actions.ConversationsGetter;
import hr.fer.pipp.desktop.actions.StylesAndAvatarsGetter;
import hr.fer.pipp.desktop.frames.MainFrame;
import hr.fer.pipp.desktop.frames.NewConversationFrame;
import hr.fer.pipp.desktop.models.ComboItem;
import hr.fer.pipp.desktop.models.MyMenuBar;
import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.util.LocalDataStorage;
import hr.fer.pipp.desktop.util.Refresher;

/**
 * glavni panel aplikacije u kojeg smo stavili menu bar, i nekoliko ostalih panela
 */
public class MainPanel extends AbstractMyPanel{
	private Connection connection;
	private ThisUser thisUser;
	private WebScrollPane chatScroll; //stavljen je ovdje zbog boja
	private WebButton newConversationButton;
	private LocalDataStorage localDataStorage;
	
	private static final long serialVersionUID = 2949687444255909471L;
	public MainPanel(Connection connection, ThisUser thisUser, MainFrame mainFrame) {
		this.connection=connection;
		this.thisUser=thisUser;
		
		SpringLayout springLayout = new SpringLayout();
		this.setLayout(springLayout);
		
		localDataStorage = new LocalDataStorage(thisUser);
		
		StylesAndAvatarsGetter.getAllAvatars(connection, localDataStorage);
		StylesAndAvatarsGetter.getAllFontsAndColors(connection, localDataStorage);
		ContactsGetter.getContacts(connection, localDataStorage);
		ConversationsGetter.getAndStoreConversations(connection, localDataStorage);
		
		MyMenuBar menuBar = new MyMenuBar(connection, localDataStorage, mainFrame);
		springLayout.putConstraint(SpringLayout.NORTH, menuBar, 0, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.SOUTH, menuBar, 25, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, menuBar, 0, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, menuBar, 0, SpringLayout.EAST, this);
		this.add(menuBar);
		
		HeaderPanel headerPanel = new HeaderPanel(connection, localDataStorage);
		springLayout.putConstraint(SpringLayout.NORTH, headerPanel, 5, SpringLayout.SOUTH, menuBar);
		springLayout.putConstraint(SpringLayout.SOUTH, headerPanel, 45, SpringLayout.SOUTH, menuBar);
		springLayout.putConstraint(SpringLayout.WEST, headerPanel, 5, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, headerPanel, -5, SpringLayout.EAST, this);
		this.add(headerPanel);
		
		WebPanel newConvButtonAndConvListPanel = new WebPanel();
		SpringLayout layout3 = new SpringLayout();
		newConvButtonAndConvListPanel.setLayout(layout3);
		newConvButtonAndConvListPanel.setOpaque(false);
		
		newConversationButton = new WebButton("+ Novi razgovor");
		newConversationButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				NewConversationFrame newConversationFrame = new NewConversationFrame(newConversationButton, connection, localDataStorage);
				newConversationFrame.setVisible(true);
			}
		});
		layout3.putConstraint(SpringLayout.NORTH, newConversationButton, 0, SpringLayout.NORTH, newConvButtonAndConvListPanel);
		layout3.putConstraint(SpringLayout.SOUTH, newConversationButton, 30, SpringLayout.NORTH, this);
		layout3.putConstraint(SpringLayout.WEST, newConversationButton, 0, SpringLayout.WEST, newConvButtonAndConvListPanel);
		layout3.putConstraint(SpringLayout.EAST, newConversationButton, 0, SpringLayout.EAST, newConvButtonAndConvListPanel);
		newConvButtonAndConvListPanel.add(newConversationButton);
		
		ConversationsPanel conversationsPanel = new ConversationsPanel(connection, localDataStorage);
		WebScrollPane conversationsScroll = new WebScrollPane(conversationsPanel); //panel je stavljen u scrollpane da se moze scrollati
		conversationsScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		conversationsScroll.getViewport().setOpaque(false);
		conversationsScroll.setOpaque(false);
		layout3.putConstraint(SpringLayout.NORTH, conversationsScroll, 5, SpringLayout.SOUTH, newConversationButton);
		layout3.putConstraint(SpringLayout.SOUTH, conversationsScroll, 0, SpringLayout.SOUTH, newConvButtonAndConvListPanel);
		layout3.putConstraint(SpringLayout.WEST, conversationsScroll, 0, SpringLayout.WEST, newConvButtonAndConvListPanel);
		layout3.putConstraint(SpringLayout.EAST, conversationsScroll, 0, SpringLayout.EAST, newConvButtonAndConvListPanel);
		newConvButtonAndConvListPanel.add(conversationsScroll);
		
		
		springLayout.putConstraint(SpringLayout.NORTH, newConvButtonAndConvListPanel, 5, SpringLayout.SOUTH, headerPanel);
		springLayout.putConstraint(SpringLayout.SOUTH, newConvButtonAndConvListPanel, -5, SpringLayout.SOUTH, this);
		springLayout.putConstraint(SpringLayout.WEST, newConvButtonAndConvListPanel, 5, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, newConvButtonAndConvListPanel, 230, SpringLayout.WEST, this);
		this.add(newConvButtonAndConvListPanel);
		
		InputPanel inputPanel = new InputPanel(connection, localDataStorage);
		springLayout.putConstraint(SpringLayout.NORTH, inputPanel, -100, SpringLayout.SOUTH, this);
		springLayout.putConstraint(SpringLayout.SOUTH, inputPanel, -5, SpringLayout.SOUTH, this);
		springLayout.putConstraint(SpringLayout.WEST, inputPanel, 5, SpringLayout.EAST, newConvButtonAndConvListPanel);
		springLayout.putConstraint(SpringLayout.EAST, inputPanel, -5, SpringLayout.EAST, this);
		this.add(inputPanel);
		
		WebPanel chatWithTitlePanel = new WebPanel();
		SpringLayout layout2 = new SpringLayout();
		chatWithTitlePanel.setLayout(layout2);
		chatWithTitlePanel.setOpaque(false);
		
		WebLabel conversationNameLabel = new WebLabel("Trenutno niste u razgovoru");
		layout2.putConstraint(SpringLayout.NORTH, conversationNameLabel, 0, SpringLayout.NORTH, chatWithTitlePanel);
		layout2.putConstraint(SpringLayout.WEST, conversationNameLabel, 0, SpringLayout.WEST, chatWithTitlePanel);
		layout2.putConstraint(SpringLayout.EAST, conversationNameLabel, -30, SpringLayout.EAST, chatWithTitlePanel);
		layout2.putConstraint(SpringLayout.SOUTH, conversationNameLabel, 25, SpringLayout.NORTH, chatWithTitlePanel);
		
		chatWithTitlePanel.add(conversationNameLabel);
		
		WebLabel conversationOptions = new WebLabel();
		conversationOptions.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		ImageIcon gearIcon = new ImageIcon(classLoader.getResource("gear-icon.png"));
		Image newImg = gearIcon.getImage().getScaledInstance(24, 24, Image.SCALE_SMOOTH); //skaliranje
		ImageIcon scaledGearIcon = new ImageIcon(newImg);
		conversationOptions.setIcon(scaledGearIcon);
		
		
		layout2.putConstraint(SpringLayout.NORTH, conversationOptions, 0, SpringLayout.NORTH, chatWithTitlePanel);
		layout2.putConstraint(SpringLayout.EAST, conversationOptions, 0, SpringLayout.EAST, chatWithTitlePanel);
		chatWithTitlePanel.add(conversationOptions);
		
		ChatPanel chatPanel = new ChatPanel(connection, localDataStorage, conversationNameLabel, conversationOptions);
		chatScroll = new WebScrollPane(chatPanel);
		chatScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		chatScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		chatScroll.setOpaque(false);
		chatScroll.getViewport().setOpaque(true);
		chatScroll.getViewport().setBackground(new Color(255, 255, 255, 80));
		layout2.putConstraint(SpringLayout.NORTH, chatScroll, 0, SpringLayout.SOUTH, conversationNameLabel);
		layout2.putConstraint(SpringLayout.SOUTH, chatScroll, 0, SpringLayout.SOUTH, chatWithTitlePanel);
		layout2.putConstraint(SpringLayout.WEST, chatScroll, 0, SpringLayout.WEST, chatWithTitlePanel);
		layout2.putConstraint(SpringLayout.EAST, chatScroll, 0, SpringLayout.EAST, chatWithTitlePanel);
		chatScroll.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {  
		    public void adjustmentValueChanged(AdjustmentEvent e) {
		    	if(localDataStorage.ScrollBarLocked){ //spustit ga na dno ako treba jer se stalno dizo gore kad ne treba
		    		e.getAdjustable().setValue(e.getAdjustable().getMaximum());
		    	}
		    }
		});
		chatScroll.getWebVerticalScrollBar().setUnitIncrement(30);
		chatWithTitlePanel.add(chatScroll);
		
		springLayout.putConstraint(SpringLayout.NORTH, chatWithTitlePanel, 5, SpringLayout.SOUTH, headerPanel);
		springLayout.putConstraint(SpringLayout.SOUTH, chatWithTitlePanel, -5, SpringLayout.NORTH, inputPanel);
		springLayout.putConstraint(SpringLayout.WEST, chatWithTitlePanel, 5, SpringLayout.EAST, newConvButtonAndConvListPanel);
		springLayout.putConstraint(SpringLayout.EAST, chatWithTitlePanel, -5, SpringLayout.EAST, this);
		this.add(chatWithTitlePanel);
		
		this.subscribeComponentsForLooksChange();
		
	}

	@Override
	public void subscribeComponentsForLooksChange() {
		Refresher.subscribeToPrimaryColorChange(this);
		Refresher.subscribeToPrimaryColorChange(chatScroll);
		
		Refresher.subscribeToFontChange(newConversationButton);
		
		Refresher.executeChangeFont(localDataStorage.getFont(thisUser.getPreferredFontID()).getValue());
		Refresher.executeChangeColor(localDataStorage.getInterfaceColor(thisUser.getPreferredInterfaceColorID()).getValue());
		Refresher.executeChangeFontColor(localDataStorage.getFontColor(thisUser.getPreferredFontColorID()).getValue());
		
	}
}
