package hr.fer.pipp.desktop.panels;

import com.alee.laf.panel.WebPanel;

public abstract class AbstractMyPanel extends WebPanel {
	private static final long serialVersionUID = 9027400875015017104L;
	
	public abstract void subscribeComponentsForLooksChange();
}
