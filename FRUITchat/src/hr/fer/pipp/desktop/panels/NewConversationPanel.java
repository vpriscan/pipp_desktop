package hr.fer.pipp.desktop.panels;

import javax.swing.*;

import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;

import java.util.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import hr.fer.pipp.desktop.actions.CreateNewConversationAction;
import hr.fer.pipp.desktop.frames.NewConversationFrame;
import hr.fer.pipp.desktop.models.ComboItem;
import hr.fer.pipp.desktop.models.Contact;
import hr.fer.pipp.desktop.models.MyTextField;
import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.util.LocalDataStorage;

public class NewConversationPanel extends WebPanel{
	
	//unutarnja pomocna klasa
	private class ContactCheckPanel extends WebPanel{
		private static final long serialVersionUID = -3747879441595504311L;
		private WebLabel contactAvatar;
		private WebCheckBox contactCheckBox;
		private Contact contact;
		
		public ContactCheckPanel(Contact contact, LocalDataStorage localDataStorage) {
			ContactCheckPanel.this.contact = contact;
			//springLayout2 je MALI SpringLayout u kojeg ce ic WebCheckBox i avatar, i onda cemo tih malih SpringLayouta naslagat u BoxLayout jednog ispod drugog i tako dobit listu checkboxova
			SpringLayout springLayout2 = new SpringLayout(); 
			ContactCheckPanel.this.setLayout(springLayout2);
			ContactCheckPanel.this.setOpaque(false);
			
			String helpString;
			if(contact.getNickname()!=null && !contact.getNickname().equals("")){
				helpString = contact.getNickname() + " (" + contact.getUsername() + ")"; 
			}
			else{
				helpString = contact.getUsername();
			}
			
			contactCheckBox = new WebCheckBox(helpString);
			
			springLayout2.putConstraint(SpringLayout.NORTH, contactCheckBox, 1, SpringLayout.NORTH, ContactCheckPanel.this);
			springLayout2.putConstraint(SpringLayout.WEST, contactCheckBox, 1, SpringLayout.WEST, ContactCheckPanel.this);
			springLayout2.putConstraint(SpringLayout.EAST, contactCheckBox, -50/*mjesta za avatare*/, SpringLayout.EAST, ContactCheckPanel.this);
			springLayout2.putConstraint(SpringLayout.SOUTH, contactCheckBox, -1, SpringLayout.SOUTH, ContactCheckPanel.this);
			
			ContactCheckPanel.this.add(contactCheckBox);
			
			contactAvatar = new WebLabel();
			ComboItem<ImageIcon> realAvatar = localDataStorage.getAvatar(contact.getAvatarID());
			Image newImg = realAvatar.getValue().getImage().getScaledInstance(30, 30, Image.SCALE_SMOOTH); //slike unutar WebLabela
			ImageIcon scaledAvatar = new ImageIcon(newImg);
			contactAvatar.setIcon(scaledAvatar);
			
			springLayout2.putConstraint(SpringLayout.NORTH, contactAvatar, 0, SpringLayout.NORTH, contactCheckBox);
			springLayout2.putConstraint(SpringLayout.WEST, contactAvatar, 5, SpringLayout.EAST, contactCheckBox);
			springLayout2.putConstraint(SpringLayout.EAST, contactAvatar, -5, SpringLayout.EAST, ContactCheckPanel.this);
			springLayout2.putConstraint(SpringLayout.SOUTH, contactAvatar, 0, SpringLayout.SOUTH, contactCheckBox);
			
			ContactCheckPanel.this.add(contactAvatar);
			
			ContactCheckPanel.this.setFontColors(localDataStorage.getFontColor(localDataStorage.getThisUser().getPreferredFontColorID()).getValue());
			ContactCheckPanel.this.setFonts(localDataStorage.getFont(localDataStorage.getThisUser().getPreferredFontID()).getValue());
			
		}
		private void setFonts(Font value) {
			contactCheckBox.setFont(value);
		}
		private void setFontColors(Color value) {
			contactCheckBox.setForeground(value);
		}
		public boolean isChecked(){
			return contactCheckBox.isSelected();
		}
		public int contactID(){
			return contact.getUserID();
		}
	}
	//druga unutarnja anonimna klasa
	public class ContactListPanel extends WebPanel{
		private static final long serialVersionUID = 5689420652217497326L;
		private List<ContactCheckPanel> contactCheckPanelList;
		
		public ContactListPanel(Collection<Contact> collection, LocalDataStorage localDataStorage) {
			
			ContactListPanel.this.setLayout(new BoxLayout(ContactListPanel.this, BoxLayout.Y_AXIS));
			ContactListPanel.this.setBackground(Color.PINK);
			ContactListPanel.this.setOpaque(false); //TODO kad zavrsimo stavit opaque false i maknut rozu
			
			contactCheckPanelList = new ArrayList<>();
			
			for(Contact contact: collection){ //za svakog kontakta napravi njegov contactCheckPanel
				ContactCheckPanel contactCheckPanel = new ContactCheckPanel(contact, localDataStorage);
				contactCheckPanel.setPreferredSize(new Dimension(ContactListPanel.this.getWidth(), 30)); //al width ignorira jer nemamo horizontalglue pa ga ionako rastegne
				
				contactCheckPanelList.add(contactCheckPanel); //dodavanje u listu ContactCheckPanel-a
				ContactListPanel.this.add(contactCheckPanel); //dodavanje na ContactListPanel
				
			}
			ContactListPanel.this.add(Box.createVerticalGlue()); //ovo je da se ne rastezu komponente kao
		}
		
		public List<Integer> getCheckedContactIDs(){ //lista id-jeva kontakata ciji checkbox je checkiran
			List<Integer> checkedContactIDsList = new ArrayList<>();
			
			for(ContactCheckPanel contactCheckPanel: contactCheckPanelList){
				if(contactCheckPanel.isChecked()){ //za svaki contactCheckPanel gdje je checkbox checkiran
					checkedContactIDsList.add(contactCheckPanel.contactID());
				}
			}
			return checkedContactIDsList;
		}
	}
	
	private static final long serialVersionUID = -4882398798664912227L;
	private MyTextField newConversationNameField;
	private ContactListPanel contactListPanel;
	private WebButton createConversation;

	public NewConversationPanel(Connection connection, LocalDataStorage localDataStorage, NewConversationFrame frame) {
		
		SpringLayout springLayout = new SpringLayout();
		NewConversationPanel.this.setLayout(springLayout);
		
		ThisUser thisUser = localDataStorage.getThisUser(); //dobili thisusera
		
		//JTextBox za "unesi naziv novog razgovora"
		newConversationNameField = new MyTextField();
		
		String nameHint = " Ime razgovora";
		newConversationNameField.setHint(nameHint);
		
		springLayout.putConstraint(SpringLayout.NORTH , newConversationNameField, 40, SpringLayout.NORTH, NewConversationPanel.this);
		springLayout.putConstraint(SpringLayout.WEST, newConversationNameField, 40, SpringLayout.WEST, NewConversationPanel.this);
		springLayout.putConstraint(SpringLayout.SOUTH, newConversationNameField, 30, SpringLayout.NORTH, newConversationNameField);
		springLayout.putConstraint(SpringLayout.EAST, newConversationNameField, -40, SpringLayout.EAST, NewConversationPanel.this);
		
		NewConversationPanel.this.add(newConversationNameField);
		
		contactListPanel = new ContactListPanel(thisUser.getContacts(), localDataStorage);
		WebScrollPane contactListScroll = new WebScrollPane(contactListPanel);
		contactListScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		contactListScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		contactListScroll.getViewport().setOpaque(false);
		contactListScroll.setOpaque(false);
		contactListScroll.getWebVerticalScrollBar().setUnitIncrement(16);
		
		NewConversationPanel.this.add(contactListScroll);
		
		createConversation = new WebButton("Stvori razgovor");
		CreateNewConversationAction action = new CreateNewConversationAction(connection, contactListPanel, newConversationNameField, localDataStorage);
		createConversation.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				action.actionPerformed(e);
				frame.dispose();
			}	
		});
		
		
		NewConversationPanel.this.add(createConversation);
		//svejedno je li this.add() iznad ili ispod springLayout.putConstraint()
		springLayout.putConstraint(SpringLayout.NORTH , createConversation, -30, SpringLayout.SOUTH, createConversation);
		springLayout.putConstraint(SpringLayout.WEST, createConversation, 40, SpringLayout.WEST, NewConversationPanel.this);
		springLayout.putConstraint(SpringLayout.SOUTH, createConversation, -40, SpringLayout.SOUTH, NewConversationPanel.this);
		springLayout.putConstraint(SpringLayout.EAST, createConversation, -40, SpringLayout.EAST, NewConversationPanel.this);
		
		
		springLayout.putConstraint(SpringLayout.NORTH , contactListScroll, 20, SpringLayout.SOUTH, newConversationNameField);
		springLayout.putConstraint(SpringLayout.WEST, contactListScroll, 40, SpringLayout.WEST, NewConversationPanel.this);
		springLayout.putConstraint(SpringLayout.SOUTH, contactListScroll, -20, SpringLayout.NORTH, createConversation);
		springLayout.putConstraint(SpringLayout.EAST, contactListScroll, -40, SpringLayout.EAST, NewConversationPanel.this);
		
		setBackgroundColors(localDataStorage.getInterfaceColor(localDataStorage.getThisUser().getPreferredInterfaceColorID()).getValue());
		setFontColors(localDataStorage.getFontColor(localDataStorage.getThisUser().getPreferredFontColorID()).getValue());
		setFonts(localDataStorage.getFont(localDataStorage.getThisUser().getPreferredFontID()).getValue());
	}
	public void setFonts(Font font) {
		newConversationNameField.setFont(font);
		createConversation.setFont(font);
	}
	public void setBackgroundColors(Color color) {
		this.setBackground(color);
		/*ne subscribeati jer se dogodi kao memory leak jer zatvaramo prozor a stari panel ostaje u listi u refresheru zauvijek*/
		
		//Color buttonColor = new Color(color.getRed()/2, color.getGreen()/2, color.getBlue()/2);
		//createConversation.setBackground(buttonColor);
	}
	public void setFontColors(Color color) {
		//createConversation.setForeground(color);
	}

}
