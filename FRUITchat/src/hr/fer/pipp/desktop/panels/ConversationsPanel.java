package hr.fer.pipp.desktop.panels;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.util.Deque;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.alee.extended.painter.BorderPainter;
import com.alee.extended.painter.Painter;
import com.alee.laf.button.WebButton;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.menu.WebPopupMenu;
import com.alee.laf.optionpane.WebOptionPane;

import hr.fer.pipp.desktop.actions.ChatMessagesGetter;
import hr.fer.pipp.desktop.actions.ConversationsGetter;
import hr.fer.pipp.desktop.actions.ConversationsUpdater;
import hr.fer.pipp.desktop.models.Conversation;
import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.util.LocalDataStorage;
import hr.fer.pipp.desktop.util.UpdateListener;
import hr.fer.pipp.desktop.util.Refresher;

/**
 * panel s popisom razgovora
 */
public class ConversationsPanel extends AbstractMyPanel{

	private Connection connection;
	private ThisUser thisUser;
	private LocalDataStorage localDataStorage;
	
	private Deque<Conversation> conversationLIFO; //sluzi za rjesavanje duplikata i iteraciju kroz razgovore za stavit NA EKRAN
	
	
	UpdateListener updateListener;
	
	private static final long serialVersionUID = 3440286061879547860L;

	public ConversationsPanel(Connection connection, LocalDataStorage localDataStorage) { //ovo je konstruktor ConversationPanela - on se poziva kad se aplikacija pokre�e, treba STVORIT taj nas ConversationPanel
		
		this.connection=connection;
		this.thisUser=localDataStorage.getThisUser();
		this.localDataStorage=localDataStorage;
		Refresher.subscribeConversationsPanel(this);
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.setOpaque(false); //stavimo da je proziran pa ce preuzet boju od nadkomponente (mainpanel)
		
		//uzet razgovore da ih mozemo postavljat po ekranu i dodat im slusace
		Map<Integer, Conversation> conversationMap = thisUser.getConversations();
		
		/**
		 * dretva koja sinkrono oslusava asinkrone dolazece notifikacije o promjeni :D
		 */
		updateListener = new UpdateListener(connection, thisUser.getConversations().keySet(), localDataStorage); //keyset su zapravo ID-jevi svih razgovora
		updateListener.start();
		
		conversationLIFO = new LinkedBlockingDeque<>(conversationMap.values()); // iz popisa razgovora (koji su spremljeni u korisnikovoj mapi) radimo kao neki red koji ce nam samo sluziti za stavljanje na ekran

		for(Conversation conversation: conversationLIFO){
			tuneConversationButton(conversation);
			this.add(conversation);
		}
		
		// ovo obavezno na kraju konstruktora inace se sve crasha
		this.subscribeComponentsForLooksChange();
	}
	private void tuneConversationButton(Conversation conversation){ //postavljanje izgleda i akcija svakog gumba razgovora
		conversation.setConversationName(conversation.getConversationName());
		conversation.setPreferredSize(new Dimension(150, 35));
		conversation.setMaximumSize(new Dimension(Integer.MAX_VALUE, 35));
		conversation.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		conversation.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				localDataStorage.resetMessageLimit();
				conversation.addMessages(ChatMessagesGetter.getMessages(connection, conversation, localDataStorage, localDataStorage.getMessageLimit()));
				localDataStorage.setCurrentConversationID(conversation.getConversationID());
				Refresher.executeChatPanelUpdate();
				
				conversation.setTopBgColor(localDataStorage.getInterfaceColor(thisUser.getPreferredInterfaceColorID()).getValue());
				conversation.setBottomBgColor(localDataStorage.getInterfaceColor(thisUser.getPreferredInterfaceColorID()).getValue());
				conversation.setForeground(localDataStorage.getFontColor(thisUser.getPreferredFontColorID()).getValue());
				conversation.setFont(localDataStorage.getFont(thisUser.getPreferredFontID()).getValue());
			}
		});
		//za prvi put
		Color color = localDataStorage.getInterfaceColor(thisUser.getPreferredInterfaceColorID()).getValue();
		Color secondary = new Color(color.getRed()/2, color.getGreen()/2, color.getBlue()/2);
		conversation.setTopBgColor(color);
		conversation.setBottomBgColor(color);
		conversation.setForeground(localDataStorage.getFontColor(thisUser.getPreferredFontColorID()).getValue());
		conversation.setFont(localDataStorage.getFont(thisUser.getPreferredFontID()).getValue());
		conversation.setShadeColor(color);
		conversation.setInnerShadeColor(secondary);
		
		WebPopupMenu popupMenu = new WebPopupMenu();
		
		/**
		 * Stvaram preimenujItem ali ga tek addam u popupMenu u MouseListeneru (znaci tek kad se klikne desni klik na gumb)
		 * nije bilo potrebno svaki put iznova stvarat preimenujItem jer je isti za svaki gumb
		 */
		WebMenuItem preimenujItem = new WebMenuItem("Preimenuj razgovor");
		preimenujItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String newConversationName = WebOptionPane.showInputDialog("Novo ime za razgovor:");
				ConversationsUpdater.updateConversationName(connection, conversation.getConversationID(), newConversationName);
			}
		});
		
		/**
		 * Stvaram izbrisiItem ali ga tek addam u popupMenu u MouseListeneru (znaci tek kad se klikne desni klik na gumb)
		 * nije bilo potrebno svaki put iznova stvarat izbrisiItem jer je isti za svaki gumb
		 */
		WebMenuItem izbrisiItem = new WebMenuItem("Izađi iz razgovora");
		izbrisiItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				int result = WebOptionPane.showConfirmDialog(null, "Jeste li sigurni da želite napustiti ovaj razgovor? Ako nema više članova, razgovor će se obrisati. Nećete moći poništiti ovu akciju.", "OPREZ", JOptionPane.YES_NO_OPTION);
				if (result == 0){
					leaveConversation(conversation.getConversationID());
				}
				
			}
		});
		
		
		conversation.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)){
					
					popupMenu.removeAll(); /*zato jer je sad deklaracija popupmenija vani, moramo removat da nam se stalno ne addaju novi itemi na stare pri svakom kliku*/
					popupMenu.add(preimenujItem);
					popupMenu.add(izbrisiItem);
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
	}
	// stigla nova poruka (i/ili novi razgovor)
	//zvati samo iz refreshera
	public void updateConversationContent(int conversationID){ 
		if(!thisUser.getConversations().containsKey(conversationID)){ //ako nema tog razgovora u mapi znaci da je dosao novi razgovor
			Conversation newConversation = ConversationsGetter.getConversation(connection, localDataStorage, conversationID);
			if(newConversation==null) return;
			tuneConversationButton(newConversation);
			thisUser.addConversation(newConversation); //spremamo novi razgovor u korisnikovu mapu
			
			//pretplacivanje novog gumba na promjene fontova i boja
			Refresher.subscribeToFontChange(newConversation);
			Refresher.subscribeToPrimaryColorChange(newConversation);
			Refresher.subscribeToFontColorChange(newConversation);
		}
		Conversation conversation = thisUser.getConversations().get(conversationID);
		this.removeAll();//ocisti sve gumbe s ekrana da mozemo lijepo ispocetka
		conversationLIFO.remove(conversation); //ako postoji taj razgovor, makni ga da ne bude duplikata
		conversationLIFO.addFirst(conversation); //dodaj ga na pocetak
		
		for(Conversation conv: conversationLIFO){
			this.add(conv);
		}
		this.revalidate();
		this.repaint();
		if(localDataStorage.getCurrentConversationID()==conversationID){ //ako je to trenutni selektirani razgovor, prikazat osvjezenu verziju u chatpanelu
			conversation.addMessages(ChatMessagesGetter.getMessages(connection, conversation, localDataStorage, localDataStorage.getMessageLimit()));
			Refresher.executeChatPanelUpdate();
		}
		else{
			if(conversation.getBottomBgColor().equals(Color.WHITE)){
				conversation.setBottomBgColor(conversation.getTopBgColor());
				conversation.setTopBgColor(Color.WHITE);
			}else{
				conversation.setTopBgColor(conversation.getBottomBgColor());
				conversation.setBottomBgColor(Color.WHITE);
			}
			conversation.setForeground(Color.BLACK);
			Font font = localDataStorage.getFont(thisUser.getPreferredFontID()).getValue();
			conversation.setFont(new Font(font.getName(), Font.BOLD, font.getSize()));
		}
	}
	
	//zvati samo iz refreshera
	public void updateConversationInformation(Integer conversationID) {//netko drugi je izasao/usao u razgovor, ili je promijenjeno ime razgovora ili nesto slicno, ucitaj razgovor ispocetka!
		
		Conversation conversation = thisUser.getConversation(conversationID);
		Conversation withNewInfo = ConversationsGetter.getConversation(connection, localDataStorage, conversationID);
		
		conversation.addMembers(withNewInfo.getMembers());
		conversation.setConversationName(withNewInfo.getConversationName());
		
		this.removeAll();//makni sve conversatione s ekrana
		conversationLIFO.remove(conversation); //makni taj konkretni conversation iz LIFO-a
		conversationLIFO.addFirst(conversation); //dodaj ga na pocetak
		
		for(Conversation conv: conversationLIFO){
			this.add(conv); // sad sve vrati na ekran, a cinjenica da nam je struktura LIFO ce ih poredat automatski onako kako zelimo
		}
		this.revalidate();
		this.repaint();
		
		
		if(conversation.getBottomBgColor().equals(Color.WHITE) || conversation.getTopBgColor().equals(Color.WHITE)){
			conversation.setTopBgColor(Color.WHITE);
		}
		conversation.setBottomBgColor(Color.YELLOW);
		
		conversation.setForeground(Color.BLACK);
		Font font = localDataStorage.getFont(thisUser.getPreferredFontID()).getValue();
		conversation.setFont(new Font(font.getName(), Font.BOLD, font.getSize()));
		
		// iz ConversationsGettera zovi funkciju koja stvara JEDAN conversation (isti ovaj stari, samo �e mu sad podaci bit druk�iji)
		//stavi ga na ekran (na vrh), namjesti mu boju
		
		thisUser.addConversation(conversation);
		
	}
	@Override
	public void subscribeComponentsForLooksChange() {
		
		for(Conversation conversation: thisUser.getConversations().values()){
			Refresher.subscribeToFontChange(conversation);
		}
		
		//Refresher.subscribeToSecondaryColorChange(newConversationButton);
		for(Conversation conversation: thisUser.getConversations().values()){
			Refresher.subscribeToPrimaryColorChange(conversation);
		}
		
		//Refresher.subscribeToFontColorChange(newConversationButton);
		for(Conversation conversation: thisUser.getConversations().values()){
			Refresher.subscribeToFontColorChange(conversation);
		}
	}
	public void leaveConversation(Integer conversationID) {
		Conversation conversation = thisUser.getConversation(conversationID);
		
		//makni korisnika iz razgovora
		ConversationsUpdater.leaveConversation(connection, conversationID, thisUser.getUserID());
		//izbrisi razgovor s ekrana
		for(Conversation conv: conversationLIFO){ 
			ConversationsPanel.this.remove(conv); //ocisti sve gumbe s ekrana da mozemo lijepo ispocetka
		}
		conversationLIFO.remove(conversation); //ako postoji taj razgovor, makni ga da ne bude duplikata
		
		for(Conversation conv: conversationLIFO){
			ConversationsPanel.this.add(conv);
		}
		ConversationsPanel.this.revalidate();
		ConversationsPanel.this.repaint();
		
		//makni razgovor iz korisnikove mape
		thisUser.removeConversation(conversation);
		
		//unsubscribe from color change event
		Refresher.unsubscribeFromPrimaryColorChange(conversation);
	}
}
