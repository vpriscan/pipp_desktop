package hr.fer.pipp.desktop.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;
import javax.swing.SpringLayout;

import com.alee.laf.button.WebButton;

import hr.fer.pipp.desktop.actions.RegisterAction;
import hr.fer.pipp.desktop.frames.RegistrationFrame;
import hr.fer.pipp.desktop.models.MyPasswordField;
import hr.fer.pipp.desktop.models.MyTextField;

/**
 * panel za registraciju
 */
public class RegistrationPanel extends AbstractEntryPanel{
	private static final long serialVersionUID = 2654308175776140138L;
	private MyTextField usernameField;
	private MyPasswordField passwordField;
	private MyPasswordField repeatPasswordField;
	private MyTextField emailField;
	private WebButton registerButton;
	
	private RegisterAction registerAction;

	public RegistrationPanel(Color defaultBackgroundColor, Font defaultFont, RegistrationFrame registrationFrame) {
		registerAction = new RegisterAction(this, registrationFrame);
		
		SpringLayout springLayout = new SpringLayout();
		this.setLayout(springLayout);

		usernameField = new MyTextField();
		usernameField.setHint("korisničko ime");
		usernameField.setPreferredSize(new Dimension(100, 40));
		
		springLayout.putConstraint(SpringLayout.NORTH, usernameField, 40, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, usernameField, 15, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, usernameField, -15, SpringLayout.EAST, this);
		this.add(usernameField);
		
		passwordField = new MyPasswordField();
		passwordField.setHint("lozinka");
		passwordField.setPreferredSize(new Dimension(100, 40)); 
		springLayout.putConstraint(SpringLayout.NORTH , passwordField, 20, SpringLayout.SOUTH, usernameField);
		springLayout.putConstraint(SpringLayout.WEST, passwordField, 15, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, passwordField, -15, SpringLayout.EAST, this);
		this.add(passwordField);
		
		repeatPasswordField = new MyPasswordField();
		repeatPasswordField.setHint("ponovljena lozinka");
		repeatPasswordField.setPreferredSize(new Dimension(100, 40)); 
		springLayout.putConstraint(SpringLayout.NORTH , repeatPasswordField, 20, SpringLayout.SOUTH, passwordField);
		springLayout.putConstraint(SpringLayout.WEST, repeatPasswordField, 15, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, repeatPasswordField, -15, SpringLayout.EAST, this);
		this.add(repeatPasswordField);
		
		emailField = new MyTextField();
		emailField.setHint("email adresa");
		emailField.setPreferredSize(new Dimension(100, 40)); 
		springLayout.putConstraint(SpringLayout.NORTH , emailField, 20, SpringLayout.SOUTH, repeatPasswordField);
		springLayout.putConstraint(SpringLayout.WEST, emailField, 15, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, emailField, -15, SpringLayout.EAST, this);
		this.add(emailField);
		
		registerButton = new WebButton(registerAction);
		registerButton.setText("Registriraj se");
		registerButton.setPreferredSize(new Dimension(200, 30));
		springLayout.putConstraint(SpringLayout.NORTH, registerButton, 20, SpringLayout.SOUTH, emailField);
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, registerButton, 0, SpringLayout.HORIZONTAL_CENTER, this);
		this.add(registerButton);
		
		registerButton.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "registerkey");
		registerButton.getActionMap().put("registerkey", registerAction);
		
		setFonts(defaultFont);
		setInterfaceColors(defaultBackgroundColor);
		setFontColors(Color.WHITE);
		putFruitLogoOnPanel(springLayout, this);
	}
	@Override
	public void setFonts(Font font) {
		usernameField.setFont(font);
		passwordField.setFont(font);
		repeatPasswordField.setFont(font);
		emailField.setFont(font);
		registerButton.setFont(font);
	}
	@Override
	public void setInterfaceColors(Color color) {
		this.setBackground(color);
		Color secondary = new Color(color.getRed()/2, color.getGreen()/2, color.getBlue()/2);
		registerButton.setBottomBgColor(secondary);
		registerButton.setTopBgColor(color);
		
		registerButton.setBottomSelectedBgColor(registerButton.getTopBgColor());
		registerButton.setTopSelectedBgColor(registerButton.getTopBgColor());
		
	}
	@Override
	public void setFontColors(Color color) {
	}
	
	
	public MyTextField getUsernameField(){
		return usernameField;
	}
	public MyPasswordField getPasswordField(){
		return passwordField;
	}
	public MyPasswordField getRepeatPasswordField(){
		return repeatPasswordField;
	}
	public MyTextField getEmailField(){
		return emailField;
	}
}
