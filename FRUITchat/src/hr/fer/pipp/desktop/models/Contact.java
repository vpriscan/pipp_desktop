package hr.fer.pipp.desktop.models;

/**
 * kontakt, nasljedjuje usera i dodaje nickname, kojeg je "vlasnik" kontakta napravio za kontakt
 */
public class Contact extends User{

	private String nickname = null; //samo za kontakte

	public Contact(Integer userID, String username, int avatarID) {
		super(userID, username, avatarID);
	}
	
	public Contact(Integer userID, String username, int avatarID, String nickname) {
		this(userID, username, avatarID);
		this.nickname = nickname;
	}
	
	public Contact(User user) {
		this(user.getUserID(), user.getUsername(), user.getAvatarID());
	}
	
	public Contact(User user, String nickname) {
		this(user);
		this.nickname = nickname;
	}

	@Override
	public String getNickname() {
		return nickname;
	}
	
	@Override
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
}
