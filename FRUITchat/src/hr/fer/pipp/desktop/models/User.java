package hr.fer.pipp.desktop.models;

/**
 * razred koji opisuje korisnika, sadrzava samo ID, username i avatarID
 * nasljedjuju ga ThisUser i Contact, ali i ne mora se naslijediti i onda predstavlja
 * nekog znj korisnika koji prica sa drugim korisnikom a nije mu u kontaktima
 */
public class User {
	private int userID;
	private String username;
	private int avatarID;
	
	public User(Integer userID, String username, int avatarID) {
		this.userID=userID;
		this.username=username;
		this.avatarID=avatarID;
	}
	public int getUserID(){
		return userID;
	}
	public String getUsername(){
		return username;
	}
	public int getAvatarID(){
		return avatarID;
	}
	public void setAvatarID(int avatarID){
		this.avatarID = avatarID;
	}
	public void setNickname(String nickname){
		/*nista jer useri koji nisu u kontaktima nemaju nickname, metoda je ovdje samo da bi ju overrideali*/
	}
	public String getNickname(){
		return null;
	}
	@Override
	public String toString(){
		return username;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + avatarID;
		result = prime * result + userID;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (avatarID != other.avatarID)
			return false;
		if (userID != other.userID)
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
}
