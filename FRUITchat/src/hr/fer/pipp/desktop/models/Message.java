package hr.fer.pipp.desktop.models;

import java.awt.Dimension;
import java.awt.Font;
import java.util.StringTokenizer;

import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;

import hr.fer.pipp.desktop.util.LocalDataStorage;

/**
 * razred koji extenda WebLabel tako da se može dodati na ekran, 
 * prikazivat ce poruku, posiljatelja i vrijeme slanja
 * @author vedran
 */
public class Message extends WebPanel{
	
	private static final long serialVersionUID = -5683153680645135994L;
	
	private LocalDataStorage localDataStorage;
	private int senderID, messageID, senderAvatarID;
	private String senderUsername, messageTextAdjusted, timeMessageSent;
	private int strokeThickness = 7;
	private int radius = 10;
	private int arrowSize = 12;
	private int padding = strokeThickness / 2;
	private Color boja;
	  
	private boolean senderIsThisUser;
	JTextArea messageTextLabel;
	    
	public Message(LocalDataStorage localDataStorage, int senderID, String senderUsername, int senderAvatarID,
			int messageID, String messageText, String timeMessageSent, boolean senderIsThisUser) {
		
		this.localDataStorage = localDataStorage;
		this.senderID = senderID;
		this.senderUsername = senderUsername;
		this.senderAvatarID = senderAvatarID;
		this.messageID = messageID;
		this.messageTextAdjusted = addLineBreaks(messageText, 50);
		this.timeMessageSent = timeMessageSent;
		this.senderIsThisUser = senderIsThisUser;
		
		SpringLayout springLayout = new SpringLayout();
		this.setLayout(springLayout);
		
		if (senderIsThisUser) {
			 boja = new Color (0.5f, 0.8f, 1f);
		}
		else {
			 boja = new Color (0.6f, 0.6f, 1f);
		}
		
		int conversationMemberIconSize = localDataStorage.getConversationMemberIconSize();
		
		WebLabel senderAvatarLabel = new WebLabel(localDataStorage.getScaledAvatar(senderAvatarID).getValue());
		springLayout.putConstraint(SpringLayout.NORTH, senderAvatarLabel, 20, SpringLayout.NORTH, this);
		if(senderIsThisUser){
			springLayout.putConstraint(SpringLayout.EAST, senderAvatarLabel, 0, SpringLayout.EAST, this);
			springLayout.putConstraint(SpringLayout.WEST, senderAvatarLabel, -conversationMemberIconSize, SpringLayout.EAST, senderAvatarLabel); //10
		}
		else{
			springLayout.putConstraint(SpringLayout.WEST, senderAvatarLabel, 0, SpringLayout.WEST, this);
			springLayout.putConstraint(SpringLayout.EAST, senderAvatarLabel, conversationMemberIconSize, SpringLayout.WEST, senderAvatarLabel); //10
		}
		springLayout.putConstraint(SpringLayout.SOUTH, senderAvatarLabel, conversationMemberIconSize, SpringLayout.NORTH, senderAvatarLabel);
		this.add(senderAvatarLabel);
		
		String senderName;
		ThisUser thisUser = localDataStorage.getThisUser();
		if(thisUser.hasContact(senderID) && thisUser.getContact(senderID).getNickname()!=null 
				&& !thisUser.getContact(senderID).getNickname().equals("")){
			senderName = thisUser.getContact(senderID).getNickname()+" ("+this.senderUsername+")";
		}else{
			senderName = this.senderUsername;
		}
		
		Font preferredFont = localDataStorage.getPreferredFont();
		WebLabel senderNameLabel = new WebLabel(senderName);
		
		senderNameLabel.setFontName(preferredFont.getName());
		senderNameLabel.setFontStyle(Font.BOLD);
		senderNameLabel.setFontSize(preferredFont.getSize()-2);
		
		springLayout.putConstraint(SpringLayout.NORTH, senderNameLabel, 0, SpringLayout.NORTH, this);
		if(senderIsThisUser){
			senderNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
			springLayout.putConstraint(SpringLayout.EAST, senderNameLabel, 0, SpringLayout.WEST, senderAvatarLabel);
			springLayout.putConstraint(SpringLayout.WEST, senderNameLabel, 5, SpringLayout.WEST, this);
		}
		else{
			senderNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			springLayout.putConstraint(SpringLayout.WEST, senderNameLabel, 0, SpringLayout.EAST, senderAvatarLabel);
			springLayout.putConstraint(SpringLayout.EAST, senderNameLabel, -5, SpringLayout.EAST, this);
		}
		
		this.add(senderNameLabel);
		
		messageTextLabel = new JTextArea(this.messageTextAdjusted);
		messageTextLabel.setFont(new Font(preferredFont.getName(), Font.PLAIN, preferredFont.getSize()-2));
		messageTextLabel.setEditable(false);
		messageTextLabel.setOpaque(false); //ako je proziran ne moramo ga farbati
		springLayout.putConstraint(SpringLayout.NORTH, messageTextLabel, 0, SpringLayout.SOUTH, senderNameLabel);
		if(senderIsThisUser){
			springLayout.putConstraint(SpringLayout.EAST, messageTextLabel, 0, SpringLayout.WEST, senderAvatarLabel);
			springLayout.putConstraint(SpringLayout.WEST, messageTextLabel, 5, SpringLayout.WEST, this);
		}else{
			springLayout.putConstraint(SpringLayout.WEST, messageTextLabel, 5, SpringLayout.EAST, senderAvatarLabel);
			springLayout.putConstraint(SpringLayout.EAST, messageTextLabel, 0, SpringLayout.EAST, this);
		}
		this.add(messageTextLabel);
		
		WebLabel timeMessageSentLabel = new WebLabel(this.timeMessageSent);
		timeMessageSentLabel.setFontName(preferredFont.getName());
		timeMessageSentLabel.setFontStyle(Font.BOLD);
		timeMessageSentLabel.setFontSize(preferredFont.getSize()-3);
		
		springLayout.putConstraint(SpringLayout.NORTH, timeMessageSentLabel, 0, SpringLayout.SOUTH, messageTextLabel);
		if(senderIsThisUser){
			timeMessageSentLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			springLayout.putConstraint(SpringLayout.EAST, timeMessageSentLabel, -5, SpringLayout.WEST, senderAvatarLabel);
			springLayout.putConstraint(SpringLayout.WEST, timeMessageSentLabel, 0, SpringLayout.WEST, this);
		}else{
			timeMessageSentLabel.setHorizontalAlignment(SwingConstants.LEFT);
			springLayout.putConstraint(SpringLayout.WEST, timeMessageSentLabel, 5, SpringLayout.EAST, senderAvatarLabel);
			springLayout.putConstraint(SpringLayout.EAST, timeMessageSentLabel, 0, SpringLayout.EAST, this);
		}
		this.add(timeMessageSentLabel);
		
		int preferredWidth = (this.getPreferredSize().width);
		int preferredHeight = senderNameLabel.getPreferredSize().height
				+messageTextLabel.getPreferredSize().height
				+timeMessageSentLabel.getPreferredSize().height;
		Dimension size = new Dimension(preferredWidth, preferredHeight);
		this.setPreferredSize(size);
	}
	public String addLineBreaks(String input, int maxLineLength) { //rezanje poruke
	    StringTokenizer tok = new StringTokenizer(input, " ");
	    StringBuilder output = new StringBuilder(input.length());
	    int lineLen = 0;
	    while (tok.hasMoreTokens()) {
	        String word = tok.nextToken()+" ";
	        if(word.length()>maxLineLength){
	        	int numberOfNewRows = word.length()/maxLineLength;
	        	if(numberOfNewRows%maxLineLength!=0) numberOfNewRows++;
	        	
	        	String[] wordParts = splitN(word, numberOfNewRows);
	        	for(int i=0;i<numberOfNewRows;i++){
	        		if(i!=0) {
	        			output.append("\n");
	        			lineLen = 0;
	        		}
	        		output.append(wordParts[i]);
	        		lineLen += wordParts[i].length();
	        	}
	        	continue;
	        }
	        if (lineLen + word.length() > maxLineLength) {
	            output.append("\n");
	            lineLen = 0;
	        }
	        output.append(word);
	        lineLen += word.length();
	    }
	    return output.toString();
	}
	static String[] splitN(String s, final int N) { //rezanje ogromne rijeci
	    final int base = s.length() / N;
	    final int remainder = s.length() % N;

	    String[] parts = new String[N];
	    for (int i = 0; i < N; i++) {
	        int length = base + (i < remainder ? 1 : 0);
	        parts[i] = s.substring(0, length);
	        s = s.substring(length);
	    }
	    return parts;
	}
	//ako ce nam ovi getteri trebat ok, ako ne ne
	public int getSenderID(){
		return senderID;
	}
	public String getSenderUsername(){
		return senderUsername;
	}
	public int getSenderAvatarID(){
		return senderAvatarID;
	}
	public int getMessageID(){
		return messageID;
	}
	public String getMessageText(){
		return messageTextAdjusted;
	}
	public String getTimeMessageSent(){
		return timeMessageSent;
	}
	@Override
	public String toString() {
		return messageTextAdjusted;
	}
	@Override
	protected void paintComponent(final Graphics g) {
		final Graphics2D g2d = (Graphics2D) g;
		int colorID = localDataStorage.getThisUser().getPreferredInterfaceColorID();
		Color color = localDataStorage.getInterfaceColor(colorID).getValue();
		/*Color thisUserColor = new Color(color.getRed()*2/3, color.getGreen()*2/3, color.getBlue()*2/3);
		Color otherUserColor = new Color(color.getRed()*3/4, color.getGreen()*3/4, color.getBlue()*3/4);
		if (senderIsThisUser) {
			g2d.setColor(thisUserColor);
		}
		else {
			g2d.setColor(otherUserColor);
		}*/
		g2d.setColor(boja);
		int bottomLineY = getHeight() - strokeThickness;
		int width = getWidth() - arrowSize - (strokeThickness * 2);
		RoundRectangle2D.Double rect;
		if(senderIsThisUser){
			g2d.fillRect(padding, padding, width, bottomLineY);
			rect = new RoundRectangle2D.Double(padding, padding, width, bottomLineY,  radius, radius);
		}else{
			g2d.fillRect(padding+20, padding, width, bottomLineY);
			rect = new RoundRectangle2D.Double(padding+20, padding, width, bottomLineY,  radius, radius);
		}
		Polygon arrow = new Polygon();
		if(senderIsThisUser){
			arrow.addPoint(width, 8);
			arrow.addPoint(width + arrowSize-1, 10);
			arrow.addPoint(width, 12);
		}else{
			arrow.addPoint(strokeThickness+5 + arrowSize, 8);
			arrow.addPoint(strokeThickness+8, 10);
			arrow.addPoint(strokeThickness+5 + arrowSize, 12);
		}
		Area area = new Area(rect);
		area.add(new Area(arrow));
		g2d.setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
		g2d.setStroke(new BasicStroke(7));
		g2d.draw(area);
	}
}
