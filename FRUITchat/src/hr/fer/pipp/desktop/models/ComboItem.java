package hr.fer.pipp.desktop.models;

/**
 * 
 * koristimo ga tako da nam predstavlja avatar, font, i boju
 * obicni avatar/boja/font plus dodatne varijable za ime/label i id
 * @param <T>
 */
public class ComboItem<T>{
	private T value;
	private String label;
	private int id;
	public ComboItem(T value, String label, int id){
		this.value = value;
		this.label = label;
		this.id = id;
	}
	
	public T getValue(){
		return value;
	}
	public String getLabel(){
		return label;
	}
	public int getID(){
		return id;
	}
	@Override
	public String toString(){
		return label;
	}
}
