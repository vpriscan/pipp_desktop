package hr.fer.pipp.desktop.models;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.sql.Connection;

import javax.swing.ImageIcon;

import com.alee.laf.menu.WebMenu;
import com.alee.laf.menu.WebMenuBar;
import com.alee.laf.menu.WebMenuItem;

import hr.fer.pipp.desktop.actions.StyleAndAvatarStorer;
import hr.fer.pipp.desktop.frames.AvatarChooserFrame;
import hr.fer.pipp.desktop.frames.LoginFrame;
import hr.fer.pipp.desktop.frames.MainFrame;
import hr.fer.pipp.desktop.util.LocalDataStorage;
import hr.fer.pipp.desktop.util.Refresher;

/**
 * nalazi se na vrhu main panela, u njemu su logout, postavke izgleda i promjena avatara
 */
public class MyMenuBar extends WebMenuBar{
	private WebMenu menu, fonts, fontColors, interfaceColors;
	private WebMenuItem menuItem;

	private static final long serialVersionUID = 1971734298013447041L;

	public MyMenuBar(Connection connection, LocalDataStorage localDataStorage, MainFrame mainFrame) {
		
		ThisUser thisUser = localDataStorage.getThisUser();
		
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		menu = new WebMenu("Datoteka", new ImageIcon(classLoader.getResource("file.png")));
		this.add(menu);
		
		menuItem = new WebMenuItem("Odjava", new ImageIcon(classLoader.getResource("file_exit.png")));
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mainFrame.dispose();
				Refresher.clearAll();
				LoginFrame loginFrame = new LoginFrame();
				loginFrame.setVisible(true);
			}
		});
		menu.add(menuItem);
		
		menu = new WebMenu("Uređivanje", new ImageIcon(classLoader.getResource("edit.png")));
		this.add(menu);
		
		fonts = new WebMenu("Fontovi", new ImageIcon(classLoader.getResource("font.png")));
		for(ComboItem<Font> font: localDataStorage.getAllFonts()){
			menuItem = new WebMenuItem(font.getLabel());
			menuItem.setFontName(font.getValue().getFontName());
			menuItem.setFontStyle(font.getValue().getStyle());
			menuItem.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					thisUser.setPreferredFontID(font.getID());
					StyleAndAvatarStorer.storePreferredFont(connection, thisUser, font);
					Refresher.executeChangeFont(font.getValue());
				}
			});
			fonts.add(menuItem);
		}
		menu.add(fonts);
		
		fontColors = new WebMenu("Boje fontova", new ImageIcon(classLoader.getResource("fontcolor.png")));
		for(ComboItem<Color> fontColor: localDataStorage.getAllFontColors()){
			BufferedImage bi = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
			Graphics2D gr = bi.createGraphics();
			gr.setPaint(fontColor.getValue());
			gr.fillRect(0, 0, 16, 16);
			gr.drawRect(0, 0, 16, 16);
			menuItem = new WebMenuItem(fontColor.getLabel(), new ImageIcon(bi));
			menuItem.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					thisUser.setPreferredFontColorID(fontColor.getID());
					StyleAndAvatarStorer.storePreferredFontColor(connection, thisUser, fontColor);
					Refresher.executeChangeFontColor(fontColor.getValue());
					Refresher.executeChangeLogo((ImageIcon) localDataStorage.getLogo());
				}
			});
			fontColors.add(menuItem);
		}
		menu.add(fontColors);
		

		interfaceColors = new WebMenu("Boje sučelja", new ImageIcon(classLoader.getResource("interfacecolor.png")));
		for(ComboItem<Color> interfaceColor: localDataStorage.getAllInterfaceColors()){
			BufferedImage bi = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
			Graphics2D gr = bi.createGraphics();
			gr.setPaint(interfaceColor.getValue());
			gr.fillRect(0, 0, 16, 16);
			gr.drawRect(0, 0, 16, 16);
			menuItem = new WebMenuItem(interfaceColor.getLabel(), new ImageIcon(bi));
			menuItem.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					thisUser.setPreferredInterfaceColorID(interfaceColor.getID());
					StyleAndAvatarStorer.storePreferredInterfaceColor(connection, thisUser, interfaceColor);
					Refresher.executeChangeColor(interfaceColor.getValue());
				}
			});
			interfaceColors.add(menuItem);
		}
		menu.add(interfaceColors);
		
		menuItem = new WebMenuItem("Promjena avatara", new ImageIcon(classLoader.getResource("avatar.png")));
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AvatarChooserFrame avatarChooserFrame = new AvatarChooserFrame(menu, connection, localDataStorage);
				avatarChooserFrame.setVisible(true);
			}
		});
		
		menu.add(menuItem);
	}
}
