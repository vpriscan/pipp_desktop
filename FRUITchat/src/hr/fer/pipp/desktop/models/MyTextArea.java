package hr.fer.pipp.desktop.models;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.alee.laf.text.WebTextArea;

public class MyTextArea extends WebTextArea{
	private static final long serialVersionUID = 843876947349100189L;
	private static Color textColor = new Color(0, 0, 1); // u slucaju da u buducnosti budemo trebali mijenjati sheme boja
	private static Color hintColor = new Color(128, 128, 129);
	
	private boolean isHint = false;
	
	public boolean isHint() {
		return isHint;
	}
	
	public boolean isEmpty(){
		if(getText()==null || getText().equals("")){
			return true;
		} else return false;
	}
	
	public void setHint(String hint){
		
		this.setText(hint);
		this.setForeground(hintColor);
		this.isHint = true;
		
		this.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {	
				if(MyTextArea.this.isHint){
					MyTextArea.this.isHint = false;
					MyTextArea.this.setText("");
					MyTextArea.this.setForeground(textColor);
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
			}
		});
		
		this.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				if(MyTextArea.this.isEmpty()){
					MyTextArea.this.setText(hint);
					MyTextArea.this.setForeground(hintColor);
					MyTextArea.this.isHint = true;
				}
				
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				if(MyTextArea.this.isEmpty()){
					MyTextArea.this.setText(hint);
					MyTextArea.this.setForeground(hintColor);
					MyTextArea.this.isHint = true;
				}
				if(MyTextArea.this.isHint){
					MyTextArea.this.setCaretPosition(0);
				}
			}
		});
	}
}

