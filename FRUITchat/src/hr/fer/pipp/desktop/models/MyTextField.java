package hr.fer.pipp.desktop.models;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.alee.laf.text.WebTextField;

public class MyTextField extends WebTextField{
	private static final long serialVersionUID = 843876947349100189L;
	private static Color textColor = new Color(0, 0, 1); // u slucaju da u buducnosti budemo trebali mijenjati sheme boja
	private static Color hintColor = new Color(128, 128, 129);
	
	private boolean isHint = false;
	
	public boolean isHint() {
		return isHint;
	}
	
	public boolean isEmpty(){
		if(getText()==null || getText().equals("")){
			return true;
		} else return false;
	}
	
	public void setHint(String hint){
		
		this.setText(hint);
		this.setForeground(hintColor);
		this.isHint = true;
		
		this.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {	
				if(MyTextField.this.isHint){
					MyTextField.this.isHint = false;
					MyTextField.this.setText("");
					MyTextField.this.setForeground(textColor);
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
			}
		});
		
		this.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				if(MyTextField.this.isEmpty()){
					MyTextField.this.setText(hint);
					MyTextField.this.setForeground(hintColor);
					MyTextField.this.isHint = true;
				}
				
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				if(MyTextField.this.isEmpty()){
					MyTextField.this.setText(hint);
					MyTextField.this.setForeground(hintColor);
					MyTextField.this.isHint = true;
				}
				if(MyTextField.this.isHint){
					MyTextField.this.setCaretPosition(0);
				}
			}
		});
	}
}
