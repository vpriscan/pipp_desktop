package hr.fer.pipp.desktop.models;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * ovo je onaj tko upravo koristi aplikaciju, nasljedjuje obicnog usera
 * i dodaje svoj email i svoje odabrane stilove 
 * (koje smo pri loginu izvukli iz baze, kako bi se odmah mogla promijeniti tema na ekranu)
 */
public class ThisUser extends User{
	private String email;
	private int preferredFontColorID;
	private int preferredFontID;
	private int preferredInterfaceColorID;
	private Map<Integer, Contact> contactMap;
	private Map<Integer, Conversation> conversationMap;
	
	public ThisUser(Integer userID, String username, String email, 
			int preferredFontColorID, int preferredFontID, 
			int preferredInterfaceColorID, int avatarID) {
		
		super(userID, username, avatarID);
		this.email=email;
		this.preferredFontColorID=preferredFontColorID;
		this.preferredFontID=preferredFontID;
		this.preferredInterfaceColorID=preferredInterfaceColorID;
	}
	
	public String getEmail(){
		return email;
	}
	public int getPreferredFontColorID(){
		return preferredFontColorID;
	}
	public int getPreferredFontID(){
		return preferredFontID;
	}
	public int getPreferredInterfaceColorID(){
		return preferredInterfaceColorID;
	}
	public Collection<Contact> getContacts(){
		return contactMap.values();
	}
	public Contact getContact(int contactID){
		return contactMap.get(contactID);
	}
	public void addContactMap(Map<Integer, Contact> contactMap){
		this.contactMap = contactMap;
	}
	public void addContact(Contact contact){
		if(contactMap==null){
			contactMap = new HashMap<>();
		}
		contactMap.put(contact.getUserID(), contact);
	}
	public void removeContact(int contactID){
		contactMap.remove(contactID);
	}
	public boolean hasContact(int contactID){
		return contactMap.containsKey(contactID);
	}
	
	public void setEmail(String email){
		this.email = email; 
	}
	public void setPreferredFontID(int fontID){
		this.preferredFontID = fontID;
	}
	public void setPreferredFontColorID(int colorID){
		this.preferredFontColorID = colorID;
	}
	public void setPreferredInterfaceColorID(int colorID){
		this.preferredInterfaceColorID = colorID;
	}
	
	public void addConversations(Map<Integer, Conversation> conversationMap){
		this.conversationMap = conversationMap;
	}
	public void addConversation(Conversation conversation){
		conversationMap.put(conversation.getConversationID(), conversation);
	}
	
	public void removeConversation(Conversation conversation){
		conversationMap.remove(conversation.getConversationID());
	}

	public Map<Integer, Conversation> getConversations() {
		return conversationMap;
	}
	public Conversation getConversation(int conversationID){
		return conversationMap.get(conversationID);
	}
}
