package hr.fer.pipp.desktop.models;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.alee.laf.text.WebPasswordField;

public class MyPasswordField extends WebPasswordField{
	private static final long serialVersionUID = -8893289674800717746L;
	private static Color textColor = new Color(0, 0, 1); // u slucaju da u buducnosti budemo trebali mijenjati sheme boja
	private static Color hintColor = new Color(128, 128, 129);
	
	private boolean isHint = false;
	
	public boolean isHint(){
		return isHint;
	}
	
	public boolean isEmpty(){
		return getPassword().length==0;
	}
	
	public void setHint(String hint){
		
		this.setText(hint);
		this.setForeground(hintColor);
		this.setEchoChar((char)0);
		this.isHint = true;
		
		this.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {	
				if(MyPasswordField.this.isHint){
					MyPasswordField.this.isHint=false;
					MyPasswordField.this.setText("");
					MyPasswordField.this.setForeground(textColor);
					MyPasswordField.this.setEchoChar((char)8226); //ovaj character se zove Bullet
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
			}
		});
		
		this.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				if(MyPasswordField.this.isEmpty()){
					MyPasswordField.this.setText(hint);
					MyPasswordField.this.setForeground(hintColor);
					MyPasswordField.this.setEchoChar((char)0);
					MyPasswordField.this.isHint=true;
				}
				
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				if(MyPasswordField.this.isEmpty()){
					MyPasswordField.this.setText(hint);
					MyPasswordField.this.setForeground(hintColor);
					MyPasswordField.this.setEchoChar((char)0);
					MyPasswordField.this.isHint=true;
				}
				if(MyPasswordField.this.isHint){
					MyPasswordField.this.setCaretPosition(0);
				}
			}
		});
	}	
}
