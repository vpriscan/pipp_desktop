package hr.fer.pipp.desktop.models;

import java.util.LinkedList;
import java.util.List;

import com.alee.laf.button.WebButton;



/**
 * razred razgovor, u njega se spremaju podaci o nekom razgovoru 
 */
public class Conversation extends WebButton{

	private static final long serialVersionUID = -3221330496494566479L;
	private int conversationID;
	private String conversationName;
	private String timeConversationStarted;
	private List<User> conversationMembers;
	private List<Message> messageList;
	private int unread = 0;
	
	public Conversation(int conversationID, String conversationName, String timeConversationStarted) {
		this.conversationID = conversationID;
		this.conversationName = conversationName;
		this.timeConversationStarted = timeConversationStarted;
		conversationMembers = new LinkedList<>();
	}
	public void addMember(User member){
		conversationMembers.add(member);
	}
	public int getConversationID(){
		return conversationID;
	}
	public void setConversationName(String conversationName){
		this.conversationName = conversationName;
	}
	public String getConversationName(){
		return conversationName;
	}
	public List<User> getMembers(){//vracamo cijelu listu, a ako netko treba individualne moze preko localDataStorage uzet memberove podatke
		return conversationMembers;
	}
	public void addMembers(List<User> members){
		this.conversationMembers=members;
	}
	public List<Message> getMessages() {
		unread = 0;
		return messageList;
	}
	public void addMessages(List<Message> messageList){
		this.messageList=messageList;
	}
	public void increaseUnreadNumber(){
		unread++;
	}
	public int getUnreadNumber(){
		return unread;
	}
	public String getTimeConversationStarted(){
		return timeConversationStarted;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + conversationID;
		result = prime * result + ((timeConversationStarted == null) ? 0 : timeConversationStarted.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conversation other = (Conversation) obj;
		if (conversationID != other.conversationID)
			return false;
		if (timeConversationStarted == null) {
			if (other.timeConversationStarted != null)
				return false;
		} else if (!timeConversationStarted.equals(other.timeConversationStarted))
			return false;
		return true;
	}
	@Override
	public String getText() {
		return conversationName;
	}
	@Override
	public void setText(String text) {
		conversationName = text;
	}
}
