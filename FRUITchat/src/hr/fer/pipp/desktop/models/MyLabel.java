package hr.fer.pipp.desktop.models;

import javax.swing.ImageIcon;
import com.alee.laf.label.WebLabel;

public class MyLabel extends WebLabel{

	private static final long serialVersionUID = 629193665815264409L;
	private int id;
	
	public MyLabel(ImageIcon avatar, int avatarID) {
		super(avatar);
		this.id=avatarID;
	}
	public MyLabel(ComboItem<ImageIcon> avatar) {
		this(avatar.getValue(), avatar.getID());
	}
	public int getID(){
		return id;
	}
	public void setID(int id){
		this.id=id;
	}
}
