package hr.fer.pipp.desktop.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.text.JTextComponent;

import com.alee.laf.button.WebButton;

import hr.fer.pipp.desktop.models.ComboItem;
import hr.fer.pipp.desktop.models.MyLabel;
import hr.fer.pipp.desktop.panels.ChatPanel;
import hr.fer.pipp.desktop.panels.ConversationsPanel;


/**
 * ovo je razred koji pamti koji textfieldovi/WebLabeli/itd su se pretplatili na odredjeni dogadjaj
 * recimo da se jedan WebLabel pretplatio na promjenu avatara, i onda kad zelimo promijeniti avatar
 * pozovemo executeChangeAvatarIcons() i on obavi promjenu za sve komponente koje su pretplacene na to
 */
public class Refresher {
	
	/**
	 * ako se zelimo odlogirati, da nam ne ostanu komponente od prosle sesije u listama
	 */
	public static void clearAll(){ //pozvati na logoutu
		listForAvatarChange.clear();
		listForContactNicknameChange.clear();
		listForFontChange.clear();
		listForFontColorChange.clear();
		listForPrimaryColorChange.clear();
		listForSecondaryColorChange.clear();
		chatPanel = null;
		conversationsPanel = null;
	}
	
	private static List<MyLabel>  listForAvatarChange= new LinkedList<>();
	private static List<JTextComponent> listForContactNicknameChange = new LinkedList<>(); 
	private static List<JComponent> listForFontChange = new LinkedList<>();
	private static List<JComponent> listForFontColorChange = new LinkedList<>();
	private static List<JComponent> listForPrimaryColorChange = new LinkedList<>();
	private static List<JComponent> listForSecondaryColorChange = new LinkedList<>();

	private static ChatPanel chatPanel;
	private static ConversationsPanel conversationsPanel;
	private static JLabel logoLabel;
	
	public static void subscribeToAvatarChange(MyLabel label){
		listForAvatarChange.add(label);
	}
	public static void subscribeToContactNicknameChange(JTextComponent comp){
		listForContactNicknameChange.add(comp);
	}
	public static void subscribeToFontChange(JComponent comp){
		listForFontChange.add(comp);
	}
	public static void subscribeToFontColorChange(JComponent comp){
		listForFontColorChange.add(comp);
	}
	public static void subscribeToPrimaryColorChange(JComponent comp){
		listForPrimaryColorChange.add(comp);
	}
	public static void unsubscribeFromPrimaryColorChange(JComponent comp){//kod uklanjanja conversation buttona
		listForPrimaryColorChange.remove(comp);
	}
	public static void subscribeToSecondaryColorChange(JComponent comp){
		listForSecondaryColorChange.add(comp);
	}
	public static void executeChangeAvatarIcons(int oldID, ComboItem<ImageIcon> newAvatar){
		for(MyLabel label: listForAvatarChange){
			if(label.getID()==oldID){
				int width = label.getWidth();
				int height = label.getHeight();
				
				Image newImg = newAvatar.getValue().getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH);
				ImageIcon scaledAvatar = new ImageIcon(newImg);
				
				label.setIcon(scaledAvatar);
				label.setID(newAvatar.getID());
				
				label.repaint();
			}
		}
	}
	public static void executeChangeNicknameFields(String oldNickname, String newNickname){
		for(JTextComponent comp: listForContactNicknameChange){
			if(comp.getText().equals(oldNickname))
				comp.setText(newNickname);
		}
	}
	public static void executeChangeFont(Font font){
		for(JComponent comp: listForFontChange){
			comp.setFont(font);
		}
	}
	public static void executeChangeFontColor(Color color){
		for(JComponent comp: listForFontColorChange){
			comp.setForeground(color);
		}
	}
	public static void executeChangeColor(Color color){
		Color secondary = new Color(color.getRed()/2, color.getGreen()/2, color.getBlue()/2);
		for(JComponent comp: listForPrimaryColorChange){
			if(comp instanceof WebButton){
				WebButton webButton = (WebButton) comp;
				webButton.setBottomBgColor(color);
				webButton.setTopBgColor(color);/*
				webButton.setBottomSelectedBgColor(color);
				webButton.setTopSelectedBgColor(color);*/
				webButton.setShadeColor(Color.LIGHT_GRAY);
				webButton.setInnerShadeColor(secondary);
			}else{
				comp.setBackground(color);
			}
		}
		for(JComponent comp: listForSecondaryColorChange){
			if(comp instanceof WebButton){
				WebButton webButton = (WebButton) comp;
				webButton.setBottomBgColor(secondary);
				webButton.setTopBgColor(color);/*
				webButton.setBottomSelectedBgColor(secondary);
				webButton.setTopSelectedBgColor(secondary);*/
			}else{
				comp.setBackground(secondary);
			}
		}
	}
	public static void subscribeChatPanel(ChatPanel panel){
		chatPanel = panel;
	}
	public static void subscribeConversationsPanel(ConversationsPanel panel){
		conversationsPanel = panel;
	}
	public static void leaveConversation(Integer conversationID){
		conversationsPanel.leaveConversation(conversationID);
	}
	public static void executeChatPanelUpdate(){
		chatPanel.updatePanel();
	}
	public static void executeUpdateConversationContent(int conversationID){
		conversationsPanel.updateConversationContent(conversationID);
	}
	public static void executeUpdateConversationInformation(int conversationID){
		conversationsPanel.updateConversationInformation(conversationID);
	}
	public static void subscribeLogoLabel(JLabel logo) {
		Refresher.logoLabel = logo;
	}
	
	public static void executeChangeLogo(ImageIcon icon){
		logoLabel.setIcon(icon);
	}
}
