package hr.fer.pipp.desktop.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.postgresql.PGConnection;
import org.postgresql.PGNotification;

/**
 * slusac promjene u bazi podataka
 * u novoj dretvi
 * slusa postgresql notifikejsne
 * u bazi su napravljeni triggeri koji automatski NOTIFY-aju tocno ove kanale koji se ovdje LISTEN-aju
 * konstruktor za ovo treba pozivati pri svakoj novoj konekciji na bazu
 * trenutno slusamo na nove poruke i na nove razgovore
 * TODO izlazak covjeka iz razgovora
 * @author vedran
 */
public class UpdateListener extends Thread{
	private int sleepTimeInMiliseconds = 2000;
	private Connection con;
	private PGConnection pgcon;
	private String newMessageOrConversationStringPart1 = "obavijesti_", 
			newMessageOrConversationStringPart2 = "_o_novoj_poruci_ili_razgovoru";
	private String changesInConversationStringPart1 = "obavijesti_", 
			changesInConversationStringPart2 = "_o_promjenama_u_razgovoru"; //pokriva ulaske/izlaske iz razgovora drugih korisnika i promjenu naziva razgovora
	
	/*izmijenjeni su kanali i vise ne treba zasebno svaki razgovor osluskivati, sve je na jednom kanalu*/
	
	public UpdateListener(Connection con, Collection<Integer> conversationIDlist, LocalDataStorage localDataStorage){
		this.con = con;
		this.pgcon = (PGConnection)con;
		
		try{
			Statement st = con.createStatement();
			st.execute("LISTEN obavijesti_"+localDataStorage.getThisUser().getUserID()+"_o_novoj_poruci_ili_razgovoru;"
					+ " LISTEN obavijesti_"+localDataStorage.getThisUser().getUserID()+"_o_promjenama_u_razgovoru;");
			st.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		Statement st = null;
		try {
			st = con.createStatement();
			while (true) {
					st.execute("SELECT 1"); // posalji lagani query da kontaktiras bazu
					PGNotification notifications[] = pgcon.getNotifications(); // uzmi notifikacije koje baza vrati
					if (notifications != null) {
						for(int i=0; i<notifications.length; i++){
							//System.out.println(notifications[i].getName()+", "+notifications[i].getParameter());
							if(notifications[i].getName().startsWith(newMessageOrConversationStringPart1) && notifications[i].getName().endsWith(newMessageOrConversationStringPart2)){ //samo ako je dosla notifikacija za nove poruke / nove razgovore
								int conversationID = Integer.valueOf(notifications[i].getParameter());
								Refresher.executeUpdateConversationContent(conversationID);
							}
							else if(notifications[i].getName().startsWith(changesInConversationStringPart1) && notifications[i].getName().endsWith(changesInConversationStringPart2)){ //samo ako je dosla notifikacija za neku promjenu u razgovoru (izlaza/ulazak clanova, rename)
								int conversationID = Integer.valueOf(notifications[i].getParameter()); //uzmi payload, u ovom slucaju conversationID
								Refresher.executeUpdateConversationInformation(conversationID);
							}
						}
					}
					Thread.sleep(sleepTimeInMiliseconds);
			}
		} catch (SQLException ex1) {//ovo ce se dogodit ako se prekine konekcija ili se server ugasi
			ex1.printStackTrace(); 
		} catch (InterruptedException ex2) { //ovo ce se ponekad dogodit kod gasenja programa
			
		} finally{
			try {
				if(st!=null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
