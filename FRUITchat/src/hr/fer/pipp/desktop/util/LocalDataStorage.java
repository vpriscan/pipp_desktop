package hr.fer.pipp.desktop.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import hr.fer.pipp.desktop.models.User;
import hr.fer.pipp.desktop.models.ComboItem;
import hr.fer.pipp.desktop.models.ThisUser;

/**
 * ovdje spremamo listu kontakata, mapu svih ucitanih avatara i mapu
 * svih ucitanih usera (bilo kontakata, clanova razgovora, ili ulogirani korisnik)
 * zato da ne moramo svaki put dohvacati podatke iz baze za korisnika koji se nalazi u vise razgovora, 
 * isto i za avatar kojeg ima vise razlicitih korisnika
 */
public class LocalDataStorage {
	private int conversationMemberIconSize = 20;
	private ThisUser thisUser;
	private Map<Integer, ComboItem<ImageIcon>> avatarMap; // <Kljuc, Vrijednost>
	private Map<Integer, ComboItem<ImageIcon>> scaledAvatarMap; //za prikaz sličica u razgovoru, da se ne moraju scaleat slicice posebno za svaku poruku nego samo jednom
	private Map<Integer, User> knownUserMap;

	private Map<Integer, ComboItem<Font>> fontsMap;
	private Map<Integer, ComboItem<Color>> fontColorsMap;
	private Map<Integer, ComboItem<Color>> interfaceColorsMap;
	
	private int currentConversationID = -1;
	private int defaultMessageLimit = 50;
	private int messageLimit = defaultMessageLimit;
	private ImageIcon scaledLogoIconWhite, scaledLogoIconBlack;
	
	//ukazala se potreba za dodavanjem ova dva panela jer se puno njihovih komponenti stalno mijenja
	//ako ce se ukazat potreba za dodavanjem jos kojih panela, dodajte
	public boolean ScrollBarLocked = false;
	
	public LocalDataStorage(ThisUser thisUser) {
		this.thisUser = thisUser;
		avatarMap = new HashMap<>();
		knownUserMap = new HashMap<>();
		scaledAvatarMap = new HashMap<>();
		
		
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		ImageIcon logoIconWhite = new ImageIcon(classLoader.getResource("logoWhite.png"));
		Image logoImgWhite = logoIconWhite.getImage().getScaledInstance(200, 40, Image.SCALE_SMOOTH); //skaliranje
		scaledLogoIconWhite = new ImageIcon(logoImgWhite);
		
		ClassLoader classLoader2 = Thread.currentThread().getContextClassLoader();
		ImageIcon logoIconBlack = new ImageIcon(classLoader2.getResource("logoBlack.png"));
		Image logoImgBlack = logoIconBlack.getImage().getScaledInstance(200, 40, Image.SCALE_SMOOTH); //skaliranje
		scaledLogoIconBlack = new ImageIcon(logoImgBlack);
	}
	
	public void insertUser(User user){
		knownUserMap.put(user.getUserID(), user);
	}
	public boolean containsAvatar(Integer avatarID){
		return avatarMap.containsKey(avatarID);
	}
	public boolean containsUser(Integer userID){
		return knownUserMap.containsKey(userID);
	}
	public ThisUser getThisUser(){
		return thisUser;
	}
	public int getConversationMemberIconSize(){
		return conversationMemberIconSize;
	}
	
	public void insertAvatars(Map<Integer, ComboItem<ImageIcon>> avatarMap) {
		this.avatarMap = avatarMap;
		scaledAvatarMap.clear();
		for(ComboItem<ImageIcon> avatar: avatarMap.values()){
			Image newImg = avatar.getValue().getImage().getScaledInstance(conversationMemberIconSize, conversationMemberIconSize, Image.SCALE_SMOOTH);
			ComboItem<ImageIcon> scaledAvatar = new ComboItem<ImageIcon>(new ImageIcon(newImg), avatar.getLabel(), avatar.getID());
			scaledAvatarMap.put(scaledAvatar.getID(), scaledAvatar);
		}
	}
	public ComboItem<ImageIcon> getScaledAvatar(int avatarID){
		if(scaledAvatarMap.containsKey(avatarID)){
			return scaledAvatarMap.get(avatarID);
		}
		else return scaledAvatarMap.get((int)-1);
	}
	
	public ComboItem<ImageIcon> getAvatar(Integer avatarID){
		if(avatarMap.containsKey(avatarID)){
			return avatarMap.get(avatarID);
		}
		else return avatarMap.get((int)-1);
	}
	public User getUser(Integer userID){
		return knownUserMap.get(userID);
	}
	public Collection<ComboItem<ImageIcon>> getAllAvatars(){
		return avatarMap.values();
	}
	public Collection<User> getAllKnownUsers(){
		return knownUserMap.values();
	}
	public void addAllFontsAndColors(Map<Integer, ComboItem<Font>> fontsMap, Map<Integer, ComboItem<Color>> fontColorsMap, 
			Map<Integer, ComboItem<Color>> interfaceColorsMap){
		this.fontsMap=fontsMap;
		this.fontColorsMap=fontColorsMap;
		this.interfaceColorsMap=interfaceColorsMap;
	}
	public Collection<ComboItem<Font>> getAllFonts(){
		return fontsMap.values();
	}
	public Collection<ComboItem<Color>> getAllFontColors(){
		return fontColorsMap.values();
	}
	public Collection<ComboItem<Color>> getAllInterfaceColors(){
		return interfaceColorsMap.values();
	}
	
	public ComboItem<Font> getFont(int fontID){
		return fontsMap.get(fontID);
	}
	public ComboItem<Color> getInterfaceColor(int colorID){
		return interfaceColorsMap.get(colorID);
	}
	public ComboItem<Color> getFontColor(int colorID){
		return fontColorsMap.get(colorID);
	}
	
	public Font getPreferredFont(){
		return getFont(thisUser.getPreferredFontID()).getValue();
	}
	public Color getPreferredFontColor(){
		return getFontColor(thisUser.getPreferredFontColorID()).getValue();
	}
	public Color getPreferredInterfaceColor(){
		return getInterfaceColor(thisUser.getPreferredInterfaceColorID()).getValue();
	}
	
	public void setCurrentConversationID(int id){ //obavezno zvat svaki put kad odaberemo novi razgovor (klik na razgovor)
		currentConversationID = id;
	}
	public int getCurrentConversationID(){//zvat kad se salje poruka
		return currentConversationID;
	}

	public int getMessageLimit() {
		return messageLimit;
	}

	public void setMessageLimit(int messageLimit) {
		this.messageLimit = messageLimit;
	}
	public void resetMessageLimit(){
		this.messageLimit=defaultMessageLimit;
	}

	public Icon getLogo() {
		Color currentFontColor = this.getFontColor(thisUser.getPreferredFontColorID()).getValue();
		if(currentFontColor.getRed()<129 && currentFontColor.getGreen()<129 && currentFontColor.getBlue()<129){
			return scaledLogoIconBlack;
		}else{
			return scaledLogoIconWhite;
		}
	}
}
