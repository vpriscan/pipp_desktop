package hr.fer.pipp.desktop.actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import hr.fer.pipp.desktop.models.Conversation;
import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.models.User;
import hr.fer.pipp.desktop.util.LocalDataStorage;

public class ConversationsGetter {
	
	public static Conversation getConversation(Connection connection, LocalDataStorage localDataStorage, int conversationID){
		ThisUser thisUser = localDataStorage.getThisUser();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Conversation conversation = null;
		try{
			preparedStatement = connection.prepareStatement("SELECT imerazgovora, vrijemepocetka"
					+ " FROM pripadnost_razgovoru"
					+ " JOIN razgovor"
					+ " ON pripadnost_razgovoru.idrazgovora=razgovor.idrazgovora"
					+ " WHERE pripadnost_razgovoru.idrazgovora=?"
					+ " AND idsudionika=?"
					+ " AND status = TRUE");
			preparedStatement.setInt(1, conversationID);
			preparedStatement.setInt(2, thisUser.getUserID());
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){//ok stvarno je dodan te je i dalje u razgovoru
				conversation = new Conversation(conversationID, resultSet.getString(1), resultSet.getString(2));
				addConversationMembers(connection, conversation, localDataStorage); //bez ovog se moze crashat ako citamo poruke usera koji nije u lokalnoj bazi
			}
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				resultSet.close();
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
		return conversation;
	}
	/**
	 * stores a map of conversations to localDataStorage
	 * @param connection
	 * @param localDataStorage
	 */
	public static void getAndStoreConversations(Connection connection, LocalDataStorage localDataStorage) {
		ThisUser thisUser = localDataStorage.getThisUser();
		Conversation conversation;
		Map<Integer, Conversation> conversationMap = new LinkedHashMap<>();//za brzo dohvacanje jednog razgovora preko ID-a
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			preparedStatement = connection.prepareStatement("SELECT pripadnost_razgovoru.idrazgovora, imerazgovora, vrijemepocetka, vrijemeslanja"
					+" FROM pripadnost_razgovoru"
					+" JOIN razgovor ON pripadnost_razgovoru.idrazgovora=razgovor.idrazgovora"
					+" LEFT JOIN poruka ON razgovor.idrazgovora=poruka.idrazgovora"
					+" WHERE idsudionika = ?"
					+" AND status = TRUE"
					+" AND (vrijemeslanja IS NULL"
					+" OR vrijemeslanja=(SELECT MAX(vrijemeslanja)"
					+" FROM poruka poruka2"
					+" WHERE poruka2.idrazgovora=pripadnost_razgovoru.idrazgovora))"
					+" ORDER BY vrijemeslanja DESC, vrijemepocetka DESC"); //prazni razgovori bez poruka ce biti na vrhu
			preparedStatement.setInt(1, thisUser.getUserID());
			resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()){
				Integer conversationID = Integer.valueOf(resultSet.getString(1));
				conversation = new Conversation(conversationID, resultSet.getString(2), resultSet.getString(3));
				
				addConversationMembers(connection, conversation, localDataStorage); //svim tim razgovorima dodati membere
				conversationMap.put(conversationID, conversation); //spremit svaki razgovor u mapu
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				resultSet.close();
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
		
		thisUser.addConversations(conversationMap);
	}
	
	private static void addConversationMembers(Connection connection, Conversation conversation, LocalDataStorage localDataStorage){
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			preparedStatement = connection.prepareStatement("SELECT idsudionika"
					+ " FROM pripadnost_razgovoru"
					+ " WHERE idrazgovora = ?"
					+ " AND status = TRUE");
			preparedStatement.setInt(1, conversation.getConversationID());
			resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()){
				int memberID = Integer.valueOf(resultSet.getString(1));
				User member;
				if(!localDataStorage.containsUser(memberID)){
					member = BasicUserDataGetter.getUserData(connection, memberID);
					localDataStorage.insertUser(member);
				}//ovaj if-else je da ne vucemo svako-malo iz baze podatke o korisniku ako smo ga vec procitali..
				else{
					member = localDataStorage.getUser(memberID);
				}
				conversation.addMember(member);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				resultSet.close();
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
	}
}
