package hr.fer.pipp.desktop.actions;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.AbstractAction;
import com.alee.laf.text.WebTextArea;

import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.util.LocalDataStorage;

/**
 * ono sto se dogadja kad zelimo poslati poruku
 * (akciju treba pridruziti nekoj swing komponenti)
 */
public class SendMessageAction extends AbstractAction{
	private Connection connection;
	private ThisUser thisUser;
	private LocalDataStorage localDataStorage;
	private WebTextArea messageArea;
	private String hint;

	private static final long serialVersionUID = 9008049288672279143L;

	public SendMessageAction(Connection connection, LocalDataStorage localDataStorage,
			WebTextArea messageArea, String hint) {
		this.connection=connection;
		this.thisUser=localDataStorage.getThisUser();
		this.localDataStorage=localDataStorage;
		this.messageArea=messageArea;
		this.hint=hint;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int thisUserID = thisUser.getUserID();
		int currentConversationID = localDataStorage.getCurrentConversationID();
		String message = messageArea.getText();
		messageArea.setText("");
		if(currentConversationID==-1 || message.equals("") || message.equals(hint)) {
			System.err.println("ili nisi u razgovoru ili saljes prazan string ili hint");
			return;
		}
		
		PreparedStatement preparedStatement = null;
		try{
			preparedStatement = connection.prepareStatement("INSERT INTO poruka(tekst, idposiljatelja, idrazgovora)"
					+ " VALUES(?, ?, ?);");
			preparedStatement.setString(1, message);
			preparedStatement.setInt(2, thisUserID);
			preparedStatement.setInt(3, currentConversationID);
			
			preparedStatement.execute();
			
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
	}

}
