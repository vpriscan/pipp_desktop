package hr.fer.pipp.desktop.actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import hr.fer.pipp.desktop.models.User;

/**
 * iz baze podataka dohvacamo korisnicko ime i avatar za odredjeni idkorisnika,
 * ovi podaci su za sve one usere s kojima chatamo i koje imamo u kontaktima,
 * da na ekranu mozemo vidjeti te stvari (znaci korisnicko ime i avatar)
 */
public class BasicUserDataGetter {
	
	public static User getUserData(Connection connection, int userID){
		User user = null;
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			preparedStatement = connection.prepareStatement("SELECT korisnickoime, idavatar"
					+ " FROM korisnik"
					+ " WHERE idkorisnika = ?");
			
			preparedStatement.setInt(1, userID);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()){
				String username = resultSet.getString(1);
				int avatarID = Integer.valueOf(resultSet.getString(2));
				user = new User(userID, username, avatarID);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				resultSet.close();
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
		return user;
	}
	public static List<User> getFilteredUsersData(String search, Connection connection){
		List<User> userList = new ArrayList<>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			preparedStatement = connection.prepareStatement("SELECT idkorisnika, korisnickoime, idavatar"
					+ " FROM korisnik"
					+ " WHERE aktivan=TRUE"
					+ " AND LOWER(korisnickoime) LIKE ?;");
			preparedStatement.setString(1, "%" + search.toLowerCase() + "%");
			resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()){
				int userID = Integer.valueOf(resultSet.getString(1));
				String username = resultSet.getString(2);
				int avatarID = Integer.valueOf(resultSet.getString(3));
				User user = new User(userID, username, avatarID);
				userList.add(user);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				resultSet.close();
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
		return userList;
	}
}
