package hr.fer.pipp.desktop.actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserToConversationAdder {
	
	public static void addUserToConversation(Connection connection, Integer conversationID, Integer userID){
		
		PreparedStatement preparedStatement = null, preparedStatement2 = null;
		ResultSet resultSet = null;
		
		try{
			preparedStatement = connection.prepareStatement("SELECT * FROM pripadnost_razgovoru"
					+ " WHERE idrazgovora=? AND idsudionika=?");
			preparedStatement.setInt(1, conversationID);
			preparedStatement.setInt(2, userID);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){/*ovo se nikad ne bi zapravo trebalo dogodit jer smo tek kreirali razgovor al da ne bi bilo*/
				preparedStatement2 = connection.prepareStatement("UPDATE pripadnost_razgovoru"
						+ " SET status=TRUE"
						+ " WHERE idrazgovora=? AND idsudionika=?");
			}
			else{
				preparedStatement2 = connection.prepareStatement("INSERT INTO pripadnost_razgovoru(idrazgovora,idsudionika)"
						+ " VALUES(?,?)");
			}
			preparedStatement2.setInt(1, conversationID);
			preparedStatement2.setInt(2, userID);
			preparedStatement2.execute();
		}catch(SQLException e){
			try {
				resultSet.close();
				preparedStatement.close();
				preparedStatement2.close();
			}catch (SQLException | NullPointerException ignorable) {} 
		}
	}
}
