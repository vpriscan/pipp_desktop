package hr.fer.pipp.desktop.actions;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import hr.fer.pipp.desktop.models.ComboItem;
import hr.fer.pipp.desktop.util.LocalDataStorage;

/**
 * dohvacanje svih avatara i svih fontova, boja fontova i boja sucelja iz baze,
 * ili dohvacanje samo odredjenih preko njihovog ID-a
 */
public class StylesAndAvatarsGetter{
	
	public static void getAllFontsAndColors(Connection connection, LocalDataStorage localDataStorage){
		Map<Integer, ComboItem<Font>> comboFontsList = null;
		Map<Integer, ComboItem<Color>> comboFontColorsList = null;
		Map<Integer, ComboItem<Color>> comboInterfaceColorsList = null;
		
		comboFontsList = getAllFonts(connection);
		comboFontColorsList = getAllFontColors(connection);
		comboInterfaceColorsList = getAllInterfaceColors(connection);
		
		localDataStorage.addAllFontsAndColors(comboFontsList, comboFontColorsList, comboInterfaceColorsList);
	}

	private static Map<Integer, ComboItem<Font>> getAllFonts(Connection connection){
		Map<Integer, ComboItem<Font>> comboFontsList = new HashMap<>();
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			preparedStatement = connection.prepareStatement("SELECT idfonta, imefonta, velicinafonta, stilfonta"
					+ " FROM font;");
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){ //za SVE fontove/boje/itd ide while, za JEDNOG ide if
				int fontID = Integer.valueOf(resultSet.getString(1));
				String fontName = resultSet.getString(2);
				int fontSize = Integer.valueOf(resultSet.getString(3));
				String fontStyleString = resultSet.getString(4);
				int fontStyle;
				if(fontStyleString.toLowerCase().equals("bold")){
					fontStyle = Font.BOLD;
				}
				else if(fontStyleString.toLowerCase().equals("italic")){
					fontStyle = Font.ITALIC;
				}else{
					fontStyle = Font.PLAIN;
				}
				
				Font font = new Font(fontName, fontStyle, fontSize);
				ComboItem<Font> comboFont = new ComboItem<>(font, fontName+", "+fontSize+", "+fontStyleString, fontID);
				comboFontsList.put(comboFont.getID(), comboFont);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				resultSet.close();
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
		return comboFontsList;
	}
	private static Map<Integer, ComboItem<Color>> getAllFontColors(Connection connection){
		Map<Integer, ComboItem<Color>> comboFontColorsList = new HashMap<>();
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			preparedStatement = connection.prepareStatement("SELECT idboje, imeboje, rgb_r, rgb_g, rgb_b"
					+ " FROM bojafonta;");
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				int colorID = Integer.valueOf(resultSet.getString(1));
				String colorName = resultSet.getString(2);
				int red = Integer.valueOf(resultSet.getString(3));
				int green = Integer.valueOf(resultSet.getString(4));
				int blue = Integer.valueOf(resultSet.getString(5));
				
				Color color = new Color(red, green, blue);
				ComboItem<Color> comboColor = new ComboItem<>(color, colorName, colorID);
				comboFontColorsList.put(comboColor.getID(), comboColor);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				resultSet.close();
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
		return comboFontColorsList;
	}
	private static Map<Integer, ComboItem<Color>> getAllInterfaceColors(Connection connection){
		Map<Integer, ComboItem<Color>> comboFontColorsList = new HashMap<>();
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			preparedStatement = connection.prepareStatement("SELECT idboje, imeboje, rgb_r, rgb_g, rgb_b"
					+ " FROM bojasucelja;");
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				int colorID = Integer.valueOf(resultSet.getString(1));
				String colorName = resultSet.getString(2);
				int red = Integer.valueOf(resultSet.getString(3));
				int green = Integer.valueOf(resultSet.getString(4));
				int blue = Integer.valueOf(resultSet.getString(5));
				
				Color color = new Color(red, green, blue);
				ComboItem<Color> comboColor = new ComboItem<>(color, colorName, colorID);
				comboFontColorsList.put(comboColor.getID(), comboColor);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				resultSet.close();
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
		return comboFontColorsList;
	}
	
	public static void getAllAvatars(Connection connection, LocalDataStorage localDataStorage){
		Map<Integer, ComboItem<ImageIcon>> comboIconList = new HashMap<>();

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try{
			preparedStatement = connection.prepareStatement("SELECT idavatar, lokacijaavatara, imeavatara"
					+ " FROM avatar;");
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				try{
					int avatarID = Integer.valueOf(resultSet.getString(1));
					URL url = new URL(resultSet.getString(2));
					String avatarName = resultSet.getString(3);
					
					ImageIcon imageIcon = new ImageIcon(ImageIO.read(url));
					ComboItem<ImageIcon> comboIcon = new ComboItem<>(imageIcon, avatarName, avatarID);
					comboIconList.put(comboIcon.getID(), comboIcon);
					
				}catch(NullPointerException | IOException e){
					System.err.println("jednu ili vise slika u bazi nije moguce ucitati"); //formata je SVG kojeg ne znam kako ucitati
					ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
					ImageIcon errorIcon = new ImageIcon(classLoader.getResource("error.png"));
					ComboItem<ImageIcon> comboErrorIcon = new ComboItem<>(errorIcon, "error", -1);
					comboIconList.put(comboErrorIcon.getID(), comboErrorIcon);
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				resultSet.close();
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
		localDataStorage.insertAvatars(comboIconList);
	}
}
