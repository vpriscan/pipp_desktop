package hr.fer.pipp.desktop.actions;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.alee.laf.optionpane.WebOptionPane;

import hr.fer.pipp.desktop.frames.LoginFrame;
import hr.fer.pipp.desktop.models.MyPasswordField;
import hr.fer.pipp.desktop.models.MyTextField;
import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.panels.LoginPanel;
import hr.fer.pipp.desktop.util.PasswordHashGenerator;

/**
 * ono sto se dogadja kad se zelimo prvi put spojiti na bazu
 * stvaramo objekt tipa Connection koristeci defaultni username za bazu.
 * onda saljemo svoj username ili email i password na provjeru u bazu
 * ako ne valja, zatvori konekciju i dojavi pogresku da je krivi username/email ili sifra
 * (akciju treba pridruziti nekoj swing komponenti)
 */
public class LoginAction extends AbstractEntryAction{
	private static final long serialVersionUID = 8721591653717512719L;
	private LoginFrame loginFrame; //loginFrame ce nam trebat da ga mozemo zatvorit kad se ostvari veza
	private LoginPanel loginPanel;
	private String username, email; //tu cemo spremati upisane podatke iz prvog jtextfielda
	private Connection connection;
	private ThisUser thisUser; //klasu User smo sami napisali, u objekt ovog tipa cemo spremiti podatke o korisniku

	public LoginAction(LoginPanel loginPanel, LoginFrame loginFrame) { //konstruktor za ovu akciju, prima loginPanel jer ce nam trebat njegove komponente
		this.loginPanel=loginPanel;
		this.loginFrame=loginFrame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		MyTextField usernamerOrEmailField = loginPanel.getUsernameOrEmailField();
		MyPasswordField passwordField = loginPanel.getPasswordField();
		
		if(usernamerOrEmailField.isHint()
				|| passwordField.isHint()){ //ako je tekst iz bilo kojeg jtextfielda sive boje znaci da je to hint, a ne unos
			WebOptionPane.showMessageDialog(null, "Nedostaje korisničko ime ili lozinka", "NEISPRAVAN UNOS", 2);
			return;
		}
		if(usernamerOrEmailField.getText().contains("@")){//ako je email. Kasnije mozemo promijeniti na regex
			email = usernamerOrEmailField.getText();
			WebOptionPane.showMessageDialog(null, "Prijava s email-om nije podržana", "NEISPRAVAN UNOS", 2);
			return;
		}
		else{
			username = usernamerOrEmailField.getText();
		}
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;//sve sto nam upit vrati, svi retci i stupci
	
		try {
			String encryptedPassword = PasswordHashGenerator.GenerateStringSHA1(String.valueOf(passwordField.getPassword())).toLowerCase();//kriptiramo lozinku sa SHA-1
			
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(super.getDBurl(), super.getDBuser(), super.getDBpassword()); // veza ostvarena
			if(username!=null){
				preparedStatement = connection.prepareStatement("SELECT idkorisnika, korisnickoime, email, idbojafonta, idfont, idbojasucelja, idavatar"
						+ " FROM korisnik"
						+ " WHERE korisnickoime=?"
						+ " AND LOWER(lozinka)=?"
						+ " AND aktivan=true;");
				preparedStatement.setString(1, username);
				preparedStatement.setString(2, encryptedPassword);
				resultSet = preparedStatement.executeQuery();
			}
			else{//ovaj else ionako nije podrzan zasad al nema veze neka bude tu
				preparedStatement = connection.prepareStatement("SELECT idkorisnika, korisnickoime, email, idbojafonta, idfont, idbojasucelja, idavatar"
						+ " FROM korisnik"
						+ " WHERE email=?"
						+ " AND LOWER(lozinka)=?"
						+ " AND aktivan=true;");
				preparedStatement.setString(1, email);
				preparedStatement.setString(2, encryptedPassword);
				resultSet = preparedStatement.executeQuery();
			}//resultSet je sad sve ono sto nam se vratilo
			if(resultSet.next()){ //iterator koji pomice referencu na sljedeci redak(ovdje je samo jedan)
				int userID = Integer.valueOf(resultSet.getString(1)); //rezultat u stupcu 0
				String username = resultSet.getString(2);
				String email = resultSet.getString(3);
				int preferredFontColorID = Integer.valueOf(resultSet.getString(4));
				int preferredFontID = Integer.valueOf(resultSet.getString(5));
				int preferredInterfaceColorID = Integer.valueOf(resultSet.getString(6));
				int avatarID = Integer.valueOf(resultSet.getString(7));
				
				thisUser = new ThisUser(userID, username, email, preferredFontColorID, preferredFontID, preferredInterfaceColorID, avatarID); //slazemo objekt tipa ThisUser sa svim podacima koje smo dobili iz baze
			
				loginFrame.dispatchEvent(new WindowEvent(loginFrame, WindowEvent.WINDOW_CLOSING)); //gasimo prozor
			}else{
				WebOptionPane.showMessageDialog(null, "Pogrešno korisničko ime ili lozinka", "NEISPRAVAN UNOS", 2);
				connection.close();//zatvaramo konekciju tako da nam se ne otvori slucajno MainFrame (tako je napisano u LoginFrame-u)
			}
			
		} catch (SQLException e1) {
			WebOptionPane.showMessageDialog(null, e1.getMessage(), "GREŠKA", 0);
			e1.printStackTrace();
			try {
				connection.close();//da se ne bi dogodilo da slucajno ostane otvorena veza
			} catch (Exception e2) {
			} 
		} catch(Exception e2){//neka neocekivana pogreska
			e2.printStackTrace();
		} finally{
			try{
				resultSet.close();
				preparedStatement.close(); //sve treba pozatvarat osim connection
			}catch(SQLException | NullPointerException ignorable){}
		}
	}
	
	public Connection getConnection(){
		return connection;
	}
	public ThisUser getUser(){
		return thisUser;
	}
}
