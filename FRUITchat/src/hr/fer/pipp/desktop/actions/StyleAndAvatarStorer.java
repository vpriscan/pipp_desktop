package hr.fer.pipp.desktop.actions;

import java.awt.Color;
import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.ImageIcon;

import hr.fer.pipp.desktop.models.ComboItem;
import hr.fer.pipp.desktop.models.ThisUser;

/**
 * spremanje novog avatara i odabranog fonta, boje fonta, i boje sucelja u bazu podataka
 */
public class StyleAndAvatarStorer {
	
	public static boolean storeNewAvatar(Connection connection, ThisUser thisUser, ComboItem<ImageIcon> avatar){
		PreparedStatement preparedStatement = null;
		try{
			preparedStatement = connection.prepareStatement("UPDATE korisnik"
					+ " SET idavatar=?"
					+ " WHERE idkorisnika=?;");
			preparedStatement.setInt(1, avatar.getID());
			preparedStatement.setInt(2, thisUser.getUserID());
			preparedStatement.execute();
		}
		catch(SQLException e){
			e.printStackTrace();
			return false;
		}finally{
			try{
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
		return true;
	}
	
	public static void storePreferredFont(Connection connection, ThisUser thisUser, ComboItem<Font> comboFont){
		PreparedStatement preparedStatement = null;
		try{
			preparedStatement = connection.prepareStatement("UPDATE korisnik"
					+ " SET idfont=?"
					+ " WHERE idkorisnika=?;");
			preparedStatement.setInt(1, comboFont.getID());
			preparedStatement.setInt(2, thisUser.getUserID());
			preparedStatement.execute();
		}
		catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
	}
	public static void storePreferredFontColor(Connection connection, ThisUser thisUser, ComboItem<Color> comboColor){
		PreparedStatement preparedStatement = null;
		try{
			preparedStatement = connection.prepareStatement("UPDATE korisnik"
					+ " SET idbojafonta=?"
					+ " WHERE idkorisnika=?;");
			preparedStatement.setInt(1, comboColor.getID());
			preparedStatement.setInt(2, thisUser.getUserID());
			preparedStatement.execute();
		}
		catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
	}
	public static void storePreferredInterfaceColor(Connection connection, ThisUser thisUser, ComboItem<Color> comboColor){
		PreparedStatement preparedStatement = null;
		try{
			preparedStatement = connection.prepareStatement("UPDATE korisnik"
					+ " SET idbojasucelja=?"
					+ " WHERE idkorisnika=?;");
			preparedStatement.setInt(1, comboColor.getID());
			preparedStatement.setInt(2, thisUser.getUserID());
			preparedStatement.execute();
		}
		catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
	}
}
