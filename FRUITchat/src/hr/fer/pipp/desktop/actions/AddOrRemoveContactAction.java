package hr.fer.pipp.desktop.actions;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.swing.AbstractAction;

import com.alee.laf.optionpane.WebOptionPane;

import hr.fer.pipp.desktop.models.Contact;
import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.util.LocalDataStorage;

public class AddOrRemoveContactAction extends AbstractAction{

	private static final long serialVersionUID = 2011015354921914252L;

	private ThisUser thisUser;
	private LocalDataStorage localDataStorage;
	private Connection connection;
	private Contact target = null;
	public AddOrRemoveContactAction(Connection connection, LocalDataStorage localDataStorage) {
		this.connection=connection;
		this.localDataStorage=localDataStorage;
		this.thisUser=localDataStorage.getThisUser();
	}
	public void setTargetedUser(Contact contact){
		this.target=contact;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(target==null) return;
		boolean isInContacts = localDataStorage.getThisUser().hasContact(target.getUserID());
		PreparedStatement preparedStatement = null;
		PreparedStatement preparedStatement2 = null;
		ResultSet rs = null;
		try{
			if(isInContacts){
				preparedStatement = connection.prepareStatement("UPDATE kontakti"
						+ " SET status=FALSE" //kao brisanje iz kontakata
						+ " WHERE idvlasnika=?"
						+ " AND idkontakta=?;");
				preparedStatement.setInt(1, thisUser.getUserID());
				preparedStatement.setInt(2, target.getUserID());
				preparedStatement.execute();
				thisUser.removeContact(target.getUserID());
				System.out.println(target.getUsername()+" izbrisan iz kontakata");
			}else{
				target.setNickname(WebOptionPane.showInputDialog("Odaberite nadimak za "+target.getUsername()+":"));
				preparedStatement = connection.prepareStatement("SELECT * FROM kontakti"
						+ " WHERE idvlasnika=?"
						+ " and idkontakta=?;");
				preparedStatement.setInt(1, thisUser.getUserID());
				preparedStatement.setInt(2, target.getUserID());
				rs = preparedStatement.executeQuery();
				
				if(rs.next()){ //jednom mu je bio u kontaktima
					preparedStatement2 = connection.prepareStatement("UPDATE kontakti"
							+ " SET status=TRUE, nadimak=?" //kao brisanje iz kontakata
							+ " WHERE idvlasnika=?"
							+ " AND idkontakta=?;");
					if(target.getNickname()==null || target.getNickname().equals("")){
						preparedStatement2.setNull(1, Types.VARCHAR);
						preparedStatement2.setInt(2, thisUser.getUserID());
						preparedStatement2.setInt(3, target.getUserID());
					}
					else{
						preparedStatement2.setString(1, target.getNickname());
						preparedStatement2.setInt(2, thisUser.getUserID());
						preparedStatement2.setInt(3, target.getUserID());
					}
				}
				else{
					if(target.getNickname()==null || target.getNickname().equals("")){
						preparedStatement2 = connection.prepareStatement("INSERT INTO kontakti(idvlasnika, idkontakta)"
								+ " VALUES(?, ?)");
						preparedStatement2.setInt(1, thisUser.getUserID());
						preparedStatement2.setInt(2, target.getUserID());
						
					}else{
						preparedStatement2 = connection.prepareStatement("INSERT INTO kontakti(nadimak, idvlasnika, idkontakta)"
								+ " VALUES(?, ?, ?)");
						preparedStatement2.setString(1, target.getNickname());
						preparedStatement2.setInt(2, thisUser.getUserID());
						preparedStatement2.setInt(3, target.getUserID());
					}
				}
				
				preparedStatement2.execute();
				thisUser.addContact(target);
			}
			
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				rs.close();
				preparedStatement.close();
				preparedStatement2.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
	}

}
