package hr.fer.pipp.desktop.actions;

import javax.swing.AbstractAction;

/**
 * apstraktni razred u kojem su parametri za spajanje na bazu podataka
 * nasljedjuju ga LoginAction i RegisterAction
 */
public abstract class AbstractEntryAction extends AbstractAction{
	private static final long serialVersionUID = 5623075824340894954L;

	private String dburl = "jdbc:postgresql://vekszorp.noip.me:5432/pippchat"; //ovakvo je pravilo za spajanje na bazu pomocu naseg jdbc drivera
	private String dbuser = "defaultuser"; //account na bazi za obicnog usera
	private String dbpassword = "sifra";
	
	public String getDBurl(){
		return dburl;
	}
	public String getDBuser(){
		return dbuser;
	}
	public String getDBpassword(){
		return dbpassword;
	}
}
