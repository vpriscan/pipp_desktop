package hr.fer.pipp.desktop.actions;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.swing.AbstractAction;

import com.alee.laf.text.WebTextField;

import hr.fer.pipp.desktop.panels.NewConversationPanel.ContactListPanel;
import hr.fer.pipp.desktop.util.LocalDataStorage;

public class CreateNewConversationAction extends AbstractAction{
	
	private static final long serialVersionUID = -8385795677316777922L;
	private Connection connection;
	private ContactListPanel contactListPanel; 
	private WebTextField newConversationNameField;
	private LocalDataStorage localDataStorage;
	
	public CreateNewConversationAction(Connection connection, ContactListPanel contactListPanel, WebTextField newConversationNameField, LocalDataStorage localDataStorage) {
		this.connection = connection;
		this.contactListPanel = contactListPanel;
		this.newConversationNameField = newConversationNameField;
		this.localDataStorage = localDataStorage;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		List<Integer> checkedContactIDsList = contactListPanel.getCheckedContactIDs();
		String newConversationName = newConversationNameField.getText();
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try{
			preparedStatement = connection.prepareStatement("SELECT kreiraj_razgovor(?, ?)");
			preparedStatement.setString(1, newConversationName);
			preparedStatement.setInt(2, localDataStorage.getThisUser().getUserID());
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				Integer newConversationID = resultSet.getInt(1);
				for(Integer userID: checkedContactIDsList){
					UserToConversationAdder.addUserToConversation(connection, newConversationID, userID);
				}
			}
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try {
				resultSet.close();
				preparedStatement.close();
			}catch (SQLException | NullPointerException ignorable) {} 
		}
	}

}
