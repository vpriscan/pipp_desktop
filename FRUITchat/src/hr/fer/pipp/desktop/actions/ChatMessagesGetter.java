package hr.fer.pipp.desktop.actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import hr.fer.pipp.desktop.models.Conversation;
import hr.fer.pipp.desktop.models.Message;
import hr.fer.pipp.desktop.models.User;
import hr.fer.pipp.desktop.util.LocalDataStorage;

/**
 * iz baze podataka dohvacamo n posljednjih poruka u odredjenom razgovoru,
 * i sve podatke vezane uz tu poruku, i vracamo u obliku lis
 * NEDOVRSENO
 */
public class ChatMessagesGetter {
	/*
	private static int defaultMessageLimit = 50;
	
	public static List<Message> getMessages(Connection connection, Conversation conversation, 
			LocalDataStorage localDataStorage){
		return getMessages(connection, conversation, localDataStorage, defaultMessageLimit);
	}*/
	
	public static List<Message> getMessages(Connection connection, Conversation conversation, 
			LocalDataStorage localDataStorage, int messageLimit){
		int thisUserID = localDataStorage.getThisUser().getUserID();
		int conversationID = conversation.getConversationID();
		List<Message> messages= new LinkedList<>();
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			preparedStatement = connection.prepareStatement("SELECT idporuke, idposiljatelja, tekst, vrijemeslanja"
					+ " FROM poruka"
					+ " WHERE idrazgovora=?"
					+ " ORDER BY idporuke DESC"
					+ " LIMIT ?;");
			preparedStatement.setInt(1, conversationID);
			preparedStatement.setInt(2, messageLimit);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				int messageID = Integer.valueOf(resultSet.getString(1));
				int senderID = Integer.valueOf(resultSet.getString(2));
				String messageText = resultSet.getString(3);
				String timeMessageSentDecimal = resultSet.getString(4);
				String timeMessageSent = timeMessageSentDecimal.split("\\.")[0];
				
				User sender = localDataStorage.getUser(senderID);
				if(sender==null) sender = BasicUserDataGetter.getUserData(connection, senderID); //netko tko nije clan razgovora a njegove su poruke u razgovoru (bivsi clan ili provokator)
				
				Message message = new Message(localDataStorage, senderID, sender.getUsername(), 
						sender.getAvatarID(), messageID, messageText, timeMessageSent, senderID==thisUserID);
				messages.add(message);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				resultSet.close();
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
		return messages; //ali je obrnuta lista, treba iterirati od kraja prema pocetku
	}
}
