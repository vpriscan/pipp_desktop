package hr.fer.pipp.desktop.actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ConversationsUpdater {

	public static void updateConversationName(Connection connection, int conversationID, String newConversationName){
		
		PreparedStatement preparedStatement = null;
		try{
			preparedStatement = connection.prepareStatement("UPDATE razgovor"
					+ " SET imerazgovora=?"
					+ " WHERE idrazgovora=?");
			preparedStatement.setString(1, newConversationName);
			preparedStatement.setInt(2, conversationID);
			preparedStatement.execute();
			
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
	}
	
	public static void leaveConversation(Connection connection, int conversationID, int userID){
		
		PreparedStatement preparedStatement = null;
		try{
			preparedStatement = connection.prepareStatement("UPDATE pripadnost_razgovoru"
					+ " SET status = FALSE"
					+ " WHERE idsudionika=?"
					+ " AND idrazgovora=?");
			preparedStatement.setInt(1, userID);
			preparedStatement.setInt(2, conversationID);
			preparedStatement.execute();
			
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
	}
	
	

}
