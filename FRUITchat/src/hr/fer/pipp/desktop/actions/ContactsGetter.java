package hr.fer.pipp.desktop.actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import hr.fer.pipp.desktop.models.Contact;
import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.models.User;
import hr.fer.pipp.desktop.util.LocalDataStorage;

/**
 * dohvacanje svih korisnikovih kontakata iz baze podataka
 */
public class ContactsGetter {
	
	public static void getContacts(Connection connection, LocalDataStorage localDataStorage){
		Map<Integer, Contact> contactMap = new HashMap<>();
		ThisUser thisUser = localDataStorage.getThisUser();
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			preparedStatement = connection.prepareStatement("SELECT idkontakta, nadimak"
					+ " FROM kontakti"
					+ " WHERE idvlasnika=?"
					+ " AND status=TRUE;");
			preparedStatement.setInt(1, localDataStorage.getThisUser().getUserID());
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				int contactID = Integer.valueOf(resultSet.getString(1));
				String contactNickname = resultSet.getString(2);
				User temp = BasicUserDataGetter.getUserData(connection, contactID);
				Contact contact = new Contact(temp, contactNickname);
				
				contactMap.put(contact.getUserID(), contact);
				localDataStorage.insertUser(contact); //dodavanje poznatih korisnika u lokalni bazu
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				resultSet.close();
				preparedStatement.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
		thisUser.addContactMap(contactMap); //pridodjeljivanje liste kontakata ovom korisniku
	}
}
