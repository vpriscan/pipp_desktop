package hr.fer.pipp.desktop.actions;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alee.laf.optionpane.WebOptionPane;

import hr.fer.pipp.desktop.frames.RegistrationFrame;
import hr.fer.pipp.desktop.models.MyPasswordField;
import hr.fer.pipp.desktop.models.MyTextField;
import hr.fer.pipp.desktop.panels.RegistrationPanel;
import hr.fer.pipp.desktop.util.PasswordHashGenerator;

/**
 * akcija koja se dogadja kad smo stisnuli gumb za registraciju (u prozoru za registraciju)
 * nakon sto smo vec upisali zeljene podatke
 */
public class RegisterAction extends AbstractEntryAction{
	private static final long serialVersionUID = -3689226202266920895L;
	private RegistrationFrame registrationFrame;
	private RegistrationPanel registrationPanel;
	private Connection connection; 
	
	public RegisterAction(RegistrationPanel registrationPanel, RegistrationFrame registrationFrame) { //registrationFrame ce unistit kad zavrsi akcija
		this.registrationPanel=registrationPanel;
		this.registrationFrame=registrationFrame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		MyTextField usernameField = registrationPanel.getUsernameField();
		MyPasswordField passwordField = registrationPanel.getPasswordField();
		MyPasswordField repeatPasswordField = registrationPanel.getRepeatPasswordField();
		MyTextField emailField = registrationPanel.getEmailField();
		if(usernameField.isHint()
				|| passwordField.isHint()
				|| repeatPasswordField.isHint()
				|| emailField.isHint()){
			WebOptionPane.showMessageDialog(null, "Nedostaju podatci za uspješnu registraciju", "NEISPRAVAN UNOS", 2);
			return;
		}
		if(!Arrays.equals(passwordField.getPassword(), repeatPasswordField.getPassword())){
			WebOptionPane.showMessageDialog(null, "Lozinka i ponovljena lozinka nisu iste", "NEISPRAVAN UNOS", 2);
			return;
		}
		Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"); //regex za email ukraden s interneta
		Matcher matcher = emailPattern.matcher(emailField.getText());
		if(!matcher.find()){
			WebOptionPane.showMessageDialog(null, "Email adresa nije dobro upisana", "NEISPRAVAN UNOS", 2);
			return;
		}
		
		PreparedStatement preparedStatement = null;
		
		try{
			String encryptedPassword = PasswordHashGenerator.GenerateStringSHA1(String.valueOf(passwordField.getPassword())).toLowerCase();//kriptiramo lozinku sa SHA-1
			
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(super.getDBurl(), super.getDBuser(), super.getDBpassword());
			preparedStatement = connection.prepareStatement("INSERT INTO korisnik(korisnickoime, lozinka, email) "
					+ "VALUES(?, ?, ?)");
			preparedStatement.setString(1, usernameField.getText());
			preparedStatement.setString(2, encryptedPassword);
			preparedStatement.setString(3, emailField.getText());
			preparedStatement.execute();
			
			WebOptionPane.showMessageDialog(null, "Uspješna registracija!", "Obavijest", 1);
			registrationFrame.dispatchEvent(new WindowEvent(registrationFrame, WindowEvent.WINDOW_CLOSING));//gasenje registration prozora
		}catch(SQLException e1){
			WebOptionPane.showMessageDialog(null, e1.getMessage(), "GREŠKA", 0);
			e1.printStackTrace();
		}catch(Exception e2){
			e2.printStackTrace();
		}finally{
			try{
				preparedStatement.close();
				connection.close();
			}catch(SQLException | NullPointerException ignorable){}
		}
	}

}
