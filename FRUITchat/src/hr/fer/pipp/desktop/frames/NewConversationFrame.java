package hr.fer.pipp.desktop.frames;

import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JFrame;

import hr.fer.pipp.desktop.panels.NewConversationPanel;
import hr.fer.pipp.desktop.util.LocalDataStorage;

public class NewConversationFrame extends JFrame  {
	private static final long serialVersionUID = 1062920878383498144L;

		public NewConversationFrame(JButton newConversationButton, Connection connection, LocalDataStorage localDataStorage) {
			this.setTitle("Stvaranje novog razgovora");
			this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			this.setLocationRelativeTo(newConversationButton);
			this.setSize(400,400);
			this.setResizable(false);
			
			NewConversationPanel newConversationPanel = new NewConversationPanel(connection, localDataStorage, this);
			this.add(newConversationPanel);
		}
}
