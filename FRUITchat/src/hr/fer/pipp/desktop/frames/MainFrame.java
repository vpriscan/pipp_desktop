package hr.fer.pipp.desktop.frames;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JFrame;

import hr.fer.pipp.desktop.models.ThisUser;
import hr.fer.pipp.desktop.panels.MainPanel;

/**
 * glavni prozor
 *
 */
public class MainFrame extends JFrame{

	private static final long serialVersionUID = 5679738829733120846L;
	
	public MainFrame(Connection connection, ThisUser thisUser) {
		this.setTitle("FRUITchat");
		this.setSize(974, 602);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		MainPanel mainPanel = new MainPanel(connection, thisUser, this);
		this.add(mainPanel);
		this.addWindowListener(new WindowAdapter() { //kad se ugasi prozorcic, gasi se veza
			@Override
			public void windowClosing(WindowEvent e){
				try {
					if(connection!=null && !connection.isClosed()){
						connection.close();
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				super.windowClosing(e);
			}
		});
	}
}
