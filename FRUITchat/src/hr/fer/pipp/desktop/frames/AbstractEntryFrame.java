package hr.fer.pipp.desktop.frames;

import java.awt.Color;
import java.awt.Font;

import com.alee.laf.rootpane.WebFrame;

/**
 * apstratni razred, opisuje velicinu, izgled, defaultni naslov i postavke ulaznog prozora,
 * nasljedjuje ga LoginFrame i RegistrationFrame
 */
public abstract class AbstractEntryFrame extends WebFrame{
	private static final long serialVersionUID = -8733351217061046777L;
	
	private Color defaultColor = new Color(204, 255, 51); //nezrelaBanana
	private Font defaultFont = new Font("Arial", Font.BOLD, 15);
	
	public AbstractEntryFrame() {
		this.setTitle("FRUITchat");	//1.
		this.setSize(301, 487);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setResizable(false);
	}
	public Color getDefaultColor(){
		return defaultColor;
	}
	public Font getDefaultFont(){
		return defaultFont;
	}
}
