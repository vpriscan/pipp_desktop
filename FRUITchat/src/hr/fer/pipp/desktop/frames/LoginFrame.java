package hr.fer.pipp.desktop.frames;
import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import com.alee.laf.WebLookAndFeel;

import hr.fer.pipp.desktop.panels.LoginPanel;

/**
 * prozor za ulogiravanje, sadrzi main metodu, pri pokretanju stvara graficku dretvu,
 * kod gasenja programa zatvara otvorenu konekciju prema bazi
 *
 */
public class LoginFrame extends AbstractEntryFrame{ // konstruktor
	private static final long serialVersionUID = 7189376258507935322L;
	
	private Connection connection;
	private MainFrame mainFrame;
	/** ulaz u program
	 * 1. stvara graficku dretvu,
	 * 2. zove konstruktor za LoginFrame u toj novoj dretvi
	 */
	public static void main(String[] args) {
		WebLookAndFeel.install();
		EventQueue.invokeLater(new Runnable() {	//1.
			@Override
			public void run() {
				try{
					/*for(LookAndFeelInfo info: UIManager.getInstalledLookAndFeels()){
						if("Nimbus".equals(info.getName())){
							UIManager.setLookAndFeel(info.getClassName());
							break;
						}
					}*/
					LoginFrame loginFrame = new LoginFrame();	//2.
					loginFrame.setVisible(true);
				} catch(Exception ex){
					ex.printStackTrace();
				}
			}
		});
	}
	/**
	 * 1. postavlja bitne postavke prozora (dio njih je vec u apstraktnoj nadklasi)
	 * 2. u prozor dodaje panel za login
	 * 3. dodaje slusaca za akciju zatvaranja prozora
	 * 4. u slucaju zatvaranja prozora uzmi iz referencu na objekt veze i proslijedi glavnom panelu
	 */
	public LoginFrame(){ 
		super(); //1.
		this.setTitle(this.getTitle()+" prijava");
		
		//sad stvaramo novi panel
		LoginPanel loginPanel = new LoginPanel(super.getDefaultColor(), super.getDefaultFont(), this);
		//panel ima vise metoda od samog framea - zato se obicno on stavlja u frame
		this.add(loginPanel);	//2.
		this.addWindowListener(new WindowAdapter() {	//3.
			@Override
			public void windowClosing(WindowEvent e) {	//4.
				connection = loginPanel.getLoginAction().getConnection(); //tamo negje su konekcije
				// to je veza s bazom
				try {
					if(connection!=null && !connection.isClosed()){ // ako se netko ulogirao
						mainFrame = new MainFrame(connection, loginPanel.getLoginAction().getUser()); //otvori novi glavni frame
						mainFrame.setVisible(true);
					}else{
						System.exit(0); //nije se htjela ugasit aplikacija kad bi gasili LoginFrame nakon pozvanog logouta
					}
				} catch (SQLException e1) { //za rjesavanje gresaka
					e1.printStackTrace();
				}
				super.windowClosing(e); //nastavi zatvarati prozor kako si inace i mislio
			}
		});
	}
}
