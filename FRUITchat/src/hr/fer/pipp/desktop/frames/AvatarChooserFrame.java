package hr.fer.pipp.desktop.frames;

import java.sql.Connection;

import javax.swing.JFrame;
import javax.swing.JMenu;

import com.alee.laf.scroll.WebScrollPane;

import hr.fer.pipp.desktop.panels.AvatarChooserPanel;
import hr.fer.pipp.desktop.util.LocalDataStorage;

/**
 * prozor koji se otvara kad odaberemo promjenu avatara (u meniju)
 */
public class AvatarChooserFrame extends JFrame{

	private static final long serialVersionUID = 2420737217353512547L;

	public AvatarChooserFrame(JMenu menu, Connection connection, LocalDataStorage localDataStorage) {
		this.setTitle("Odabir avatara");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(menu);
		this.setSize(356, 400);
		this.setResizable(false);
		
		
		AvatarChooserPanel avatarChooserPanel = new AvatarChooserPanel(connection, localDataStorage);
		WebScrollPane scrollPane = new WebScrollPane(avatarChooserPanel);
		this.add(scrollPane);
	}
}
