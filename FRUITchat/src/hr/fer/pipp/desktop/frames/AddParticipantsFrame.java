package hr.fer.pipp.desktop.frames;

import java.sql.Connection;

import javax.swing.JFrame;
import javax.swing.JLabel;

import com.alee.laf.rootpane.WebFrame;

import hr.fer.pipp.desktop.models.Conversation;
import hr.fer.pipp.desktop.panels.AddParticipantsPanel;
import hr.fer.pipp.desktop.panels.NewConversationPanel;
import hr.fer.pipp.desktop.util.LocalDataStorage;

public class AddParticipantsFrame extends WebFrame {
	private static final long serialVersionUID = -5794460300433533970L;

	public AddParticipantsFrame(JLabel conversationOptions, Connection connection, LocalDataStorage localDataStorage, Conversation conversation) {
		this.setTitle("Dodavanje sudionika u razgovor");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(conversationOptions);
		this.setSize(400,400);
		this.setResizable(false);
		
		AddParticipantsPanel addParticipantsPanel = new AddParticipantsPanel(connection, localDataStorage, conversation, this);
		this.add(addParticipantsPanel);
	}
}
