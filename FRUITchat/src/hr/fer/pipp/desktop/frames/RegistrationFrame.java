package hr.fer.pipp.desktop.frames;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import hr.fer.pipp.desktop.panels.RegistrationPanel;

/**
 * prozor za registraciju
 *
 */
public class RegistrationFrame extends AbstractEntryFrame{
	private static final long serialVersionUID = -6102375006887036303L;
	
	public RegistrationFrame(LoginFrame loginFrame) {//prima loginFrame kako bi ga mogao ponovno postaviti kao vidljivog kad se prozor ugasi
		super();
		this.setTitle(this.getTitle()+" registracija");
		
		RegistrationPanel registrationPanel = new RegistrationPanel(super.getDefaultColor(), super.getDefaultFont(), this);
		this.add(registrationPanel);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e){ //u slucaju zatvaranja prozora, ponovno prikazi loginFrame
				loginFrame.setVisible(true);
				super.windowClosing(e);
			}
		});
	}

}
