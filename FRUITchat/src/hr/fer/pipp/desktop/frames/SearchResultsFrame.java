package hr.fer.pipp.desktop.frames;

import java.sql.Connection;

import javax.swing.JFrame;
import javax.swing.JTextField;

import hr.fer.pipp.desktop.panels.SearchResultsPanel;
import hr.fer.pipp.desktop.util.LocalDataStorage;

public class SearchResultsFrame extends JFrame  {
	private static final long serialVersionUID = 7689283432246963981L;

		public SearchResultsFrame(JTextField search, Connection connection, LocalDataStorage localDataStorage) {
			
			this.setTitle("Rezultati pretraživanja");
			this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			this.setLocationRelativeTo(search);
			this.setSize(400,400);
			this.setResizable(false);
			
			SearchResultsPanel searchResultsPanel = new SearchResultsPanel(search, connection, localDataStorage);
			this.add(searchResultsPanel);
		}
}
