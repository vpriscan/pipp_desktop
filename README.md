# README #

### What is this repository for? ###

* FRUITchat desktop aplikacija

### How do I get set up? ###

* pullat sve u jedan folder i otvorit sa eclipsom
* projekt zahtjeva javu 8 pa negdje morate u datotečnom sustavu imati jdk1.8
* postgresql jdbc driver je već includan u projekt pa ne trebate ništa dirati
* hostname servera s bazom: vekszorp.noip.me; port je 5432
* ime baze: pippchat; ime admina: postgres; ime obicnog korisnika: defaultuser